const localsMock = require('./../mocks/locals.json');
const dataMock = require('./../mocks/data.json');

module.exports = (app) => {
  app.locals = Object.assign(app.locals, localsMock);
  app.locals = Object.assign(app.locals, dataMock)
};
