const express = require('express');
const multer = require('multer');

const router = express.Router();
const controllerAsync = require('./../controllers/account');

const storage = multer.memoryStorage()
const upload = multer({
  storage,
  limits: {
    fileSize: 3 * 1024 * 1024,
    files: 1,
  },
});

/* GET home page. */
router.get('/', (req, res, next) => {
  return controllerAsync(req)
    .then((result) => {
      const currentPageData = Object.assign(result, { user: req.user });
      return res.render('pages/account', { currentPageData });
    })
    .catch(next);
});

router.post('/', (req, res, next) => {
  return controllerAsync.update(req)
    .then((result) => {
      res.json({ data: result });
    })
    .catch((err) => {
      console.error(err);
      res.json({ error: true });
    });
});

module.exports = router;
