import React, { PureComponent } from 'react';
import wrapDisplayName from 'recompose/wrapDisplayName';
import hoistNonReactStatic from 'hoist-non-react-statics';
import Typography from 'material-ui/Typography';

import AppFrame from './../containers/AppFrame';
import AppContent from './../containers/AppContent';
import AppContentInner from './../containers/AppContentInner';

const withWrapper = (BaseComponent, options) => {
  const WithWrapper = props => (
    <AppFrame {...props}>
      <AppContent {...props}>
        <AppContentInner {...props}>
          <Typography type="display1" gutterBottom>
            {options.title}
          </Typography>
          <BaseComponent {...props} />
        </AppContentInner>
      </AppContent>
    </AppFrame>
  );

  return WithWrapper;
};

export default withWrapper;
