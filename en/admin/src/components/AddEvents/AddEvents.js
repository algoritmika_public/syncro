import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';

import SelectAllQueryShort from './../SelectAllQueryShort';
import AddEventDialog from './AddEventDialog';
import EventsList from './EventsList';

import DeleteEventDialog from './DeleteEventDialog';
import DialogDeleteItem from './../DialogDeleteItem';

import generateEventsOfData from './generateEventsOfData';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

const formatDataForList = events => events.map(event => (
  Object.assign(
    {},
    event,
    {
      lecturers: event.lecturersIds_title,
      location: event.locationId_title,
      curator: event.curatorId_title,
      quantityOfTickets: event.quantityOfTickets,
    })));

class AddEvents extends Component {
  constructor(props) {
    super(props);
    const {
      lecturersIds,
      locationId,
      curatorId,
      price,
      quantityOfTickets,
      lectureId,
      data,
    } = props;
    let events = [];
    let isMutation = false;

    if (Array.isArray(data.eventsSrc)) {
      isMutation = true;
      events = generateEventsOfData(data.eventsSrc);
  }

    this.state = {
      open: false,
      lecturersIds,
      locationId,
      curatorId,
      price,
      quantityOfTickets,
      lectureId,
      events,
      isMutation,
      itemToDelete: null,
      itemToEdit: null,
    };
    this.handleShowDialog = this.handleShowDialog.bind(this);
    this.handleDialogSubmit = this.handleDialogSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeNoEvents = this.handleChangeNoEvents.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.handleDialogSubmitMutation = this.handleDialogSubmitMutation.bind(this);
  }
  handleChange = id => (event) => {
    this.setState({
      [id]: event.target.value,
    });
  }
  handleChangeNoEvents = id => (value, titles) => {
    if (titles) {
      this.setState({
        [id]: value,
        [`${id}_title`]: titles.slice(),
      });
    } else {
      this.setState({
        [id]: value,
      });
    }
  }
  handleShowDialog() {
    this.setState({
      open: !this.state.open,
    });
  }

  handleDialogSubmitMutation(newEventMutation) {
    const { itemToEdit, events } = this.state;
    const newEvent = generateEventsOfData([newEventMutation])[0];
    let newEvents = [];
    if (itemToEdit !== null) {
      newEvents = events.map((event) => {
        if (event.id === newEvent.id) {
          return newEvent;
        }
        return event;
      });
    } else {
      newEvents = events.concat(newEvent);
    }
    const newEventsTitle = this.props.field.format(newEvents);
    this.setState({
      open: false,
      events: newEvents,
    }, () => {
      // this.props.onChange(newEvents);
      this.props.onSelect(newEvents, newEventsTitle);
    });
  }
  handleDialogSubmit(newEvent) {
    const { itemToEdit } = this.state;
    let newEvents = [];
    if (itemToEdit !== null) {
      newEvents = this.state.events.map((event, index) => {
        if (index === itemToEdit) {
          return newEvent;
        }
        return event;
      });
    } else {
      newEvents = this.state.events.slice();
      newEvents.push(newEvent);
    }
    const newEventsTitle = this.props.field.format(newEvents);

    this.setState({
      open: !this.state.open,
      events: newEvents,
    }, () => {
      // this.props.onChange(newEvents);
      this.props.onSelect(newEvents, newEventsTitle);
    });
  }
  handleEdit = index => () => {
    this.setState({
      open: true,
      itemToEdit: index,
    });
  }
  handleDelete = index => () => {
    this.setState({
      itemToDelete: index,
    });
  }
  handleCancelDelete() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleDeleteConfirm() {
    const newEvents = this.state.events;
    newEvents.splice(this.state.itemToDelete, 1);
    const newEventsTitle = this.props.field.format(newEvents);
    this.setState({
      events: newEvents,
      itemToDelete: null,
    },  () => {
      // this.props.onChange(newEvents);
      this.props.onSelect(newEvents, newEventsTitle);
    });
  }
  render() {
    const {
      classes,
    } = this.props;
    const {
      open,
      lecturersIds,
      locationId,
      curatorId,
      price,
      quantityOfTickets,
      lectureId,
      events,
      itemToDelete,
      itemToEdit,
      isMutation,
    } = this.state;

    return (
      <div>
        <div>
          <SelectAllQueryShort
            typeMaterial="lecturers"
            typeView="select"
            multiple
            ids={lecturersIds}
            onChange={this.handleChangeNoEvents('lecturersIds')}
          />
          <SelectAllQueryShort
            typeMaterial="locations"
            typeView="select"
            ids={locationId}
            onChange={this.handleChangeNoEvents('locationId')}
          />
          <SelectAllQueryShort
            typeMaterial="curator"
            typeView="select"
            ids={curatorId}
            onChange={this.handleChangeNoEvents('curatorId')}
          />
          <div>
            <TextField
              id={'price'}
              label={'Цена билета'}
              value={price}
              onChange={this.handleChange('price')}
              className={classes.textField}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            />
            <TextField
              id={'quantityOfTickets'}
              label={'Количество билетов'}
              value={quantityOfTickets}
              onChange={this.handleChange('quantityOfTickets')}
              className={classes.textField}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            />
          </div>

        </div>
        <div>
          <Button
            raised
            color="primary"
            className={classes.button}
            onClick={this.handleShowDialog}
          >
              Добавить дату
          </Button>
        </div>
        <EventsList
          data={formatDataForList(events)}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
        />
        {
          open &&
          <AddEventDialog
            open={open}
            data={events[itemToEdit]}
            onClose={this.handleShowDialog}
            onSubmit={this.handleDialogSubmit}
            onSubmitMutation={this.handleDialogSubmitMutation}
            lecturersIds={lecturersIds}
            locationId={locationId}
            curatorId={curatorId}
            price={price}
            quantityOfTickets={quantityOfTickets}
            lectureId={lectureId}
            isMutation={isMutation}
          />
        }
        {
          isMutation ?
            (
              itemToDelete !== null &&
                (
                  <DeleteEventDialog
                    open
                    title={itemToDelete !== null ? `${events[itemToDelete].date} ${events[itemToDelete].time}` : ''}
                    data={events[itemToDelete]}
                    onCancel={this.handleCancelDelete}
                    onConfirm={this.handleDeleteConfirm}
                  />
                )

            ) :
            (
              <DialogDeleteItem
                open={itemToDelete !== null ? true : false}
                title={itemToDelete !== null ? `${events[itemToDelete].date} ${events[itemToDelete].time}` : ''}
                onCancel={this.handleCancelDelete}
                onConfirm={this.handleDeleteConfirm}
              />
            )
        }
      </div>
    );
  }
}

AddEvents.propTypes = {
  classes: PropTypes.object.isRequired,
};

AddEvents.defaultProps = {
  lecturersIds: [],
  locationId: '',
  price: '',
  quantityOfTickets: '',
  lectureId: 'draft',
  onChange() {},
};

export default withStyles(styles)(AddEvents);
