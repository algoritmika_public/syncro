import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import keycode from 'keycode';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import FilterListIcon from 'material-ui-icons/FilterList';
import Button from 'material-ui/Button';

import generateOutputFile from './../../libs/generateOutputFile';

import ExtTableHead from './ExtTableHead';
import ExtTableBody from './ExtTableBody';
import ExtTableFooter from './ExtTableFooter';
import ExtTableToolbar from './ExtTableToolbar';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 800,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  button: {},
  containerInner: {},
});

class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    const {
      data,
      selected,
    } = props;

    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected,
      data: props.data,
      page: 0,
      rowsPerPage: 5,
    };
    this.handleGenerateOutputFile = this.handleGenerateOutputFile.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    // const { selected: initSelected } = nextProps;
    // let { selected } = this.state;
    // if (selected.length === 0) {
    //   selected = initSelected.slice();
    // }

    this.setState({
      data: nextProps.data,
      selected: nextProps.selected.slice(),
    });
  }
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data =
      order === 'desc'
        ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ data, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.data.map(n => n.id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleKeyDown = (event, id) => {
    if (keycode(event) === 'space') {
      this.handleClick(event, id);
    }
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected }, () => {
      // this.props.onSelect({ selected: newSelected, data: this.props.data });
      this.props.onSelect(newSelected);
    });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleEdit = id => (event) => {
    this.props.onEdit(id);
  }
  handleDelete = n => (event) => {
    this.props.onDelete(n);
  }
  handleView = id => (event) => {
    this.props.onView(id);
  }
  handleGenerateOutputFile() {
    generateOutputFile(this.state.data, this.props.columnData);
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const {
      classes,
      columnData,
      toolbar,
      showActions,
    } = this.props;

    const { data, order, orderBy, selected, rowsPerPage, page } = this.state;

    return (
      <Paper className={classes.root}>
        {
          toolbar &&
          <ExtTableToolbar
            numSelected={selected.length}
            title={'Title'}
          />
        }
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <ExtTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              columnData={columnData}
              showActions={showActions}
            />
            <ExtTableBody
              data={data}
              onClick={this.handleClick}
              onKeyDown={this.handleKeyDown}
              page={page}
              rowsPerPage={rowsPerPage}
              columnData={columnData}
              isSelected={this.isSelected}
              onEdit={this.handleEdit}
              onDelete={this.handleDelete}
              onView={this.handleView}
              showActions={showActions}
            />
            <ExtTableFooter
              data={data}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </Table>
          {
            showActions &&
            (
              <Button
                raised
                color="primary"
                onClick={this.handleGenerateOutputFile}
              >
                Сохранить в файл
              </Button>
            )
          }

        </div>
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

EnhancedTable.defaultProps = {
  columnData: [],
  data: [],
  selectOnlyOne: true,
  showActions: true,
  onSelect: () => {},
  onEdit: () => {},
  onDelete: () => {},
  onView: () => {},
  selected: [],
};

export default withStyles(styles)(EnhancedTable);
