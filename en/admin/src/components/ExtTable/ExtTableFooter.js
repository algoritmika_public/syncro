import React from 'react';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';

import { format } from './../../utils';

const ExtTableFooter = (props) => {
  const {
    data,
    rowsPerPage,
    page,
    onChangePage,
    onChangeRowsPerPage,
  } = props;
  return (
    <TableFooter>
      <TableRow>
        <TablePagination
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={onChangePage}
          onChangeRowsPerPage={onChangeRowsPerPage}
          labelRowsPerPage={'Строк на страницу:'}
          labelDisplayedRows={format.labelDisplayedRows}
        />
      </TableRow>
    </TableFooter>
  );
};

export default ExtTableFooter;
