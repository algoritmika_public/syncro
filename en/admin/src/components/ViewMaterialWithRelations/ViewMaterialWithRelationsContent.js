import React, { Component } from 'react';

import ExtForm from './../ExtForm';
import withProgress from './../../hoc/withProgress';
import Title from './Title';

class ViewMaterialWithRelationsContent extends Component {
  render() {
    const {
      gqlQueryWithRelationsName,
      gqlQueryWithRelationsNameItemName,
      fields,
    } = this.props;
    const data = this.props[gqlQueryWithRelationsName][gqlQueryWithRelationsNameItemName];
    let title = '';
    if (data) {
      if (data.title) {
        title = data.title;
      } else if (data.firstName && data.lastName) {
        title = `${data.firstName} ${data.lastName}`;
      }
    }
    const Form = withProgress([gqlQueryWithRelationsName])(ExtForm);
    return (
      <div>
        <Title title={title} />
        <Form
          {...this.props}
          fields={fields.view}
          data={data}
          addRelationList
          disabledAllInputs
          disabledAllActions
          actions={[]}
        />
      </div>
    )
  }
}

export default ViewMaterialWithRelationsContent;
