import React from 'react';

import ViewMaterialWithRelationsContainer from './../../containers/ViewMaterialWithRelationsContainer';
import ViewMaterialWithRelationsContent from './ViewMaterialWithRelationsContent';

const ViewMaterialWithRelations = (props) => {
  return (
    <div>
      <ViewMaterialWithRelationsContainer
        {...props}
        component={ViewMaterialWithRelationsContent}
      />
    </div>
  );
};

export default ViewMaterialWithRelations;
