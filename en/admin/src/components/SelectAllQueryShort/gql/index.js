import {
  ALL_DISCOUNTS_QUERY_SHORT,
  ALL_LECTURERS_QUERY_SHORT,
  ALL_LOCATIONS_QUERY_SHORT,
  ALL_TAGS_QUERY_SHORT,
  ALL_ACQUIRINGS_QUERY_SHORT,
  ALL_CURATOR_QUERY_SHORT,
} from './../../../constants';

import allDiscontsQueryShort from './allDiscontsQueryShort';
import allLecturersQueryShort from './allLecturersQueryShort';
import allLocationsQueryShort from './allLocationsQueryShort';
import allTagsQueryShort from './allTagsQueryShort';
import allAcquiringsQuery from './allAcquiringsQuery';
import allCuratorsQueryShort  from './allCuratorQueryShort';

const gql = {
  [ALL_DISCOUNTS_QUERY_SHORT]: allDiscontsQueryShort,
  [ALL_LECTURERS_QUERY_SHORT]: allLecturersQueryShort,
  [ALL_LOCATIONS_QUERY_SHORT]: allLocationsQueryShort,
  [ALL_TAGS_QUERY_SHORT]: allTagsQueryShort,
  [ALL_ACQUIRINGS_QUERY_SHORT]: allAcquiringsQuery,
  [ALL_CURATOR_QUERY_SHORT]: allCuratorsQueryShort,
};

export default gql;
