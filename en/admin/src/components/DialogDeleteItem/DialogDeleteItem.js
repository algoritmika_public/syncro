import React from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui';

const DialogDeleteItem = (props) => {
  const { open, onCancel, onConfirm, title, disabled } = props;
  return (
    <Dialog
      open={open}
      onClose={!disabled ? onCancel : null}
      // classes={{ paper: classes.dialog }}
    >
      <DialogTitle>Удаление</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {`Удалить "${title}"?`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button disabled={disabled} onClick={onCancel} color="primary">Отменить</Button>
        <Button disabled={disabled} onClick={onConfirm} color="accent">Удалить</Button>
      </DialogActions>
    </Dialog>

  );
};

export default DialogDeleteItem;
