import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { withStyles } from 'material-ui/styles';
import FileUpload from 'material-ui-icons/FileUpload';
import Button from 'material-ui/Button';
import CircularProgress from './CircularProgress';
import config from './../../config';

const imagesHost = config.IMAGE_HOST;
const fileApi = config.FILE_API_ENDPOINT;

const styles = {
  dropzone: {
    width: '100%',
  },
  inner: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    color: 'rgba(0, 0, 0, 0.44)',
  },
  images: {
    textAlign: 'center',
  }
};

class ImageUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accepted: [],
      rejected: [],
      loading: false,
      success: false,
    };
    this.handleDrop = this.handleDrop.bind(this);
    this.handleSendFile = this.handleSendFile.bind(this);
    this.uploadFile = this.uploadFile.bind(this);
  }
  handleDrop(accepted, rejected) {
    this.setState({
      accepted,
      rejected,
      loading: false,
      success: false,
    });
  }
  handleSendFile() {
    this.setState({
      loading: true,
    }, () => {
      this.uploadFile(this.state.accepted);
    });
  }
  uploadFile = async (files) => {
    let data = new FormData()
    data.append('data', files[0])

    let output = null;
    try {
      const response = await fetch(
        fileApi,
        {
          method: 'POST',
          body: data,
        }
      );

      const file = await response.json();
      this.setState({
        loading: false,
        success: true,
      }, () => {
        this.props.onLoaded(file.secret);
      });
    } catch (e) {
      this.setState({
        loading: false,
        success: false,
      }, () => {
        this.props.snackbar(new Error('Ошибка. Файл не загружен'));
      });
    }
  }
  render() {
    const { accepted, loading, success } = this.state;
    const { width, height, classes, value } = this.props;

    return (
      <div className="dropzone">
        <Dropzone
          accept="image/jpeg, image/jpg, image/png, image/gif"
          onDrop={this.handleDrop}
          style={{ width: '100%' }}
          className={classes.dropzone}
        >
          {
            accepted.length === 0 && (!value || value === 'no-photo') &&
            (
              <Button style={{ width: '100%' }} className={classes.button} raised color="default">
                Загрузить
                <FileUpload className={classes.rightIcon} />
              </Button>
            )
          }
          {
            accepted.length === 0 && value &&
            (
              <div className={classes.images} >
                <img style={{ maxHeight: height, maxWidth: width }} src={`${imagesHost}/${value}`} />
              </div>
            )
          }
          {
            accepted.map((f, index) => (
              <div key={index} className={classes.images} >
                <img style={{ maxHeight: height, maxWidth: width }} src={f.preview} />
              </div>
            ))
          }
        </Dropzone>
        {
          accepted.length > 0 &&
          (
            <CircularProgress
              onClick={this.handleSendFile}
              loading={loading}
              success={success}
            />
          )
        }
      </div>
    );
  }
}

export default withStyles(styles)(ImageUpload);
