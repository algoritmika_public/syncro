import React from 'react';
import withProgress from './../../hoc/withProgress';

import MutationMaterial from './../MutationMaterial';

const ProgressComponent = props => <div />;

const MaterialQueryAndMutationRedirectContent = (props) => {
  const {
    gqlAllQueryName,
    gqlAllQueryItemsName,
    history,
    match,
  } = props;

  const data = props[gqlAllQueryName][gqlAllQueryItemsName];

  if (Array.isArray(data)) {
    if (data.length > 0) {
      const id = data[0].id;
      history.push(`${match.url}/update/${id}`);
    } else {
      history.push(`${match.url}/create`);
    }
  }
  //
  const Progress = withProgress([gqlAllQueryName])(ProgressComponent);
  return (
    <Progress {...props} />
  );


  // return (
  //   <div>
  //     Content
  //   </div>
  // );
};

export default MaterialQueryAndMutationRedirectContent;
