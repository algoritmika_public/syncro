import React from 'react';

import MaterialQueryAndMutationRedirectContent from './MaterialQueryAndMutationRedirectContent';
import MaterialQueryAndMutationContainer from './../../containers/MaterialQueryAndMutationContainer';

const MaterialQueryAndMutationRedirect = (props) => {
  return (
    <div>
      <MaterialQueryAndMutationContainer
        {...props}
        component={MaterialQueryAndMutationRedirectContent}
      />
    </div>
  );
};

export default MaterialQueryAndMutationRedirect;
