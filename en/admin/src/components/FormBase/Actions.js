import React from 'react';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  container: {
    marginTop: 20,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

const Actions = (props) => {
  const {
    type,
    classes,
    disabled,
    show,
    onEdit,
    onCancel,
    onReset,
    onSubmit,
  } = props;

  switch (type) {
    case 'filter': {
      return (
        <div className={classes.container}>
          <div>
            {
              show.reset && (
                <Button
                  raised
                  className={classes.button}
                  onClick={onReset}
                  disabled={disabled.cancel}
                >
                  Сбросить
                </Button>
              )
            }
            {
              show.search && (
                <Button
                  raised
                  color="primary"
                  className={classes.button}
                  disabled={disabled.search}
                  onClick={onSubmit}
                >
                  Найти
                </Button>
              )
            }
          </div>
        </div>
      );
    }
    default: {
      return (
        <div className={classes.container}>
          <div>
            {
              show.edit && (
                <Button
                  raised
                  color="primary"
                  className={classes.button}
                  onClick={onEdit}
                  disabled={disabled.edit}
                >
                  Редактировать
                </Button>
              )
            }
            {
              show.cancel && (
                <Button
                  raised
                  className={classes.button}
                  onClick={onCancel}
                  disabled={disabled.cancel}
                >
                  Отменить
                </Button>
              )
            }
            {
              show.save && (
                <Button
                  raised
                  color="primary"
                  onClick={onSubmit}
                  className={classes.button}
                  disabled={disabled.save}
                >
                  Сохранить
                </Button>
              )
            }
          </div>
        </div>
      );
    }
  }
};

export default withStyles(styles)(Actions);
