import React, { Component } from 'react';
import FormBase from './../FormBase';

const compareFields = (fields) => {
  const output = {};
  Object.keys(fields).forEach((key) => {
    const value = fields[key];
    if (value !== '' && value !== null && value !== undefined) {
      output[key] = value;
    }
  });
  return output;
};

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleSubmit(fields) {
    const filter = compareFields(fields);
    this.props.onSubmit(filter);
  }
  handleChange(fields) {
    const filter = compareFields(fields);
    this.props.onChange(filter);
  }
  render() {
    const {
      fields,
      req,
      onSubmit,
      onChange,
    } = this.props;
    return (
      <div>
        <FormBase
          {...this.props}
          type="filter"
          formTitle="Фильтр"
          fields={fields}
          disabledAll={false}
          disabledActions={
            {
              search: false,
              reset: false,
            }
          }
          showActions={
            {
              reset: true,
              search: true,
            }
          }
          onSubmit={this.handleSubmit}
          onChange={this.handleChange}
          req={req}
        />
      </div>
    );
  }

}

export default Filter;
