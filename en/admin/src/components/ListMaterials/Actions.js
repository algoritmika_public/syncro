import React from 'react';
import Button from 'material-ui/Button';
import { Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';

import ActionIcon from './ActionIcon';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

const Actions = (props) => {
  const { items, classes } = props;
  return (
    <div className={classes.root}>
      {
        items.map((item, index) => {
          return (
            <Button
              key={index}
              className={classes.button}
              raised
              dense
              color={item.color || 'primary'}
              disabled={item.disabled || false}
              component={Link}
              to={item.to}
            >
              <ActionIcon
                className={classes.leftIcon}
                type={item.type}
              />
              {item.title}
            </Button>
          )
        })
      }
    </div>
  );
};

export default withStyles(styles)(Actions);
