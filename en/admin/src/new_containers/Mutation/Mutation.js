import React, { Component } from 'react';
import { graphql, compose, withApollo } from 'react-apollo';

import withProgress from './../../new_hoc/withProgress';
import withStateMutation from './../../new_hoc/withStateMutation';
import withSnackbar from './../../new_hoc/withSnackbar';

const Mutation = (props) => {
  const {
    gql,
    gqlQueryName,
    gqlMutationName,
    queryOptions,
    children,
    modifyMutationOptions,
  } = props;

  const mutation = gql[gqlMutationName];

  let withQuery = null;
  let withMutation = null;
  let WithData = null;

  if (modifyMutationOptions) {
    withMutation = graphql(mutation, {
      name: gqlMutationName,
      options: modifyMutationOptions,
    });
  } else {
    withMutation = graphql(mutation, {
      name: gqlMutationName,
    });
  }

  if (gqlQueryName && gql[gqlQueryName]) {
    const query = gql[gqlQueryName];
    withQuery = graphql(
      query,
      {
        name: gqlQueryName,
        options: (ownProps) => {
          const id = ownProps.match.params.id;
          return (
            {
              variables: {
                id,
              },
            }
          );
        },
      },
    );

    // Enhance our component.
    WithData = compose(
      withApollo,
      withMutation,
      withQuery,
      withStateMutation({ name: gqlMutationName }),
      withSnackbar(),
      withProgress([gqlQueryName, gqlMutationName]),
    )(ownProps => React.Children.map(children, n => React.cloneElement(n, ownProps)));
  } else {
    WithData = compose(
      withApollo,
      withMutation,
      withStateMutation({ name: gqlMutationName }),
      withSnackbar(),
      withProgress([gqlQueryName, gqlMutationName]),
    )(ownProps => React.Children.map(children, n => React.cloneElement(n, ownProps)));
  }

  // Return the enhanced component.
  return (
    <WithData {...props} />
  );
};

Mutation.defaultProps = {
  modifyQueryFilter() { return {}; },
};

export default Mutation;
