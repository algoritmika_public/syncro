import React, { Component } from 'react';
import { compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';

import withSnackbar from './../../../new_hoc/withSnackbar';

import Text from './Text';

const queryWithId = gql`
  query AllAliasesQuery (
    $alias: String!
    $id: ID!
  ) {
    allAliases (
      filter: {
        alias: $alias
        id_not: $id
      }
    ) {
      id
      alias
    }
  }
`;

const queryWithOutId = gql`
  query AllAliasesQuery (
    $alias: String!
  ) {
    allAliases (
      filter: {
        alias: $alias
      }
    ) {
      id
      alias
    }
  }
`;

class Alias extends Component {
  constructor(props) {
    super(props);
    const { value, aliasId, manuallyChanged, title } = props;
    this.state = {
      value,
      aliasId,
      manuallyChanged,
    };
    this.fetchAliases = this.fetchAliases.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const { value, aliasId, manuallyChanged, title } = nextProps;
    this.setState({
      value,
      aliasId,
      manuallyChanged,
      title,
    });
  }
  shouldComponentUpdate(nextProps, nextState) {
    const { value: nextValue } = nextState;
    const { value } = this.state;

    return value !== nextValue;
  }
  componentWillUpdate(nextProps, nextState) {
    const { value, aliasId } = nextState;
    this.fetchAliases(value, aliasId);
  }
  componentDidUpdate(prevProps, prevState) {
    this.props.onChange(this.state.value);
  }
  handleChange = () => (event) => {
    let { value } = event.target;
    this.setState({
      value,
    });
  }
  fetchAliases(value, aliasId) {
    const { client } = this.props;
    let query = queryWithId;
    let variables = { alias: value, id: aliasId };
    if (!aliasId) {
      query = queryWithOutId;
      variables = { alias: value };
    }

    client.query({ query, variables })
      .then(({ data, errors }) => {
        if (data && data.allAliases && data.allAliases.length > 0) {
          if (/-\d{1,}$/g.test(value)) {
            const cnt = parseInt(/\d{1,}$/.exec(value)[0], 10) + 1;
            this.setState({
              value: `${value.replace(/-\d{1,}$/g, `-${cnt}`)}`,
            });
          } else {
            this.setState({
              value: `${value}-${data.allAliases.length}`,
            });
          }
        }
      })
      .catch((err) => {
        this.props.snackbar(err);
      });
  }
  render() {
    const { value, aliasId } = this.state;

    return (
      <Text
        {...this.props}
        value={value}
        onChange={this.handleChange}
      />
    );
  }
}

export default compose(withSnackbar(), withApollo)(Alias);
