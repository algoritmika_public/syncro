import React from 'react';

import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';

const Text = (props) => {
  const {
    label,
    type,
    classes,
    value,
    id,
    enum: enumValues,
    disabled,
    required,
    error,
    helperText,
    onChange,
  } = props;

  return (
    <TextField
      select
      label={label}
      className={classes.textField}
      value={value}
      onChange={onChange(id)}
      SelectProps={{
        MenuProps: {
          className: classes.menu,
        },
      }}
      margin="normal"
      disabled={disabled}
      required={required}
      error={error}
      helperText={helperText}
    >
      {enumValues.map(option => (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </TextField>
  );
};

export default Text;
