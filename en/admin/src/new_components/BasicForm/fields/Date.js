import React from 'react';
import moment from 'moment';
import TextField from 'material-ui/TextField';

const DateField = (props) => {
  const {
    label,
    type,
    className,
    value,
    id,
    disabled,
    required,
    error,
    helperText,
    onChange,
  } = props;

  return (
    <TextField
      id={id}
      label={label}
      type="date"
      onChange={onChange(id)}
      error={error}
      value={value ? moment(value).format('YYYY-MM-DD') : ''}
      className={className}
      InputLabelProps={{
        shrink: true,
      }}
    />
  );
};

export default DateField;
