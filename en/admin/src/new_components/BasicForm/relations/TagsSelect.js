import React from 'react';

import Select from './Select';

import gql from './../../../new_pages/Tags/gql';
import { ALL_TAGS_QUERY_SHORT } from './../../../constants';

const TagsSelect = (props) => {
  const { onChangeNoEvent, id, value, multiple } = props;
  return (
    <Select
      label="Направления"
      gql={gql}
      gqlQueryName={ALL_TAGS_QUERY_SHORT}
      queryResultObjectName="allTags"
      onSelect={onChangeNoEvent(id)}
      selected={value}
      multiple={multiple}
    />
  );
};

export default TagsSelect;
