import React from 'react';

import Select from './Select';

import gql from './../../../new_pages/Catalog/gql';
import { ALL_CURATOR_QUERY_SHORT } from './../../../constants';

const CuratorSelect = (props) => {
  const { onChangeNoEvent, id, value, multiple } = props;
  return (
    <Select
      label="Координатор"
      gql={gql}
      gqlQueryName={ALL_CURATOR_QUERY_SHORT}
      queryResultObjectName="allUsers"
      onSelect={onChangeNoEvent(id)}
      selected={value}
      multiple={multiple}
    />
  );
};

export default CuratorSelect;
