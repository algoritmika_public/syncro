import React from 'react';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

const isShow = actions => action => actions.indexOf(action) !== -1;

const styles = theme => ({
  container: {},
  actions: {
    marginTop: 20,
  },
  button: {
    margin: theme.spacing.unit,
  },
  form: {},
  textField: {},
  menu: {},
  input: {},
});

const BasicFormActions = (props) => {
  const {
    classes,
    actions,
    disabledAllActions,
    onReset,
    onSubmit,
    onPreview,
  } = props;
  const isShowCurrent = isShow(actions);

  return (
    <div className={classes.actions}>
      {
        isShowCurrent('preview') &&
        (
          <Button
            raised
            className={classes.button}
            onClick={onPreview}
            disabled={disabledAllActions}
            color="primary"
            type="submit"
          >
            Предварительный просмотр
          </Button>
        )
      }
      {
        isShowCurrent('reset') &&
        (
          <Button
            raised
            className={classes.button}
            onClick={onReset}
            disabled={disabledAllActions}
          >
            Сбросить
          </Button>
        )
      }
      {
        isShowCurrent('search') &&
        (
          <Button
            raised
            className={classes.button}
            onClick={onSubmit}
            disabled={disabledAllActions}
            color="primary"
            type="submit"
          >
            Найти
          </Button>
        )
      }
      {
        isShowCurrent('save') &&
        (
          <Button
            raised
            className={classes.button}
            onClick={onSubmit}
            disabled={disabledAllActions}
            color="primary"
            type="submit"
          >
            Сохранить
          </Button>
        )
      }
    </div>
  );
};

BasicFormActions.defaultProps = {
  actions: [],
};

export default withStyles(styles)(BasicFormActions);
