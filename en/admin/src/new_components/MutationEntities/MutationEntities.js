import React, { Component } from 'react';
import { compose } from 'react-apollo';

import Mutation from './../../new_containers/Mutation';
import FormEntity from './FormEntity';

const MutationEntities = (props) => {
  const {
    gql,
    gqlQueryName,
    queryResultObjectName,
    queryOptions,
    columnData,
    gqlMutationName,
    modifyMutationOptions,
    match,
  } = props;

  return (
    <Mutation
      gql={gql}
      gqlQueryName={gqlQueryName}
      queryResultObjectName={queryResultObjectName}
      queryOptions={queryOptions}
      gqlMutationName={gqlMutationName}
      modifyMutationOptions={modifyMutationOptions}
      match={match}
    >
      <FormEntity
        {...props}
      />
    </Mutation>
  );
};

export default MutationEntities;
