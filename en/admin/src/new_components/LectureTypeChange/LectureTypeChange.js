import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

import LectureTypeChangeDialog from './LectureTypeChangeDialog';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

class LectureTypeChange extends PureComponent {
  state = {
    open: false,
  }
  handleClick = () => {
    this.setState({
      open: true,
    });
  }
  handleClose = () => {
    this.setState({
      open: false,
    });
  };
  handleMutationSuccses = (data) => {
    const {
      location,
      history,
    } = this.props;
    const { pathname } = location;

    this.setState({
      open: false,
    }, () => {
      const { lectureType } = data;
      let newPathname = '';

      switch (lectureType) {
        case 'LECTURE_FOR_COURSE': {
          newPathname = pathname.replace(/(lectures|lectures-for-cycle)/g, 'lectures-for-course');
          break;
        }
        case 'LECTURE_FOR_CYCLE': {
          newPathname = pathname.replace(/(lectures|lectures-for-course)/g, 'lectures-for-cycle');
          break;
        }
        default: {
          newPathname = pathname.replace(/(lectures-for-cycle|lectures-for-course)/g, 'lectures');
        }
      }
      history.replace(newPathname);
    });
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button
          className={classes.button}
          raised
          dense
          color={'primary'}
          onClick={this.handleClick}
        >
          Изменить тип лекции
        </Button>
        {
          this.state.open &&
          (
            <LectureTypeChangeDialog
              {...this.props}
              onSuccessMutation={this.handleMutationSuccses}
              open={this.state.open}
              onClose={this.handleClose}
            />
          )
        }
      </div>
    );
  }
}

export default withStyles(styles)(LectureTypeChange);
