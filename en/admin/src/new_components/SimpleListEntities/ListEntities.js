import React, { Component } from 'react';
import { compose } from 'react-apollo';

import QueryAndMutation from './../../new_containers/QueryAndMutation';

import List from './List';

class ListEntities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRefetchRequested: false,
    };
    this.refetchLoading = this.refetchLoading.bind(this);
    this.refetchDone = this.refetchDone.bind(this);
  }
  refetchLoading() {
    this.setState({
      isRefetchRequested: true,
    });
  }
  refetchDone() {
    this.setState({
      isRefetchRequested: false,
    });
  }
  render() {
    const {
      gql,
      gqlQueryName,
      queryResultObjectName,
      columnData,
      modifyQueryFilter,
      gqlMutationName,
    } = this.props;
    const {
      isRefetchRequested,
    } = this.state;

    return (
      <QueryAndMutation
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryFilter={modifyQueryFilter}
        gqlMutationName={gqlMutationName}
        isRefetchRequested={isRefetchRequested}
      >
        <List
          {...this.props}
          refetchLoading={this.refetchLoading}
          refetchDone={this.refetchDone}
          isRefetchRequested={isRefetchRequested}
        />
      </QueryAndMutation>
    );
  }
}

export default ListEntities;
