import checkDiscountsParam from './../OrderDiscounts/checkDiscountsParam';
import calcDiscounts from './calcDiscounts';

// const subsCertsStat = (items) => {
//   const stat = {};
//   Object.keys(items)
//     .forEach((key) => {
//       items[key]
//         .forEach(({
//           title,
//           userFullName,
//           id,
//         }) => {
//           stat[key] = {
//             id,
//             title,
//             userFullName,
//             used: 0,
//           };
//         });
//     });
//   return stat;
// };

const subsCertsStat = (items) => {
  const stat = {};
  Object.keys(items)
    .forEach((key) => {
      if (items[key].length > 0) stat[key] = Object.assign(items[key][0], { used: 0 });
    });
  return stat;
};

const calcOrderPrice = (props) => {
  const {
    user,
    events,
    lectures,
    courses,
    cycles,
    additionalDiscounts,
    discounts,
    participants,
    certificates,
    subscriptions,
  } = props;

  let orderPrice = 0;
  let totalPrice = 0;
  let totalDiscountPrice = 0;
  let certSubsPrice = 0;

  const certStatistic = subsCertsStat(certificates);
  const subsStatistic = subsCertsStat(subscriptions);

  let buyer = user && user.length > 0 ? user[0] : null;

  if (!buyer) {
    return ({
      orderPrice,
      discountRate: 0,
      additionalDiscountsRate: 0,
      totalDiscountPrice,
      totalPrice,
      certStatistic,
      subsStatistic,
      certSubsPrice,
      discountsWithMaxRate: [],
      additionalDiscountsWithMaxRate: [],
    });
  }

  let course = null;
  let cycle = null;
  const eventsKeys = Object.keys(events);

  const certKeys = Object.keys(certificates);
  const subsKeys = Object.keys(subscriptions);

  let buyerCert = null;
  let buyerSubs = null;
  let eventsSaleOfCert = 0;
  let eventsSaleOfSubs = 0;

  let eventsAll = [];
  if (eventsKeys.length > 0) {
    eventsAll = eventsKeys
      .map(key => events[key])
      .reduce((prev, curr) => prev.concat(curr));
  }
  const participantsLength = participants.length;
  const eventsAllLength = eventsAll.length;
  const eventsCount = eventsAllLength * participantsLength;
  const eventsCountOfCertSub = eventsCount;

  if (courses.length > 0) {
    [course] = courses;
  }
  if (cycles.length > 0) {
    [cycle] = cycles;
  }

  if (cycle || course) {
    const src = cycle ? cycle.src : course.src;
    if (src.buyFull || (src._lecturesMeta.count === eventsKeys.length)) {
      orderPrice = src.price;
      const prFull = 1;
      let minEventsOfLecture = -1;
      let maxEventsOfLecture = -1;
      eventsKeys.map((key) => {
        const cnt = events[key].length;
        if (cnt < minEventsOfLecture || minEventsOfLecture === -1) minEventsOfLecture = cnt;
        if (cnt > maxEventsOfLecture || maxEventsOfLecture === -1) maxEventsOfLecture = cnt
      });
      if (minEventsOfLecture > 0 && maxEventsOfLecture > 0) {
        if (maxEventsOfLecture - minEventsOfLecture > 0) {
          eventsKeys.map((key) => {
            const cnt = events[key].length;
            if (cnt > minEventsOfLecture) {
              orderPrice += events[key]
                .slice(cnt - minEventsOfLecture)
                .map(({ price }) => price)
                .reduce((a, b) => a + b);
            }
          });
        } else {
          orderPrice *= minEventsOfLecture;
        }
      } else {
        orderPrice = 0;
      }
    }
  }


  // if (participants.length > 0) {
  //   orderPrice *= participants.length;
  // } else {
  //   orderPrice = 0;
  // }

  if (certKeys && certKeys.length) {
    [buyerCert] = certificates[buyer.id] ? certificates[buyer.id] : {};
    // сертификат покупателя
    if (buyerCert) {
      const {
        lecturesCount,
        personsCount,
        unusedLectures,
      } = buyerCert;
      let cnt = 0;
      // безлимит
      if (lecturesCount === -1) {
        // если персон больше или равно участникам
        if (personsCount > participantsLength || personsCount === participantsLength) {
          cnt = eventsCount;
        } else {
          // если персон меньше чем участников
          cnt = eventsAllLength * personsCount;
        }
        // лимит
        // доступное количество лекций для списания
        // если количество лекций больше чем мероприятий
      } else if (unusedLectures > eventsAllLength) {
        const prs = personsCount * eventsAllLength;
        cnt = prs;
        if (prs > unusedLectures) cnt = unusedLectures;
      } else {
        // если количество лекций меньше или равно чем мероприятий
        cnt = unusedLectures;
      }
      eventsSaleOfCert += cnt;
      certStatistic[buyer.id].used = cnt;
    }
    // сертификаты участников
    if (eventsSaleOfCert < eventsCount) {
      Object.keys(certificates)
        .filter(key => key !== buyer.id)
        .forEach((key) => {
          const [cert] = certificates[key];
          if (cert && eventsSaleOfCert < eventsCount) {
            const {
              lecturesCount,
              personsCount,
              unusedLectures,
            } = cert;
            let cnt = 0;
            // безлимит
            if (lecturesCount === -1) {
              cnt = (eventsSaleOfCert + eventsAllLength) < eventsCount ?
                eventsAllLength : (eventsCount - eventsSaleOfCert);
            } else if (unusedLectures > eventsAllLength) {
              cnt = (eventsSaleOfCert + eventsAllLength) < eventsCount ?
                eventsAllLength : (eventsCount - eventsSaleOfCert);
            } else {
              cnt = (eventsSaleOfCert + unusedLectures) < eventsCount ?
                unusedLectures : (eventsCount - eventsSaleOfCert);
            }
            eventsSaleOfCert += cnt;
            certStatistic[key].used = cnt;
          }
        });
    }
  }

  if (subsKeys && subsKeys.length && eventsSaleOfCert < eventsCount) {
    [buyerSubs] = subscriptions[buyer.id] ? subscriptions[buyer.id] : {};
    if (buyerSubs) {
      const {
        lecturesCount,
        personsCount,
        unusedLectures,
      } = buyerSubs;
      let cnt = 0;

      if (certificates[buyer.id] && certStatistic[buyer.id] && certStatistic[buyer.id].used) {
        //  если есть сертификат
        // на несколько персон
        const { used } = certStatistic[buyer.id];
        if (used < eventsAllLength) {
          cnt = unusedLectures > (eventsAllLength - used) ?
            unusedLectures - (eventsAllLength - used) : unusedLectures;
        }
        // if (personsCount > participantsLength || personsCount === participantsLength) {
        //
        // } else {
        //
        // }
      } else {
        // безлимит
        if (lecturesCount === -1) {
          // если персон больше или равно участникам
          if (personsCount > participantsLength || personsCount === participantsLength) {
            cnt = eventsCount;
          } else {
            // если персон меньше чем участников
            cnt = eventsAllLength * personsCount;
          }
          // лимит
          // доступное количество лекций для списания
          // если количество лекций больше чем мероприятий
        } else if (unusedLectures > eventsAllLength) {
          const prs = personsCount * eventsAllLength;
          cnt = prs;
          if (prs > unusedLectures) cnt = unusedLectures;
        } else {
          // если количество лекций меньше или равно чем мероприятий
          cnt = unusedLectures;
        }
      }
      cnt = (eventsSaleOfCert + cnt) > eventsCount ?
        (eventsCount - eventsSaleOfCert) :
        cnt;

      eventsSaleOfSubs += cnt;
      subsStatistic[buyer.id].used = cnt;
    }
    // абонементы участников
    if ((eventsSaleOfCert + eventsSaleOfSubs) < eventsCount) {
      Object.keys(subscriptions)
        .filter(key => key !== buyer.id)
        .forEach((key) => {
          const [subs] = subscriptions[key];
          let cnt = 0;
          if (subs && (eventsSaleOfCert + eventsSaleOfSubs) < eventsCount) {
            const {
              lecturesCount,
              personsCount,
              unusedLectures,
            } = subs;
            if (certificates[key] && certStatistic[key] && certStatistic[key].used) {
              //  если есть сертификат
              // на несколько персон
              const { used } = certStatistic[key];
              if (used < eventsAllLength) {
                cnt = unusedLectures > (eventsAllLength - used) ?
                  unusedLectures - (eventsAllLength - used) : unusedLectures;
              }
            } else {
              // безлимит
              if (lecturesCount === -1) {
                cnt = (eventsSaleOfCert + (eventsSaleOfSubs + eventsAllLength)) < eventsCount ?
                  eventsAllLength : (eventsCount - (eventsSaleOfCert + eventsSaleOfSubs));
              } else if (unusedLectures > eventsAllLength) {
                cnt = (eventsSaleOfCert + (eventsSaleOfSubs + eventsAllLength)) < eventsCount ?
                  eventsAllLength : (eventsCount - (eventsSaleOfCert + eventsSaleOfSubs));
              } else {
                cnt = (eventsSaleOfSubs + (eventsSaleOfCert + unusedLectures)) < eventsCount ?
                  unusedLectures : (eventsCount - (eventsSaleOfCert + eventsSaleOfSubs));
              }
            }
          }
          eventsSaleOfSubs += cnt;
          subsStatistic[key].used = cnt;
        });
    }
  }

  if (eventsKeys.length > 0) {
    if (orderPrice === 0) {
      let priceWithSaleCertSubs = 0;

      orderPrice = eventsKeys
        .map(key => events[key])
        .reduce((prev, curr) => prev.concat(curr))
        .map(({ price }) => price * participantsLength)
        .reduce((a, b) => a + b);

      if (eventsSaleOfCert > 0 || eventsSaleOfSubs > 0) {
        const eventsCertSubs = eventsKeys
          .map(key => events[key])
          .reduce((prev, curr) => prev.concat(curr))
          .map(({ price }) => Array.from({ length: participantsLength }, () => price))
          .reduce((prev, curr) => prev.concat(curr))
          .slice(eventsSaleOfCert)
          .slice(eventsSaleOfSubs);

        if (eventsCertSubs.length > 0) {
          priceWithSaleCertSubs = eventsCertSubs
            .reduce((a, b) => a + b);
        } else {
          certSubsPrice = orderPrice;
        }
        certSubsPrice = orderPrice - priceWithSaleCertSubs;
      }
    }
  } else {
    orderPrice = 0;
  }
  totalPrice = orderPrice - certSubsPrice;

  // скидки
  // 1.Одиночная лекция
  // Скидки: лекция, направление, все продукты
  // 2.Курс/Цикл
  // Скидки: лекция, курс/цикл, направление, все продукты
  const calcDiscountsResult = calcDiscounts(orderPrice, props, eventsAll, discounts, additionalDiscounts);

  const {
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
  } = calcDiscountsResult;

  let totalDiscountRate = discountRate + additionalDiscountsRate;
  totalDiscountRate = totalDiscountRate > 100 ? 100 : totalDiscountRate;

  if (totalDiscountRate > 100) {
    totalPrice = 0;
  } else {
    totalDiscountPrice = (totalDiscountRate * orderPrice) / 100;
    totalPrice -= totalDiscountPrice;
  }

  totalPrice = totalPrice > 0 ? totalPrice : 0;

  return ({
    orderPrice,
    discountRate,
    additionalDiscountsRate,
    totalDiscountPrice,
    totalPrice,
    discountsWithMaxRate: discountsWithMaxRate.filter(item => item),
    additionalDiscountsWithMaxRate: additionalDiscountsWithMaxRate.filter(item => item),
    certSubsPrice,
    certStatistic,
    subsStatistic,
  });
};

export default calcOrderPrice;
