import React, { Component } from 'react';

import SelectRelatedItems from './../../../SelectRelatedItems';

import gql from './../../../../new_pages/Users/gql';
import { ALL_USERS_QUERY } from './../../../../constants';
import columnData from './../../../../new_pages/Users/columnData';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';

const SelectParticipants = (props) => {
  const {
    participants,
    onSelectParticipants
  } = props;

  return (
    <SelectRelatedItems
      srcData={participants}
      relationTitle="Выберите участников"
      tableTitle="Пользователи"
      gql={gql}
      gqlQueryName={ALL_USERS_QUERY}
      queryResultObjectName="allUsers"
      onChange={onSelectParticipants}
      columnData={columnData}
      filter={fields.filter.users}
      modifyQueryFilter={modifyQueryFilter.users}
      multiple
    />
  );
};

export default SelectParticipants;
