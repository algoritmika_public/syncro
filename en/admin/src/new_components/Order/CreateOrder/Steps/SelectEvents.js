import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../../../SelectRelatedItems';

import gql from './../../../../new_pages/Events/gql';
import { ALL_EVENTS_QUERY_SHORT } from './../../../../constants';
import columnDataShort from './../../../../new_pages/Events/columnDataShort';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';

const SelectEvents = (props) => {
  const {
    events,
    lectures,
    onSelectLectureEvent
  } = props;

  if (lectures.length > 0) {
    return (
      <div style={{ marginTop: 20 }} >
        <Typography type="title" gutterBottom>
          Выберите даты
        </Typography>
        {
          lectures.map(({ id, title }) => (
            <SelectRelatedItems
              key={id}
              srcData={events[id] || []}
              relationTitle={title}
              tableTitle="Даты"
              gql={gql}
              gqlQueryName={ALL_EVENTS_QUERY_SHORT}
              queryResultObjectName="allEvents"
              onChange={onSelectLectureEvent(id)}
              columnData={columnDataShort}
              modifyQueryFilter={modifyQueryFilter.lectureEvent({ id })}
              multiple
            />
          ))
        }
      </div>
    );
  }

  return (
    <div style={{ marginTop: 20, marginBottom: 20 }}>
      <Typography type="title" gutterBottom>
        Выберите даты
      </Typography>
      <Typography type="body1" gutterBottom>
        Нет выбранных лекций
      </Typography>
    </div>
  );
};

SelectEvents.defaultProps = {
  lectures: [],
};

export default SelectEvents;
