const modifyQueryOptionsCertificatesAndSubscriptions = ({ id }) => (ownProps) => {
  const end = new Date();
  return (
    {
      variables: {
        id,
        end,
      },
    }
  );
};

export default {
  certificatesAndSubscriptions: modifyQueryOptionsCertificatesAndSubscriptions,
};
