import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../../../SelectRelatedItems';
import gql from './../../../../new_pages/Catalog/gql';
import {
  ALL_LECTURES_QUERY_SHORT,
} from './../../../../constants';
import {
  lecturesShort as columnDataLectures,
} from './../../../../new_pages/Catalog/columnData';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';

const SelectLectures = (props) => {
  const {
    lectures,
    courses,
    cycles,
    onSelectLectures,
  } = props;

  switch (true) {
    case courses && courses.length > 0: {
      return (
        <div style={{ marginTop: 20 }} >
          <Typography type="title" gutterBottom>
            Выберите лекцию
          </Typography>
          {
            courses.map(course => (
              <SelectRelatedItems
                key={course.id}
                srcData={lectures}
                relationTitle={course.title}
                tableTitle="Лекции"
                gql={gql}
                gqlQueryName={ALL_LECTURES_QUERY_SHORT}
                queryResultObjectName="allLectures"
                onChange={onSelectLectures}
                columnData={columnDataLectures}
                modifyQueryFilter={modifyQueryFilter.lecturesOfCourse(course)}
                multiple
                all={course.buyFull === 'Да' || false}
              />
            ))
          }
        </div>
      );
    }
    case cycles && cycles.length > 0: {
      return (
        <div style={{ marginTop: 20 }} >
          <Typography type="title" gutterBottom>
            Выберите лекцию
          </Typography>
          {
            cycles.map(cycle => (
              <SelectRelatedItems
                key={cycle.id}
                srcData={lectures}
                relationTitle={cycle.title}
                tableTitle="Лекции"
                gql={gql}
                gqlQueryName={ALL_LECTURES_QUERY_SHORT}
                queryResultObjectName="allLectures"
                onChange={onSelectLectures}
                columnData={columnDataLectures}
                modifyQueryFilter={modifyQueryFilter.lecturesOfCycle(cycle)}
                multiple
                all={cycle.buyFull === 'Да' || false}
              />
            ))
          }
        </div>
      );
    }
    default: {
      return (
        <SelectRelatedItems
          srcData={lectures}
          relationTitle="Выберите лекцию"
          tableTitle="Лекции"
          gql={gql}
          gqlQueryName={ALL_LECTURES_QUERY_SHORT}
          queryResultObjectName="allLectures"
          onChange={onSelectLectures}
          columnData={columnDataLectures}
          filter={fields.filter.lectures}
          modifyQueryFilter={modifyQueryFilter.lectures}
        />
      );
    }
  }
};

export default SelectLectures;
