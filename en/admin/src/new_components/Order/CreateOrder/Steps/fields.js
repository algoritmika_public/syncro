const filterUsers = [
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'email',
    label: 'E-mail',
  },
  {
    id: 'phone',
    label: 'Телефон',
  },
];

const filterLectures = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'tags',
    label: 'Направления',
    relation: 'tags-select',
    type: 'relation',
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
    relation: 'lecturers-select',
    type: 'relation',
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  {
    id: 'locations',
    label: 'Локации',
    relation: 'locations-select',
    type: 'relation',
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
];

const filterCourses = filterLectures.slice();

const filterCycles = filterLectures.slice();

export default {
  filter: {
    users: filterUsers,
    lectures: filterLectures,
    courses: filterLectures,
    cycles: filterCycles,
  },
};
