import React, { Component } from 'react';

import UpdateOrderPaid from './UpdateOrderPaid';
import UpdateOrderNotPaid from './UpdateOrderNotPaid';

class UpdateOrder extends Component {
  constructor(props) {
    super(props);
    const {
      gqlQueryName,
      queryResultObjectName,
    } = props;

    let order = null;
    if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
      order = props[gqlQueryName][queryResultObjectName];
    }

    this.state = {
      order,
    };
  }
  componentWillReceiveProps(nextProps) {
    const {
      gqlQueryName,
      queryResultObjectName,
    } = nextProps;
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      this.setState({
        order: nextProps[gqlQueryName][queryResultObjectName],
      });
    }
  }
  render() {
    const { props, state } = this;
    const { order } = state;
    if (order) {
      if (order.payment && order.payment.status === 'PAID') {
        return <UpdateOrderPaid {...props} order={order} />
      }
      return <UpdateOrderNotPaid {...props} order={order} />
    }
    return (
      <div>Загрузка...</div>
    );
  }
}

export default UpdateOrder;
