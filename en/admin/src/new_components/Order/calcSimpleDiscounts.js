const calcSimpleDiscounts = (discounts, additionalDiscounts) => {
  let discountRate = 0;
  let additionalDiscountsRate = 0;

  let discountWithMaxRate = null;
  let aadditionalDiscountWithMaxRate = null;

  if (discounts && discounts.length) {
    discounts.forEach((discount) => {
      const { rate, valid } = discount;
      if (valid && rate > discountRate) {
        discountRate = rate;
        discountWithMaxRate = discount;
      }
    });
    // discountRate = Math.max.apply(null, discounts.map(({ rate }) => rate));
  }
  if (additionalDiscounts && additionalDiscounts.length) {
    additionalDiscounts.forEach((additionalDiscount) => {
      const { rate, valid } = additionalDiscount;
      if (valid && rate > additionalDiscountsRate) {
        additionalDiscountsRate = rate;
        aadditionalDiscountWithMaxRate = additionalDiscount;
      }
    });
    // additionalDiscountsRate = Math.max.apply(null, additionalDiscounts.map(({ rate }) => rate));
  }

  return {
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate: discountWithMaxRate ? [discountWithMaxRate] : [],
    additionalDiscountsWithMaxRate: aadditionalDiscountWithMaxRate ? [aadditionalDiscountWithMaxRate] : [],
  };
};

export default calcSimpleDiscounts;
