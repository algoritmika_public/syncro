import React from 'react';
import Typography from 'material-ui/Typography';

import ViewRelationField from './ViewRelationField';

const ViewRelationFields = (props) => {
  const {
    fields,
  } = props;

  const currentFileds = fields
    .filter(({ type }) => type === 'relation');

  return (
    <div style={{ marginBottom: 60 }}>
      {
        currentFileds.map(field => (
          <div key={field.id} style={{ marginBottom: 40, marginTop: 40 }}>
            <Typography type="title" gutterBottom>
              {field.label}:
            </Typography>

            <ViewRelationField
              {...props}
              field={field}
            />
          </div>
        ))
      }
    </div>
  );
};

export default ViewRelationFields;
