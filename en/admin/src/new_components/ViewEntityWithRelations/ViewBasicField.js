import React from 'react';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';

import config from './../../config';

const imagesHost = config.IMAGE_HOST;
const fileApi = config.FILE_API_ENDPOINT;

const styles = theme => ({
  tagsDemo: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
  },
});

const ViewBasicField = (props) => {
  const {
    formatedData,
    field,
    classes,
  } = props;

  const {
    id,
    type,
    height,
    width,
    size,
  } = field;
  const value = formatedData[id];

  if (!value && type !== 'demo-tags') return (<div>Нет данных</div>);

  switch (type) {
    case 'image': {
      return (
        <img
          style={{ maxWidth: '100%' }}
          src={`${imagesHost}/${formatedData[id]}/${size}`}
        />
      )
    }
    case 'demo-tags': {
      return (
        <div
          className={classes.tagsDemo}
          style={{
            backgroundColor: formatedData.color,
            color: formatedData.textColor,
          }}
          margin="normal"
        >
          {formatedData.title}
        </div>
      );
    }
    default: {
      return (
        <Typography type="body1" gutterBottom>
          {formatedData[id].toString()}
        </Typography>
      )
    }
  }
};

export default withStyles(styles)(ViewBasicField);
