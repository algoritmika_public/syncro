import React, { Component } from 'react';
import MobileStepper from './MobileStepper';

class Stepper extends Component {
  state = {
    activeStep: 0,
  };

  handleNext = () => {
    this.setState({
      activeStep: this.state.activeStep + 1,
    });
  };

  handleBack = () => {
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  };
  render() {
    const {
      activeStep,
    } = this.state;
    const {
      steps,
    } = this.props;

    return (
      <div>
        <div>
          <MobileStepper
            activeStep={activeStep}
            steps={steps}
            onBack={this.handleBack}
            onNext={this.handleNext}
          />
        </div>
      </div>
    );
  }
}

Stepper.defaultProps = {
  steps: 1,
};

export default Stepper;
