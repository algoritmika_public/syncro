import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import MobileStepper from 'material-ui/MobileStepper';
import Button from 'material-ui/Button';
import KeyboardArrowLeft from 'material-ui-icons/KeyboardArrowLeft';
import KeyboardArrowRight from 'material-ui-icons/KeyboardArrowRight';

const styles = {
  root: {
    // maxWidth: 400,
    flexGrow: 1,
  },
};

const ProgressMobileStepper = (props) => {
  const {
    classes,
    theme,
    steps,
    activeStep,
    onNext,
    onBack,
  } = props;

  return (
    <MobileStepper
      style={{ marginBottom: 50 }}
      type="progress"
      steps={steps}
      position="static"
      activeStep={activeStep}
      className={classes.root}
      nextButton={
        <Button dense onClick={onNext} disabled={activeStep === steps - 1}>
          Дальше
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </Button>
      }
      backButton={
        <Button dense onClick={onBack} disabled={activeStep === 0}>
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
          Назад
        </Button>
      }
    />
  );
};

ProgressMobileStepper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(ProgressMobileStepper);
