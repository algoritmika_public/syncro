import React, { Component } from 'react';

import SortingAndSelecting from './../../SortingAndSelecting';

class ListItems extends Component {
  state = {

  }

  handeleRefetch = () => {
    const { gqlQueryName } = this.props;
    this.props[gqlQueryName].refetch({ filter: { firstName: 'Олег' } });
  }
  render() {
    const { gqlQueryName } = this.props;
    if (this.props[gqlQueryName] && this.props[gqlQueryName].loading) return <div>Loading</div>;
    if (this.props[gqlQueryName] && this.props[gqlQueryName].error) return <h1>ERROR</h1>;
    return (
      <div>
        ListItems
        <div onClick={this.handeleRefetch}>Refetch</div>
        <SortingAndSelecting {...this.props} />
      </div>
    );
  }
}

export default ListItems;
