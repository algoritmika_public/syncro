import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import { withStyles } from 'material-ui/styles';
import { red, grey, lightGreen } from 'material-ui/colors';

const styles = theme => ({
  error: {
    backgroundColor: red[500],
    color: grey[50],
  },
  success: {
    backgroundColor: lightGreen[500],
    color: grey[50],
  },
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
});

const SnackbarStyled = (props) => {
  const {
    classes,
    id,
    open,
    autoHideDuration,
    onClose,
    anchorOrigin,
    message,
    type,
  } = props;

  let contentClass = type === 'Error' ? classes.error : (type === 'Success' ? classes.success : '');

  return(
    <Snackbar
      anchorOrigin={anchorOrigin}
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      SnackbarContentProps={{
        'aria-describedby': id,
        className: contentClass,
      }}
      message={message}
      action={
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      }
    />
  );
};

export default withStyles(styles)(SnackbarStyled);
