import React, { Component } from 'react';
import List, {
  ListItem,
  ListItemText,
  ListItemIcon,
} from 'material-ui/List';
import CheckCircle from 'material-ui-icons/CheckCircle';
import green from 'material-ui/colors/green';
import { withStyles } from 'material-ui/styles';

import validateDiscounts from './validateDiscounts';

// const DiscountDescription = ({ rate, title, }) => {
//   return (
//     <div>
//       <div>LALALALA</div>
//       <div>{rate}</div>
//     </div>
//   )
// };

const discountDescription = ({
  rate,
  buyingLecturesCount,
  participantsCount,
  allProducts,
  tags,
}) => {
  let output = `${rate}%`;
  if (allProducts) {
    output += ' (на все продукты)'
  }
  if (tags && tags.length > 0) {
    output += ` на направления ${tags.map(({ title }) => title).join(', ')}`;
  }

  if (buyingLecturesCount !== -1 || participantsCount !== -1) {
    output += `, дествует с: ${buyingLecturesCount} лекций, ${participantsCount} участников`;
  }
  return output;
};

const styles = {
  valid: {
    color: green[900],
    fill: green[900],
  },
};

class ListResult extends Component {
  constructor(props) {
    super(props);
    const {
      gqlQueryName,
      queryResultObjectName,
    } = props;
    let data = [];
    if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
      data = props[gqlQueryName][queryResultObjectName];
    }

    this.state = {
      data,
    };
  }
  componentWillReceiveProps(nextProps) {
    const {
      gqlQueryName,
      queryResultObjectName,
    } = nextProps;

    let data = null;

    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      data = nextProps[gqlQueryName][queryResultObjectName];
    }
    data = validateDiscounts(nextProps, data);
    if (data) {
      this.setState({
        data,
      }, this.props.onSelectDiscounts(data));
    }
  }
  render() {
    const {
      data,
    } = this.state;
    const {
      product,
      lectures,
      courses,
      cycles,
      classes,
    } = this.props;

    return (
      <div>
        <List dense>
          {
            data.map(({
              id,
              title,
              rate,
              valid,
              buyingLecturesCount,
              participantsCount,
              allProducts,
              tags,
            }) => (
              <ListItem key={id}>
                <ListItemIcon>
                  <CheckCircle
                    className={valid ? classes.valid : ''}
                  />
                </ListItemIcon>
                <ListItemText
                  className={valid ? classes.valid : ''}
                  primary={title}
                  secondary={discountDescription({ rate, title, buyingLecturesCount, participantsCount, allProducts, tags })}
                />
                {/* <li>
                  className={valid ? classes.valid : ''}
                </li> */}
              </ListItem>
            ))
          }
        </List>
      </div>
    );
  }
}

ListResult.defaultProps = {
  selected: [],
  onSelectDiscounts() {},
};

export default withStyles(styles)(ListResult);
