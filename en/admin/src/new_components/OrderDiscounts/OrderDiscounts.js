import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import ListDiscountOfEntity from './ListDiscountOfEntity';
import SelectAdditionalDiscounts from './SelectAdditionalDiscounts';

const OrderDiscounts = (props) => {
  const {
    product,
    lectures,
    courses,
    cycles,
  } = props;
  const empty = !((product && product.length > 0) || (lectures && lectures.length > 0));

  return (
    <div>
      <Typography type="title" gutterBottom>
        Скидки
      </Typography>
      {
        !empty ?
        (
          <div>
            <ListDiscountOfEntity {...props} />
            <SelectAdditionalDiscounts {...props} />
          </div>
        ) :
        (
          <Typography type="body1" gutterBottom>
            Нет выбранных продуктов
          </Typography>
        )
      }
    </div>
  );
};
//
// class OrderDiscounts extends Component {
//   constructor(props) {
//     super(props);
//     const {
//       product,
//       lectures,
//       courses,
//       cycles,
//     } = props;
//
//     this.state = {
//       product,
//       lectures,
//       courses,
//       cycles,
//     };
//   }
//   componentWillReceiveProps(nextProps) {
//     const {
//       product,
//       lectures,
//       courses,
//       cycles,
//     } = nextProps;
//     this.setState({
//       product,
//       lectures,
//       courses,
//       cycles,
//     });
//   }
//   render() {
//     const {
//       product,
//       lectures,
//       courses,
//       cycles,
//     } = this.props;
//     const empty = !((product && product.length > 0) || (lectures && lectures.length > 0));
//     return (
//       <div>
//         <Typography type="title" gutterBottom>
//           Скидки
//         </Typography>
//         {
//           !empty ?
//           (
//             <div>
//               <ListDiscountOfEntity {...this.props} />
//               <SelectAdditionalDiscounts {...this.props} />
//             </div>
//           ) :
//           (
//             <Typography type="body1" gutterBottom>
//               Нет выбранных продуктов
//             </Typography>
//           )
//         }
//       </div>
//     );
//   }
//
// }

export default OrderDiscounts;
