import {
  ALL_DISCOUNTS_OF_CERTIFICATE,
  ALL_DISCOUNTS_OF_SUBSCRIPTION,
  ALL_DISCOUNTS_OF_LECTURES,
} from './../../../constants';

import allDiscountsOfCertificate from './allDiscountsOfCertificate';
import allDiscountsOfSubscription from './allDiscountsOfSubscription';
import allDiscountsOfLectures from './allDiscountsOfLectures';

export default {
  [ALL_DISCOUNTS_OF_CERTIFICATE]: allDiscountsOfCertificate,
  [ALL_DISCOUNTS_OF_SUBSCRIPTION]: allDiscountsOfSubscription,
  [ALL_DISCOUNTS_OF_LECTURES]: allDiscountsOfLectures,
};
