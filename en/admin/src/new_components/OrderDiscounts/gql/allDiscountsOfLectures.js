import gql from 'graphql-tag';

import fragments from './../../../new_pages/Discounts/fragments';

export default gql`
  query allDiscountsOfLectures($filter: DiscountFilter, $first: Int, $skip: Int, $orderBy: DiscountOrderBy) {
    allDiscounts(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...DiscountRelationsPayload
    }
  }
  ${fragments.discountReationsPayload}
`;
