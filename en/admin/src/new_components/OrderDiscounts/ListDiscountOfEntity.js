import React, { Component } from 'react';

import QueryWithQueryOptions from './../../new_containers/QueryWithQueryOptions';

import {
  ALL_DISCOUNTS_OF_CERTIFICATE,
  ALL_DISCOUNTS_OF_SUBSCRIPTION,
  ALL_DISCOUNTS_OF_LECTURES,
} from './../../constants';

import gql from './gql';

import ListResult from './ListResult';
import modifyQueryOptions from './modifyQueryOptions';

class ListDiscountOfEntity extends Component {
  componentWillMount() {
    this.props.onSelectDiscounts(null);
  }
  shouldComponentUpdate(nextProps, nextState) {
    const { discounts } = nextProps;
    if (!discounts) return true;
    return false;
  }
  render () {
    const {
      type,
      match,
    } = this.props;

    let gqlQueryName = null;
    const queryResultObjectName = 'allDiscounts';
    let currentModifyQueryOptions = null;

    switch (type) {
      case 'certificate': {
        gqlQueryName = ALL_DISCOUNTS_OF_CERTIFICATE;
        currentModifyQueryOptions = modifyQueryOptions.certificate;
        break;
      }
      case 'subscription': {
        gqlQueryName = ALL_DISCOUNTS_OF_SUBSCRIPTION;
        currentModifyQueryOptions = modifyQueryOptions.subscription;
        break;
      }
      default: {
        gqlQueryName = ALL_DISCOUNTS_OF_LECTURES;
        currentModifyQueryOptions = modifyQueryOptions.lectures;
      }
    }

    return (
      <QueryWithQueryOptions
        {...this.props}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryOptions={currentModifyQueryOptions}
        match={match}
      >
        <ListResult {...this.props} />
      </QueryWithQueryOptions>
    );
  }
}

// ListDiscountOfEntity.defaultProps = {
//   onSelectDiscounts() {},
// };

export default ListDiscountOfEntity;
