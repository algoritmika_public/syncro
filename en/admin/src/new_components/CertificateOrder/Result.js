import React from 'react';
import Typography from 'material-ui/Typography';
// import ListSubheader from 'material-ui/List/ListSubheader';
// import List, { ListItem, ListItemText } from 'material-ui/List';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

import { precisionRound } from './../../new_utils';
import calcOrderPrice from './calcOrderPrice';

const paymentTypeTitle = (paymentType) => {
  switch (paymentType) {
    case 'CASH': {
      return 'Наличные';
    }
    case 'INTERNET_ACQUIRING': {
      return 'Интернет-эквайринг';
    }
    case 'ACQUIRING': {
      return 'Эквайринг';
    }
    default: {
      return '';
    }
  }
};

// const styles = theme => ({
//   root: {
//     width: '100%',
//     // maxWidth: 360,
//     background: theme.palette.background.paper,
//     position: 'relative',
//     overflow: 'auto',
//     maxHeight: 300,
//   },
//   listSection: {
//     background: 'inherit',
//   },
// });

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: 30,
  },
  paper: {
    padding: 16,
    // textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

const noDta = 'Нет данных';
const noSelect = 'Не выбрано';

const Result = (props) => {
  const {
    classes,
    user,
    product,
    recipientOfGift,
    discounts,
    additionalDiscounts,
    paymentType,
  } = props;

  const {
    orderPrice,
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
    totalDiscountPrice,
    totalPrice,
  } = calcOrderPrice({
    product,
    additionalDiscounts,
    discounts,
  });

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Typography type="subheading" gutterBottom>
              Данные заказа:
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Пользователь:
            </Typography>
            <Typography type="body1" gutterBottom>
              {
                user.length > 0 ?
                (
                  `${user[0].firstName} ${user[0].lastName}, ${user[0].email}`
                ) :
                (
                  noSelect
                )
              }
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Тип сертификата:
            </Typography>
            <Typography type="body1" gutterBottom>
              {
                product.length > 0 ?
                (
                  product[0].title
                ) :
                (
                  noSelect
                )
              }
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Подарок:
            </Typography>
            <Typography type="body1" gutterBottom>
              {
                recipientOfGift.length > 0 ?
                (
                  `${recipientOfGift[0].firstName} ${recipientOfGift[0].lastName}, ${recipientOfGift[0].email}`
                ) :
                (
                  noSelect
                )
              }
            </Typography>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid container spacing={24}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Typography type="body2" gutterBottom>
              Скидки:
            </Typography>
            <Typography type="body1" gutterBottom>
              {`Основная скидка: ${discountRate}%`}
            </Typography>
            <Typography type="body1" gutterBottom>
              {`Дополнительная скидка: ${additionalDiscountsRate}%`}
            </Typography>
            <div>
              <List>
                {discountsWithMaxRate && discountsWithMaxRate.length > 0 &&
                  discountsWithMaxRate.map(({
                      id,
                      title,
                      rate,
                    }) => (
                      <li key={id}>
                        <Typography type="body1" gutterBottom>
                          {`Название: ${title}`}
                          <br />
                          {`Cкидка: ${rate}%`}
                        </Typography>
                      </li>
                    ))
                }
              </List>
            </div>
            <div>
              <List>
                {additionalDiscountsWithMaxRate && additionalDiscountsWithMaxRate.length > 0 &&
                  additionalDiscountsWithMaxRate.map(({
                      id,
                      title,
                      rate,
                    }) => (
                      <li key={id}>
                        <Typography type="body1" gutterBottom>
                          {`Название: ${title}`}
                          <br />
                        {`Cкидка: ${rate}%`}
                        </Typography>
                      </li>
                    ))
                }
              </List>
            </div>

            <Typography type="body1" gutterBottom>
              {`Сумма всех скидок: ${discountRate + additionalDiscountsRate}%(${totalDiscountPrice}₽)`}
            </Typography>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Typography type="body2" gutterBottom>
            Тип оплаты:
          </Typography>
          <Typography type="body1" gutterBottom>
            {
              paymentType ? paymentTypeTitle(paymentType) : noSelect
            }
          </Typography>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Сумма заказа:
            </Typography>
            <Typography type="body1" gutterBottom>
              {orderPrice}₽
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Сумма скидки:
            </Typography>
            <Typography type="body1" gutterBottom>
              {precisionRound(totalDiscountPrice, 2)}₽({discountRate}% + {additionalDiscountsRate}%)
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Итого:
            </Typography>
            <Typography type="body1" gutterBottom>
              {precisionRound(totalPrice, 2)}₽
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

export default withStyles(styles)(Result);
