import React, { Component } from 'react';
// import withProgress from './../../new_hoc/withProgress';

// const ProgressComponent = props => <div />;

// const RedirectToMutation = (props) => {
//   const {
//     gqlQueryName,
//     queryResultObjectName,
//     history,
//     match,
//   } = props;
//
//   const data = props[gqlQueryName][queryResultObjectName];
//
//   if (Array.isArray(data)) {
//     if (data.length > 0) {
//       const id = data[0].id;
//       history.replace(`${match.url}/update/${id}`);
//     } else {
//       history.replace(`${match.url}/create`);
//     }
//   }
//   //
//   // const Progress = withProgress([gqlQueryName])(ProgressComponent);
//   // return (
//   //   <Progress {...props} />
//   // );
//   return <div></div>;
// };

class RedirectToMutation extends Component {
  componentDidUpdate() {
    const {
      gqlQueryName,
      queryResultObjectName,
      history,
      match,
    } = this.props;

    const { url } = match;
    const data = this.props[gqlQueryName][queryResultObjectName];

    if (Array.isArray(data)) {
      if (data.length > 0) {
        const id = data[0].id;
        history.push(`${url}/update/${id}`);
      } else {
        history.push(`${url}/create`);
      }
    } else {
      if (data && data.id) {
        const { id } = data;
        history.push(`${url}/update/${id}`);
      } else {
        history.push(`${url}/create`);
      }

    }
  }
  render() {
    return (
      <div>...</div>
    );
  }
}

export default RedirectToMutation;
