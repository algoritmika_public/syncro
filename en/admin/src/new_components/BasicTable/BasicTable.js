import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

const BasicTable = (props) => {
  const {
    classes,
    columnData,
    data,
  } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            {
              columnData.map(({
                id,
                numeric,
                disablePadding,
                label,
              }) => (
                <TableCell
                  key={id}
                  numeric={numeric}
                  padding={disablePadding ? 'none' : 'default'}
                >
                  {label}
                </TableCell>
              ))
            }
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((n) => {
            return (
              <TableRow key={n.id}>
                {
                  columnData.map(({
                    id,
                    numeric,
                    disablePadding,
                    label,
                  }) => (
                    <TableCell key={id}>{n[id]}</TableCell>
                  ))
                }
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

BasicTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

BasicTable.defaultProps = {
  data: [],
  columnData: [],
};

export default withStyles(styles)(BasicTable);
