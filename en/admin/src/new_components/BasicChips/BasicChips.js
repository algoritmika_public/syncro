import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Chip from 'material-ui/Chip';

const styles = theme => ({
  chip: {
    margin: theme.spacing.unit,
  },
  row: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
  },
});

const BasicChips = (props) => {
  const { selected } = props;
  return (
    <div className={classes.row}>
      {
        selected.map(({ title }) => (
          <Chip label={title} className={classes.chip} />
        ))
      }
    </div>
  );
};

export default withStyles(styles)(BasicChips);
