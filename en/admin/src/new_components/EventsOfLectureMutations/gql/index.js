import {
  CREATE_EVENT_MUTATION,
  EDIT_EVENT_MUTATION,
  ALL_EVENTS_QUERY,
  EVENT_QUERY,
  DELETE_EVENT_MUTATION,
  ALL_EVENTS_OF_LECTURE_QUERY,
} from './../../../constants';

import createEventMutation from './createEventMutation';
import updateEventMutation from './updateEventMutation';
import allEventsQuery from './allEventsQuery';
import eventQuery from './eventQuery';
import deleteEventMutation from './deleteEventMutation';
import allEventsOfLectureQuery from './allEventsOfLectureQuery';


const gql = {
  [CREATE_EVENT_MUTATION]: createEventMutation,
  [EDIT_EVENT_MUTATION]: updateEventMutation,
  [ALL_EVENTS_QUERY]: allEventsQuery,
  [EVENT_QUERY]: eventQuery,
  [DELETE_EVENT_MUTATION]: deleteEventMutation,
  [ALL_EVENTS_OF_LECTURE_QUERY]: allEventsOfLectureQuery,
};

export default gql;
