import React, { Component } from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import moment from 'moment';

import FormEntity from './../MutationEntities/FormEntity';
import columnData from './../EventOfLectureNoMutations/columnData';

import { formatInputDataForForm } from './../../new_utils';
// import formatEventDataForForm from './formatEventDataForForm';

const EventMutationDialog = (props) => {
  const {
    open,
    onClose,
    lecturers,
    location,
    price,
    quantityOfTickets,
    lectureId,
    curator,
    data,
    title,

    fields,
    gqlMutationName,
    mutationResultObjectName,
    resultMutationMessageSuccses,
    onSuccessMutation,
  } = props;

  const formatedData = formatInputDataForForm(
    Object.assign(
      {},
      {
        lecturers,
        location,
        curator,
        price,
        quantityOfTickets,
        lectureId,
        date: '',
      },
      data,
      {
        date: (data && data.date) ? moment(data.date).format('YYYY-MM-DD') : '',
      },
    ));

  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Можно задать лектора, локацию, цену, количество билетов для конкретной даты.
          </DialogContentText>
          <FormEntity
            {...props}
            gqlMutationName={gqlMutationName}
            mutationResultObjectName={mutationResultObjectName}
            resultMutationMessageSuccses={resultMutationMessageSuccses}
            columnData={columnData}
            fields={fields}
            data={formatedData}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Отменить
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

EventMutationDialog.defaultProps = {
  onClose() {

  },
  onSubmit() {

  },
  open: false,
};

export default EventMutationDialog;
