import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

import withStateMutation from './../../hoc/withStateMutation';
//
// const MaterialMutationContainer = (props) => {
//   const {
//     gql,
//     gqlMutationName,
//     gqlQueryName,
//   } = props;

//   let InnerChild = null;
//   if (gqlQueryName) {
//     InnerChild = (
//       compose(
//         graphql(gql[gqlQueryName], {
//           name: gqlQueryName,
//           options(ownProps) {
//             const id = ownProps.match.params.id;
//             return (
//               {
//                 variables: {
//                   id,
//                 },
//               }
//             );
//           },
//         }),
//         graphql(gql[gqlMutationName], {
//           name: gqlMutationName,
//         }),
//         withStateMutation({ name: gqlMutationName }),
//       )(props.component)
//     );
//   } else {
//     InnerChild = (
//       compose(
//         graphql(gql[gqlMutationName], {
//           name: gqlMutationName,
//         }),
//         withStateMutation({ name: gqlMutationName }),
//       )(props.component)
//     );
//   }
//
//   return (
//     <InnerChild
//       {...props}
//     />
//   );
// };

class MaterialMutationContainer extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }
  render() {
    const {
      gql,
      gqlMutationName,
      gqlQueryName,
    } = this.props;

    let InnerChild = null;
    if (gqlQueryName) {
      InnerChild = (
        compose(
          graphql(gql[gqlQueryName], {
            name: gqlQueryName,
            options(ownProps) {
              const id = ownProps.match.params.id;
              return (
                {
                  variables: {
                    id,
                  },
                }
              );
            },
          }),
          graphql(gql[gqlMutationName], {
            name: gqlMutationName,
          }),
          withStateMutation({ name: gqlMutationName }),
        )(this.props.component)
      );
    } else {
      InnerChild = (
        compose(
          graphql(gql[gqlMutationName], {
            name: gqlMutationName,
          }),
          withStateMutation({ name: gqlMutationName }),
        )(this.props.component)
      );
    }

    return (
      <InnerChild
        {...this.props}
      />
    );
  }
}

MaterialMutationContainer.defaultProps = {
  gqlMutationName: '',
  gql: {},
};

export default MaterialMutationContainer;
