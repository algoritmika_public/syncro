import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

import withStateMutation from './../../hoc/withStateMutation';

// const ViewMaterialWithRelationsContainer = (props) => {
//   const {
//     gql,
//     gqlQueryWithRelationsName,
//   } = props;
//
//   const InnerChild = (
//     compose(
//       graphql(gql[gqlQueryWithRelationsName], {
//         name: gqlQueryWithRelationsName,
//         options(ownProps) {
//           const id = ownProps.match.params.id;
//           return (
//             {
//               variables: {
//                 id,
//               },
//             }
//           );
//         },
//       }),
//     )(props.component)
//   );
//
//   return (
//     <InnerChild
//       {...props}
//     />
//   );
// };

class ViewMaterialWithRelationsContainer extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }
  render() {

    const {
      gql,
      gqlQueryWithRelationsName,
    } = this.props;

    const InnerChild = (
      compose(
        graphql(gql[gqlQueryWithRelationsName], {
          name: gqlQueryWithRelationsName,
          options(ownProps) {
            const id = ownProps.match.params.id;
            return (
              {
                variables: {
                  id,
                },
              }
            );
          },
        }),
      )(this.props.component)
    );

    return (
      <InnerChild
        {...this.props}
      />
    );
  }
}

ViewMaterialWithRelationsContainer.defaultProps = {
  gqlQueryWithRelationsName: '',
  gql: {},
  filterModifier: input => input,
};

export default ViewMaterialWithRelationsContainer;
