import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

import withStateMutation from './../../hoc/withStateMutation';
//

class MaterialQueryAndMutationContainer extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }
  render() {
    const {
      gql,
      gqlMutationName,
      gqlQueryName,
      gqlAllQueryName,
      filterModifier,
    } = this.props;

    let InnerChild = null;

    InnerChild = (
      compose(
        graphql(gql[gqlAllQueryName], {
          name: gqlAllQueryName,
          options: (ownProps) => {
            return {
              variables: {
                filter: filterModifier(ownProps.allQueryFilter, ownProps.filterType),
              },
            };
          },
        }),
      )(this.props.component)
    );

    return (
      <InnerChild
        {...this.props}
      />
    );
  }
}

MaterialQueryAndMutationContainer.defaultProps = {
  gqlMutationName: '',
  gql: {},
  filterModifier: input => input,
};

export default MaterialQueryAndMutationContainer;
