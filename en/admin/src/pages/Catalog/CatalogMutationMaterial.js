import React from 'react';

import {
  output as outputUtils,
  format as formatUtil,
} from './../../utils';

import gql from './gql';

import {
  LECTURE_QUERY,
  COURSE_QUERY,
  CYCLE_QUERY,
  CERTIFICATE_PRODUCT_QUERY,
  SUBSCRIPTION_PRODUCT_QUERY,

  EDIT_LECTURE_MUTATION,
  EDIT_COURSE_MUTATION,
  EDIT_CYCLE_MUTATION,

  CREATE_LECTURE_MUTATION,
  CREATE_COURSE_MUTATION,
  CREATE_CYCLE_MUTATION,

  EDIT_SUBSCRIPTION_PRODUCT_MUTATION,
  EDIT_CERTIFICATE_PRODUCT_MUTATION,

  CREATE_SUBSCRIPTION_PRODUCT_MUTATION,
  CREATE_CERTIFICATE_PRODUCT_MUTATION,
  GC_USER_ID,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';
import LectureTypeChange from './../../components/LectureTypeChange';
import {
  lecture as lectureFields,
  lectureForCourse as lectureForCourseFields,
  lectureForCycle as lectureForCycleFields,
  course as courseFields,
  cycle as cycleFields,
  subscription as subscriptionFields,
  certificate as certificateFields,
} from './fields';

// import {
//   lectures as lecturesActions,
//   courses as coursesActions,
//   cycles as cyclesActions,
// } from './actions';

const CatalogMutationMaterial = (props) => {
  const {
    typeMaterial,
    mutationType,
    lectureType,
  } = props;

  let showLectureTypeChange = false;
  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = null;
  let mutationResultObjectName = '';
  const outputModifier = outputUtils.outputMutationModifier;
  let fields = null;
  let title = '';
  let gqlQueryName = null;
  let gqlQueryItemName = null;
  let formatInputData = null;

  switch (typeMaterial) {
    case 'lecture': {
      let currentFileds = null;
      // formatInputData = formatUtil.lectureInputData;
      switch (lectureType) {
        case 'lectureForCourse': {
          currentFileds = lectureForCourseFields;
          break;
        }
        case 'lectureForCycle': {
          currentFileds = lectureForCycleFields;
          break;
        }
        default: {
          currentFileds = lectureFields;
        }
      }
      if (mutationType === 'create') {
        gqlMutationName = CREATE_LECTURE_MUTATION;
        fields = currentFileds.create;
        mutationResultObjectName = 'createLecture';
        resulMutatiomtMessageSuccses = 'Лекция создана';
        title = 'Создание новой лекции';
      } else {
        showLectureTypeChange = true;
        gqlMutationName = EDIT_LECTURE_MUTATION;
        fields = currentFileds.update;
        mutationResultObjectName = 'updateLecture';
        resulMutatiomtMessageSuccses = 'Лекция обновлена';
        title = 'Редактирование лекции';
        gqlQueryName = LECTURE_QUERY;
        gqlQueryItemName = 'Lecture';
      }

      break;
    }
    case 'course': {
      if (mutationType === 'create') {
        gqlMutationName = CREATE_COURSE_MUTATION;
        fields = courseFields.create;
        mutationResultObjectName = 'createCourse';
        resulMutatiomtMessageSuccses = 'Курс создан';
      } else {
        gqlMutationName = EDIT_COURSE_MUTATION;
        fields = courseFields.update;
        mutationResultObjectName = 'updateCourse';
        resulMutatiomtMessageSuccses = 'Курс обновлен';
        gqlQueryName = COURSE_QUERY;
        gqlQueryItemName = 'Course';
      }
      break;
    }
    case 'cycle': {
      if (mutationType === 'create') {
        gqlMutationName = CREATE_CYCLE_MUTATION;
        fields = cycleFields.create;
        mutationResultObjectName = 'createCycle';
        resulMutatiomtMessageSuccses = 'Цикл создан';
      } else {
        gqlMutationName = EDIT_CYCLE_MUTATION;
        fields = cycleFields.update;
        mutationResultObjectName = 'updateCycle';
        resulMutatiomtMessageSuccses = 'Цикл обновлен';
        gqlQueryName = CYCLE_QUERY;
        gqlQueryItemName = 'Cycle';
      }
      break;
    }
    case 'certificate': {
      if (mutationType === 'create') {
        gqlMutationName = CREATE_CERTIFICATE_PRODUCT_MUTATION;
        fields = certificateFields.create;
        mutationResultObjectName = 'createCertificateProduct';
        resulMutatiomtMessageSuccses = 'Новый тип сертификата создан';
      } else {
        gqlMutationName = EDIT_CERTIFICATE_PRODUCT_MUTATION;
        fields = certificateFields.update;
        mutationResultObjectName = 'updateCertificateProduct';
        resulMutatiomtMessageSuccses = 'Сертификата обновлен';
        gqlQueryName = CERTIFICATE_PRODUCT_QUERY;
        gqlQueryItemName = 'CertificateProduct';
      }
      break;
    }
    case 'subscription': {
      if (mutationType === 'create') {
        gqlMutationName = CREATE_SUBSCRIPTION_PRODUCT_MUTATION;
        fields = subscriptionFields.create;
        mutationResultObjectName = 'createSubscriptionProduct';
        resulMutatiomtMessageSuccses = 'Новый тип абонемента создан';
      } else {
        gqlMutationName = EDIT_SUBSCRIPTION_PRODUCT_MUTATION;
        fields = subscriptionFields.update;
        mutationResultObjectName = 'updateCertificateProduct';
        resulMutatiomtMessageSuccses = 'Абонемент обновлен';
        gqlQueryName = SUBSCRIPTION_PRODUCT_QUERY;
        gqlQueryItemName = 'SubscriptionProduct';
      }
      break;
    }
    default: {
      gqlMutationName = '';
      mutationResultObjectName = '';
    }
  }
  fields = fields.map(field => {
    if (field.id === 'authorId') {
      field.value = localStorage.getItem(GC_USER_ID);
    }
    return field;
  })

  return (
    <div>
      {
        showLectureTypeChange &&
        <LectureTypeChange {...props} />
      }
      <MutationMaterial
        {...props}
        // formatInputData={formatInputData}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
        title={title}
      />
    </div>
  );
};

export default CatalogMutationMaterial;
