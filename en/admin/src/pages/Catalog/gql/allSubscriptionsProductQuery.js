import gql from 'graphql-tag';

export default gql`
  query AllSubscriptionProductQuery($filter: SubscriptionProductFilter, $first: Int, $skip: Int, $orderBy: SubscriptionProductOrderBy) {
    allSubscriptionProducts(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lectures,
      months,
      price,
      prefix,
      itemType,
      image,
      type,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
      subscriptions {
        id,
        title,
        start,
        end,
        user {
          id,

          firstName,
          lastName,
        }
      }
      _subscriptionsMeta {
        count
      }
      _discountsMeta {
        count
      }
      #
    },
    _allSubscriptionProductsMeta {
      count,
    },
  }
`;
