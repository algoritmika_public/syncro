import gql from 'graphql-tag';

export default gql `
  mutation UpdateCourseMutation(
      $id: ID!,
      $anons: String,
      $buyFull: Boolean,
      $description: String,
      $img1240x349: String,
      $img340x192: String,
      $metaDescription: String,
      $metaKeywords: String,
      $price: Float,
      $public: Boolean,
      $subTitle: String,
      $title: String,
      $aliasId: ID,
      $authorId: ID,
      $metaTagsId: ID,
      $inRecommendationsOfCoursesIds: [ID!],
      $inRecommendationsOfCyclesIds: [ID!],
      $inRecommendationsOfLecturesIds: [ID!],
      $lecturesIds: [ID!],
      $recommendedCoursesIds: [ID!],
      $recommendedCyclesIds: [ID!],
      $recommendedLecturesIds: [ID!],
      $reviewsIds: [ID!],
      $tagsIds: [ID!],
    ){
    updateCourse(
      id: $id,
      anons: $anons,
      buyFull: $buyFull,
      description: $description,
      img1240x349: $img1240x349,
      img340x192: $img340x192,
      metaDescription: $metaDescription,
      metaKeywords: $metaKeywords,
      price: $price,
      public: $public,
      subTitle: $subTitle,
      title: $title,
      aliasId: $aliasId,
      authorId: $authorId,
      metaTagsId: $metaTagsId,
      inRecommendationsOfCoursesIds: $inRecommendationsOfCoursesIds,
      inRecommendationsOfCyclesIds: $inRecommendationsOfCyclesIds,
      inRecommendationsOfLecturesIds: $inRecommendationsOfLecturesIds,
      lecturesIds: $lecturesIds,
      recommendedCoursesIds: $recommendedCoursesIds,
      recommendedCyclesIds: $recommendedCyclesIds,
      recommendedLecturesIds: $recommendedLecturesIds,
      reviewsIds: $reviewsIds,
      tagsIds: $tagsIds,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      anons,
      description,
      buyFull,
      img1240x349,
      img340x192,
      metaDescription,
      metaKeywords,
      price,
      public,
      #
      author {
        id,

        firstName,
        lastName,
        email,
        phone,
        role,
      }
      #
      lectures {
        id,
        title,
        events {
          id,
          date,
          price,
          lecturers {
            id,
            firstName,
            lastName,
          },
          location {
            id,
            title,
            address,
            metro,
          },
        },
      },
      #
      alias {
        id,
        alias,
      },
      #
      metaTags {
        id,
        metaKeywords,
        metaDescription,
        title,
      },
      #
      reviews {
        id,
        user {
          id,
          firstName,
          lastName,
        },
      }
      #
      tags {
        id,
        title,
        color,
        textColor,
      }
      #
      recommendedCourses {
        id,
        title,
      },
      #
      recommendedCycles {
        id,
        title,
      },
      #
      recommendedLectures {
        id,
        title,
      }
      inRecommendationsOfCourses {
        id,
        title,
      },
      #
      inRecommendationsOfCycles {
        id,
        title,
      },
      #
      inRecommendationsOfLectures {
        id,
        title,
      },
      #
    }
  }
`;
