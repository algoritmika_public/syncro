import gql from 'graphql-tag';

export default gql`
  query AllCertificateProductQuery($filter: CertificateProductFilter, $first: Int, $skip: Int, $orderBy: CertificateProductOrderBy) {
    allCertificateProducts(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lectures,
      months,
      price,
      prefix,
      type,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
      certificates {
        id,
        title,
        start,
        end,
        user {
          id,
          firstName,
          lastName,
        }
      }
      _certificatesMeta {
        count
      }
      _discountsMeta {
        count
      }
      #
    },
    _allCertificateProductsMeta {
      count,
    },
  }
`;
