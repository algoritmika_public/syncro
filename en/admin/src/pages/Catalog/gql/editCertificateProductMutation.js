import gql from 'graphql-tag';

export default gql `
  mutation UpdateCertificateProductMutation(
      $id: ID!,
      $lectures: Int!,
      $months: Int!,
      $price: Float!,
      $title: String!,
      $prefix: String!,
      $type: SubProductTypes,
      $discountsIds: [ID!],
    ){
    updateCertificateProduct(
      id: $id,

      lectures: $lectures,
      months: $months,
      price: $price,
      prefix: $prefix,
      title: $title,
      type: $type,
      discountsIds: $discountsIds,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lectures,
      months,
      price,
      type,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
      certificates {
        id,
        title,
        start,
        end,
        user {
          id,

          firstName,
          lastName,
        }
      }
      _certificatesMeta {
        count
      }
      _discountsMeta {
        count
      }
      #
    }
  }
`;
