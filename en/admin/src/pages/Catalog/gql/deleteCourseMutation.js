import gql from 'graphql-tag';

export default gql`
  mutation deleteCourseMutation(
      $id: ID!
    ){
    deleteCourse(
      id: $id,
    ) {
      id
    }
  }
`;
