import gql from 'graphql-tag';

export default gql`
  mutation deleteCycleMutation(
      $id: ID!
    ){
    deleteCycle(
      id: $id,
    ) {
      id
    }
  }
`;
