import gql from 'graphql-tag';

export default gql `
  mutation UpdateLectureMutation(
      $id: ID!,
      $anons: String,
      $anonsForCourse: String,
      $anonsForCycle: String,
      $description: String,
      $duration: Float,
      $img1240x349: String,
      $img340x192: String,
      $img340x340: String,
      $metaDescription: String,
      $metaKeywords: String,
      $public: Boolean,
      $subTitle: String,
      $themes: [String!]
      $title: String,
      $titleForCourse: String,
      $titleForCycle: String,
      $aliasId: ID,
      $authorId: ID,
      $metaTagsId: ID,
      $courseIds: [ID!],
      $cycleIds: [ID!],
      $eventsIds: [ID!],
      $inRecommendationsOfLecturesIds: [ID!],
      $recommendedLecturesIds: [ID!],
      $reviewsIds: [ID!],
      $tagsIds: [ID!],
    ) {
    updateLecture(
      id: $id,
      anons: $anons,
      anonsForCourse: $anonsForCourse,
      anonsForCycle: $anonsForCycle,
      description: $description,
      duration: $duration,
      img1240x349: $img1240x349,
      img340x192: $img340x192,
      img340x340: $img340x340,
      metaDescription: $metaDescription,
      metaKeywords: $metaKeywords,
      public: $public,
      subTitle: $subTitle,
      themes: $themes,
      title: $title,
      titleForCourse: $titleForCourse,
      titleForCycle: $titleForCycle,
      aliasId: $aliasId,
      authorId: $authorId,
      metaTagsId: $metaTagsId,
      courseIds: $courseIds,
      cycleIds: $cycleIds,
      eventsIds: $eventsIds,
      inRecommendationsOfLecturesIds: $inRecommendationsOfLecturesIds,
      recommendedLecturesIds: $recommendedLecturesIds,
      reviewsIds: $reviewsIds,
      tagsIds: $tagsIds,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      titleForCourse,
      titleForCycle,
      anons,
      anonsForCourse,
      anonsForCycle,
      public,
      description,
      themes,
      duration,
      img1240x349,
      img340x192,
      img340x340,
      metaDescription,
      metaKeywords,
      #
      author {
        id,

        firstName,
        lastName,
        email,
        phone,
        role,
      },
      #
      alias {
        id,
        alias,
      },
      #
      course {
        id,
        title,
      },
      #
      cycle {
        id,
        title,
      },
      #
      events {
        id,
        date,
        price,
        lecturers {
          id,
          firstName,
          lastName,
        },
        location {
          id,
          title,
          address,
          metro,
        },
        curator {
          id,

          firstName,
          lastName,
        }
      },
      #
      reviews {
        id,
        user {
          id,
          firstName,
          lastName,
        },
      },
      #
      tags {
        id,
        title,
        color,
        textColor,
      },
      #
      metaTags {
        id,
        metaKeywords,
        metaDescription,
        title,
      },
      #
      recommendedLectures {
        id,
        title,
      },
      #
      inRecommendationsOfLectures {
        id,
        title,
      },
      #
    }
  }
`;
