import { format } from './../../../utils';

const createCourse = [
  {
    id: 'author',
    label: 'Автор',
    type: 'author',
    disabled: true,
    hidden: true,
    format: format.authorToSting,
  },
  // {
  //   id: 'authorId',
  //   label: 'Автор',
  //   type: 'text',
  //   disabled: true,
  //   hidden: true,
  // },
  {
    id: 'title',
    label: 'Заголовок',
    required: true,
    type: 'multiline',
  },
  {
    id: 'subTitle',
    label: 'Подзаголовок',
    type: 'multiline',
  },
  {
    id: 'anons',
    label: 'Анонс',
    type: 'multiline',
  },
  {
    id: 'description',
    label: 'Описание',
    type: 'multiline',
  },

  {
    id: 'price',
    label: 'Цена',
    required: true,
    type: 'number',
  },
  {
    id: 'buyFull',
    label: 'Продажа только полного курса',
    type: 'checkbox',
  },
  {
    id: 'metaDescription',
    label: 'Meta - Description',
    type: 'multiline',
  },
  {
    id: 'metaKeywords',
    label: 'Meta - Keywords',
    type: 'multiline',
  },
  //
  {
    id: 'img1240x349',
    label: 'Картинка 1240х349',
    type: 'image',
  },
  {
    id: 'img340x192',
    label: 'Картинка 340х192',
    type: 'image',
  },
  {
    id: 'tagsIds',
    label: 'Направления',
    relation: 'tags',
    type: 'relation',
    required: true,
    disabled: true,
  },
  {
    id: 'alias',
    label: 'URL alias',
    type: 'alias',
    required: true,
  },
  {
    id: 'discountsIds',
    label: 'Скидки',
    relation: 'discounts-select',
    type: 'relation',
    disabled: true,
  },
  {
    id: 'lecturesIds',
    label: 'Лекции',
    relation: 'lectures',
    type: 'relation',
    required: true,
    disabled: true,
  },
  // {
  //   id: 'recommendedCoursesIds',
  //   label: 'Рекомендованные курсы',
  //   relation: 'recommendedCourses',
  //   type: 'relation',
  //   disabled: true,
  // },
  // {
  //   id: 'recommendedCyclesIds',
  //   label: 'Рекомендованные циклы',
  //   relation: 'recommendedCycles',
  //   type: 'relation',
  //   disabled: true,
  // },
  {
    id: 'recommendedLecturesIds',
    label: 'Рекомендованные лекции',
    relation: 'recommendedLectures',
    type: 'relation',
    disabled: true,
  },
  {
    id: 'public',
    label: 'Опубликовать',
    type: 'checkbox',
    value: false,
  },
];

export default createCourse;
