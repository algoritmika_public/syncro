import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Catalog from './Catalog';
import CatalogMutationMaterial from './CatalogMutationMaterial';
import CatalogViewWithRelations from './CatalogViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const CatalogRoutes = (props) => {
  const { match } = props;
  const { url } = match;
  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Redirect to={`${url}/lectures`} />
          )}
        />
        <Route
          exact
          path={`${url}/lectures`}
          component={rest => (
            <Catalog
              {...props}
              {...rest}
              typeMaterial="lecture"
              title={'Лекции'}
              linkForUpdateMaterial={`${url}/lectureType/update`}
              linkForViewMaterial={`${url}/lectureType/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/courses`}
          component={rest => (
            <Catalog
              {...props}
              {...rest}
              typeMaterial="course"
              title={'Курсы'}
              linkForUpdateMaterial={`${url}/courses/update`}
              linkForViewMaterial={`${url}/courses/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/cycles`}
          component={rest => (
            <Catalog
              {...props}
              {...rest}
              typeMaterial="cycle"
              title={'Циклы'}
              linkForUpdateMaterial={`${url}/cycles/update`}
              linkForViewMaterial={`${url}/cycles/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/certificates`}
          component={rest => (
            <Catalog
              {...props}
              {...rest}
              typeMaterial="certificate"
              title={'Сертификаты'}
              linkForUpdateMaterial={`${url}/certificates/update`}
              linkForViewMaterial={`${url}/certificates/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/subscriptions`}
          component={rest => (
            <Catalog
              {...props}
              {...rest}
              typeMaterial="subscription"
              title={'Абонементы'}
              linkForUpdateMaterial={`${url}/subscriptions/update`}
              linkForViewMaterial={`${url}/subscriptions/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/lectures/create`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="lecture"
              mutationType="create"
              lectureType="lecture"
            />
          )}
        />
        <Route
          exact
          path={`${url}/lectures/create-lecture-for-course`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="lecture"
              mutationType="create"
              lectureType="lectureForCourse"
            />
          )}
        />
        <Route
          exact
          path={`${url}/lectures/create-lecture-for-cycle`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="lecture"
              mutationType="create"
              lectureType="lectureForCycle"
            />
          )}
        />
        <Route
          exact
          path={`${url}/lectures/update/:id`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="lecture"
              mutationType="update"
            />
          )}
        />

        <Route
          exact
          path={`${url}/lectures/view/:id`}
          component={rest => (
            <CatalogViewWithRelations
              {...props}
              {...rest}
              typeMaterial="lecture"
            />
          )}
        />

        <Route
          exact
          path={`${url}/lectures-for-course/update/:id`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="lecture"
              mutationType="update"
              lectureType="lectureForCourse"
            />
          )}
        />

        <Route
          exact
          path={`${url}/lectures-for-course/view/:id`}
          component={rest => (
            <CatalogViewWithRelations
              {...props}
              {...rest}
              typeMaterial="lecture"
              lectureType="lectureForCourse"
            />
          )}
        />

        <Route
          exact
          path={`${url}/lectures-for-cycle/update/:id`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="lecture"
              mutationType="update"
              lectureType="lectureForCycle"
            />
          )}
        />

        <Route
          exact
          path={`${url}/lectures-for-cycle/view/:id`}
          component={rest => (
            <CatalogViewWithRelations
              {...props}
              {...rest}
              typeMaterial="lecture"
              lectureType="lectureForCycle"
            />
          )}
        />

        <Route
          exact
          path={`${url}/courses/create`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="course"
              mutationType="create"
            />
          )}
        />
        <Route
          exact
          path={`${url}/courses/update/:id`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="course"
              mutationType="edit"
            />
          )}
        />
        <Route
          exact
          path={`${url}/courses/view/:id`}
          component={rest => (
            <CatalogViewWithRelations
              {...props}
              {...rest}
              typeMaterial="course"
            />
          )}
        />

        <Route
          exact
          path={`${url}/cycles/create`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="cycle"
              mutationType="create"
            />
          )}
        />
        <Route
          exact
          path={`${url}/cycles/update/:id`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="cycle"
              mutationType="update"
            />
          )}
        />
        <Route
          exact
          path={`${url}/cycles/view/:id`}
          component={rest => (
            <CatalogViewWithRelations
              {...props}
              {...rest}
              typeMaterial="cycle"
            />
          )}
        />

        <Route
          exact
          path={`${url}/certificates/create`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="certificate"
              mutationType="create"
            />
          )}
        />
        <Route
          exact
          path={`${url}/certificates/update/:id`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="certificate"
              mutationType="update"
            />
          )}
        />

        <Route
          exact
          path={`${url}/subscriptions/create`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="subscription"
              mutationType="create"
            />
          )}
        />
        <Route
          exact
          path={`${url}/subscriptions/update/:id`}
          component={rest => (
            <CatalogMutationMaterial
              {...props}
              {...rest}
              typeMaterial="subscription"
              mutationType="update"
            />
          )}
        />

        <Route
          component={rest => (
            <NotFound {...props} {...rest} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(CatalogRoutes, { title: 'Каталог' });
export default CatalogRoutes;
