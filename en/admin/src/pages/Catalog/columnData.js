import * as R from 'ramda';
import { format } from './../../utils';

const columnData = [
  {
    id: 'title',
    numeric: false,
    disablePadding: false,
    label: 'Заголовок',
  },
  {
    id: 'tags',
    numeric: false,
    disablePadding: false,
    label: 'Направление',
    format: format.tagsToTitleString,
  },
  {
    id: 'lecturers',
    numeric: false,
    disablePadding: false,
    label: 'Лекторы',
    format: format.lectureleLecturersToTitleString,
  },
  {
    id: 'location',
    numeric: false,
    disablePadding: false,
    label: 'Локации',
    format: format.lectureLocationToTitleString,
  },
  {
    id: 'date',
    numeric: false,
    disablePadding: false,
    label: 'Даты',
    format: format.lectureDateToTitleString,
  },
  {
    id: 'price',
    numeric: false,
    disablePadding: false,
    label: 'Цена',
  },
  {
    id: 'author',
    numeric: false,
    disablePadding: false,
    label: 'Автор',
    format: format.authorToSting,
  },
  {
    id: 'createdAt',
    numeric: false,
    disablePadding: false,
    label: 'Дата создания',
    format: format.createdAt,
  },
];

const lectures = R.clone(columnData);
const lecturesAll = R.clone(columnData);
const lecturesForCourse = R.clone(columnData);
const lecturesForCycle = R.clone(columnData);
const courses = R.clone(columnData).map((field) => {
  switch (field.id) {
    case 'lecturers': {
      return Object.assign({}, field, { format: format.courseLecturersToTitleString });
    }
    case 'location': {
      return Object.assign({}, field, { format: format.courseLocationToTitleString });
    }
    case 'date': {
      return Object.assign({}, field, { format: format.courseDateToTitleString });
    }
    default: {
      return field;
    }
  }
});
courses.push({
  id: 'lectures',
  numeric: false,
  disablePadding: false,
  label: 'Лекции',
  format: format.courseLecturesToTitleString,
});
const cycles = R.clone(courses);

lecturesForCourse.push({
  id: 'courses',
  numeric: false,
  disablePadding: false,
  label: 'Курс',
});

lecturesForCycle.push({
  id: 'cycles',
  numeric: false,
  disablePadding: false,
  label: 'Цикл',
});

lecturesAll.push({
  id: 'courses',
  numeric: false,
  disablePadding: false,
  label: 'Курс',
});

lecturesAll.push({
  id: 'cycles',
  numeric: false,
  disablePadding: false,
  label: 'Цикл',
});

// cycles.push({
//   id: 'lectures',
//   numeric: false,
//   disablePadding: false,
//   label: 'Лекции',
// });

const subscriptions = [
  {
    id: 'title',
    numeric: false,
    disablePadding: false,
    label: 'Название',
  },
  {
    id: 'lectures',
    numeric: false,
    disablePadding: false,
    label: 'Количество лекций',
  },
  {
    id: 'months',
    numeric: false,
    disablePadding: false,
    label: 'Срок действия(месяцев)',
  },
  {
    id: 'discounts',
    numeric: false,
    disablePadding: false,
    label: 'Скидки',
    format: format.discountsToString,
  },
];

const certificates = subscriptions.slice();

export default {};
export { lectures };
export { lecturesAll };
export { lecturesForCourse };
export { lecturesForCycle };
export { courses };
export { cycles };
export { subscriptions };
export { certificates };
