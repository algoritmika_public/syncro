import React from 'react';

import gql from './gql';

import {
  ACQUIRING_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import acquiringsFields from './fields';

const AcquiringViewWithRelations = (props) => {
  const gqlQueryWithRelationsName = ACQUIRING_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Acquiring';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={acquiringsFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
      />
    </div>
  );
};

export default AcquiringViewWithRelations;
