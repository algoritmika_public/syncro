import gql from 'graphql-tag';

export default gql`
  mutation UpdateReviewMutation(
      $id: ID!,
    ){
    updateReview(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
    }
  }
`;
