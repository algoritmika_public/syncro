import { format } from './../../utils';

const columnData = [
  {
    id: 'userId',
    numeric: false,
    disablePadding: false,
    label: 'Пользователь',
  },
  {
    id: 'lectureId',
    label: 'Лекция',
  },
  {
    id: 'eventId',
    label: 'Дата мероприятия',
  },
  {
    id: 'anons',
    label: 'Основной текст',
  },
  {
    id: 'description',
    label: 'Основной текст',
  },
  {
    id: 'createdAt',
    label: 'Дата создания',
  },
];
export default columnData;
