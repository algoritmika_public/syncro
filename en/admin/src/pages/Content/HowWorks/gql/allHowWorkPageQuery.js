import gql from 'graphql-tag';

export default gql`
  query AllHowWorkPages($filter: HowWorkPageFilter) {
    allHowWorkPages(
      filter: $filter,
      first: 1,
    ) {
      id
      createdAt
      updatedAt

      page

      titleFirstBlock
      textFirstBlock

      titleSecondBlock
      textSecondBlock

      titleThreeBlock
      textThreeBlock

      titleFourBlock
      textFourBlock
    }
  }
`;
