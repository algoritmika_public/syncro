import React from 'react';

import { output as outputUtils } from './../../../utils';
import gql from './gql';

import {
  CREATE_IMAGE_MUTATION,
  UPDATE_IMAGE_MUTATION,
  IMAGE_QUERY,
} from './../../../constants';

import MutationMaterial from './../../../components/MutationMaterial';

import imagesFields from './fields';
import imagesActions from './actions';


const MainGalleryMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_IMAGE_MUTATION;
    fields = imagesFields.create;
    mutationResultObjectName = 'createImage';
    resulMutatiomtMessageSuccses = 'Изображение создано';
  } else {
    gqlMutationName = UPDATE_IMAGE_MUTATION;
    fields = imagesFields.update;
    mutationResultObjectName = 'updateImage';
    resulMutatiomtMessageSuccses = 'Изображение обновлено';
    gqlQueryName = IMAGE_QUERY;
    gqlQueryItemName = 'Image';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
        // showActions={false}
      />
    </div>
  );
};

export default MainGalleryMutation;
