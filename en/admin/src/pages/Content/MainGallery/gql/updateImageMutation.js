import gql from 'graphql-tag';

export default gql`
  mutation UpdateImageMutation(
      $id: ID!
      $secret: String!,
      $galleryId: ID,
      $description: String,
    ){
    updateImage(
      id: $id,
      secret: $secret,
      galleryId: $galleryId,
      description: $description,
    ) {
      id
      createdAt
      updatedAt

      secret
      description
    }
  }
`;
