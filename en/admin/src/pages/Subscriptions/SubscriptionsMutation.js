import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_SUBSCRIPTION_MUTATION,
  EDIT_SUBSCRIPTION_MUTATION,
  SUBSCRIPTION_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import subscriptionsFields from './fields';
import subscriptionsActions from './actions';


const SubscriptionsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_SUBSCRIPTION_MUTATION;
    fields = subscriptionsFields.create;
    mutationResultObjectName = 'createSubscription';
    resulMutatiomtMessageSuccses = 'Сертификат создан';
  } else {
    gqlMutationName = EDIT_SUBSCRIPTION_MUTATION;
    fields = subscriptionsFields.update;
    mutationResultObjectName = 'updateSubscription';
    resulMutatiomtMessageSuccses = 'Сертификат обновлен';
    gqlQueryName = SUBSCRIPTION_QUERY;
    gqlQueryItemName = 'Subscription';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default SubscriptionsMutation;
