import gql from 'graphql-tag';

export default gql`
  query subscriptionQueryWithRelations($id: ID!) {
    Subscription(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

    }
  }
`;
