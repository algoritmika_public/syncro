import {
  CREATE_SUBSCRIPTION_MUTATION,
  ALL_SUBSCRIPTIONS_QUERY,
  EDIT_SUBSCRIPTION_MUTATION,
  SUBSCRIPTION_QUERY,
  DELETE_SUBSCRIPTION_MUTATION,
  SUBSCRIPTION_QUERY_WITH_RELATIONS,
} from './../../../constants';
import createSubscriptionMutation from './createSubscriptionMutation';
import allSubscriptionsQuery from './allSubscriptionsQuery';
import editSubscriptionMutation from './editSubscriptionMutation';
import subscriptionQuery from './subscriptionQuery';
import deleteSubscriptionMutation from './deleteSubscriptionMutation';
import subscriptionQueryWithRelations from './subscriptionQueryWithRelations';

const gql = {
  [CREATE_SUBSCRIPTION_MUTATION]: createSubscriptionMutation,
  [ALL_SUBSCRIPTIONS_QUERY]: allSubscriptionsQuery,
  [EDIT_SUBSCRIPTION_MUTATION]: editSubscriptionMutation,
  [SUBSCRIPTION_QUERY]: subscriptionQuery,
  [DELETE_SUBSCRIPTION_MUTATION]: deleteSubscriptionMutation,
  [SUBSCRIPTION_QUERY_WITH_RELATIONS]: subscriptionQueryWithRelations,
};

export default gql;
