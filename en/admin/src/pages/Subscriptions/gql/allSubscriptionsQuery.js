import gql from 'graphql-tag';

export default gql`
  query AllSubscriptionsQuery($filter: SubscriptionFilter, $first: Int, $skip: Int, $orderBy: SubscriptionOrderBy) {
    allSubscriptions(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
    }
    _allSubscriptionsMeta {
      count
    }
  }
`;
