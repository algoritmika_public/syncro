const filterModifier = (input = {}) => {
  const {
    firstName,
    lastName,
    tagsIds,
  } = input;
  const filter = {
    firstName,
    lastName,
  };

  if (tagsIds && tagsIds.length > 0) {
    filter.tags_some = {
      id_in: tagsIds,
    };
  }

  return filter;
};

export default filterModifier;
