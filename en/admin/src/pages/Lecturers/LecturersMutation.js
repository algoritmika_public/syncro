import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_LECTURER_MUTATION,
  EDIT_LECTURER_MUTATION,
  LECTURER_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import tagsFields from './fields';
import tagsActions from './actions';


const LecturersMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_LECTURER_MUTATION;
    fields = tagsFields.create;
    mutationResultObjectName = 'createLecturer';
    resulMutatiomtMessageSuccses = 'Лектор создан';
  } else {
    gqlMutationName = EDIT_LECTURER_MUTATION;
    fields = tagsFields.update;
    mutationResultObjectName = 'updateLecturer';
    resulMutatiomtMessageSuccses = 'Лектор обновлен';
    gqlQueryName = LECTURER_QUERY;
    gqlQueryItemName = 'Lecturer';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
        // showActions={false}
      />
    </div>
  );
};

export default LecturersMutation;
