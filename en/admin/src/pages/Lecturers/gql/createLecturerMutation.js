import gql from 'graphql-tag';

export default gql`
  mutation CreateLecturerMutation(
      $anons: String!
      $alias: LectureraliasAlias
      $anonsForLecture: String
      $description: String!
      $firstName: String!
      $img160x160: String!
      $img337x536: String!
      $img340x225: String!
      $lastName: String!
      $skills: [String!]!
      $specialization: String!
      $eventsIds: [ID!]
      $tagsIds: [ID!]
    ){
    createLecturer(
      anons: $anons,
      anonsForLecture: $anonsForLecture,
      description: $description,
      firstName: $firstName,
      img160x160: $img160x160,
      img337x536: $img337x536,
      img340x225: $img340x225,
      lastName: $lastName,
      skills: $skills,
      specialization: $specialization,
      eventsIds: $eventsIds,
      tagsIds: $tagsIds,
      alias: $alias,
    ) {
      id,
      createdAt,
      updatedAt,
      anons,
      alias {
        id,
        alias,
      },
      anonsForLecture,
      description,
      events {
        id,
        lecture {
          id,
        },
      }
      firstName,
      img160x160,
      img337x536,
      img340x225,
      lastName,
      skills,
      specialization,
      tags {
        id,
        title
      }
    }
  }
`;
