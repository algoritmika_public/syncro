import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Lecturers from './Lecturers';
import LecturersMutation from './LecturersMutation';
import LecturerViewWithRelations from './LecturerViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const LecturersRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Lecturers
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <LecturersMutation {...props} {...rest} mutationType="create" title={'Новый лектор'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <LecturersMutation {...props} {...rest} mutationType="update" title={'Редактирование лектора'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <LecturerViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(LecturersRoutes, { title: 'Лекторы' });
export default LecturersRoutes;
