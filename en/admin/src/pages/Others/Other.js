import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { view } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';
import { OTHER_RELATIONS_QUERY } from './../../constants';

const Form = withProgress([OTHER_RELATIONS_QUERY])(FormBase);


class EditOther extends Component {
  render() {
    const other = this.props[OTHER_RELATIONS_QUERY].Other;
    let fields = null;
    if (other) {
      fields = R.clone(view);
      Object.keys(fields).forEach((key) => {
        fields[key].value = other[key] || 'Нет данных';
      });
    }

    return (
      <div>
        <Form
          formTitle={other ? other.title : ''}
          fields={fields || view}
          type={'edit'}
          disabledAll={false}
          showActions={{}}
          req={this.props[OTHER_RELATIONS_QUERY].loading}
          {...this.props}
        />
      </div>
    );
  }
}

export default compose(
  graphql(gql[OTHER_RELATIONS_QUERY], {
    name: OTHER_RELATIONS_QUERY,
    options(ownProps) {
      const id = ownProps.match.params.id;
      return (
        {
          variables: {
            id,
          },
        }
      );
    },
  }),
)(EditOther);
