import React, { Component } from 'react';
import { graphql, compose, withApollo } from 'react-apollo';

import { format } from './../../utils';

import ExtTable from './../../components/ExtTable';
import Filter from './../../components/Filter';
import DialogDeleteItem from './../../components/DialogDeleteItem';

import { filter } from './fields';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';
import gql from './gql';
import { ALL_OTHERS_QUERY, ITEMS_PER_PAGE, DELETE_OTHER_MUTATION } from './../../constants';

const Table = withProgress([ALL_OTHERS_QUERY])(ExtTable);

const columnData = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'descrition', numeric: false, disablePadding: false, label: 'Описание' },
];

const deleteOtherMutationResult = `${DELETE_OTHER_MUTATION}Result`;
const deleteOtherMutationError = `${DELETE_OTHER_MUTATION}Error`;
const deleteOtherMutationLoading = `${DELETE_OTHER_MUTATION}Loading`;
const deleteOtherMutationReset = `${DELETE_OTHER_MUTATION}Reset`;

class OtherList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemToDelete: null,
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.handleView = this.handleView.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[deleteOtherMutationResult]) {
      const id = nextProps[deleteOtherMutationResult].data.deleteOther.id
      this.props.snackbar({ message: `Материал удален - id: ${id}`, name: 'Success' });
      this.setState({
        req: false,
        itemToDelete: null,
      }, () => {
        this.props[ALL_OTHERS_QUERY].refetch();
      });
    }
    if (nextProps[deleteOtherMutationError]) {
      this.props.snackbar(nextProps[deleteOtherMutationError]);
      this.props[deleteOtherMutationReset]();
      this.setState({
        req: false,
      });
    }
  }
  handleView(id) {
    this.props.history.push(`${this.props.match.url}/view/${id}`)
  }
  handleSubmit(fields) {
    this.props[ALL_OTHERS_QUERY].refetch({ filter: fields });
    // this.setState({ links })
  }
  handleEdit(id) {
    const {
      match,
      history,
    } = this.props;
    history.push(`${match.url}/edit/${id}`);
  }
  handleDelete(n) {
    this.setState({
      itemToDelete: n,
    });
  }
  handleCancelDelete() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleDeleteConfirm = async () => {
    const { itemToDelete } = this.state;
    this.setState({
      req: true,
    }, () => {
      this.props[DELETE_OTHER_MUTATION]({
        variables: {
          id: itemToDelete.id,
        },
      });
    });
  }
  render() {
    const othersToRender = this.props[ALL_OTHERS_QUERY].allOthers;
    const { itemToDelete, req } = this.state;

    return (
      <div>
        <Filter
          fields={filter}
          onChange={(fields) => { }}
          onSubmit={this.handleSubmit}
        />
        <Table
          {...this.props}
          columnData={columnData}
          data={othersToRender}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
          onView={this.handleView}
        />
        <DialogDeleteItem
          open={itemToDelete ? true : false}
          title={itemToDelete ? itemToDelete.title : ''}
          onCancel={this.handleCancelDelete}
          onConfirm={this.handleDeleteConfirm}
          disabled={req}
        />
      </div>
    )
  }
}

OtherList.defaultProps = {
  [ALL_OTHERS_QUERY]: {
    allOthers: [],
  },
};

export default compose(
  graphql(gql[ALL_OTHERS_QUERY], {
    name: [ALL_OTHERS_QUERY],
  }),
  graphql(gql[DELETE_OTHER_MUTATION], { name: DELETE_OTHER_MUTATION }),
  withStateMutation({ name: DELETE_OTHER_MUTATION }),
  withSnackbar(),
  // withProgress([ALL_OTHERS_QUERY]),
)(OtherList);
