import gql from 'graphql-tag';

export default gql`
  mutation deleteOtherMutation(
      $id: ID!
    ){
    deleteOther(
      id: $id,
    ) {
      id
    }
  }
`;
