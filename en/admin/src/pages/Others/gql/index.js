import {
  CREATE_OTHER_MUTATION,
  ALL_OTHERS_QUERY,
  EDIT_OTHER_MUTATION,
  OTHER_QUERY,
  DELETE_OTHER_MUTATION,
  OTHER_RELATIONS_QUERY,
} from './../../../constants';

import createOtherMutation from './createOtherMutation';
import allOthersQuery from './allOthersQuery';
import editOtherMutation from './editOtherMutation';
import otherQuery from './otherQuery';
import deleteOtherMutation from './deleteOtherMutation';
import otherRelationQuery from './otherRelationQuery';

const gql = {
  [CREATE_OTHER_MUTATION]: createOtherMutation,
  [DELETE_OTHER_MUTATION]: deleteOtherMutation,
  [EDIT_OTHER_MUTATION]: editOtherMutation,
  [ALL_OTHERS_QUERY]: allOthersQuery,
  [OTHER_QUERY]: otherQuery,
  [OTHER_RELATIONS_QUERY]: otherRelationQuery,
};

export default gql;
