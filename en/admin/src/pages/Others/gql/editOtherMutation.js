import gql from 'graphql-tag';

export default gql`
  mutation UpdateOtherMutation(
      $id: ID!,
      $title: String!
      $description: String!
    ){
    updateOther(
      id: $id,
      title: $title,
      description: $description,
    ) {
      id,
      createdAt,
      updatedAt,
      title,
      description,
    }
  }
`;
