import gql from 'graphql-tag';

export default gql`
  query AllOthersQuery($filter: OtherFilter, $first: Int, $skip: Int, $orderBy: OtherOrderBy) {
    allOthers(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
      title,
      description,
    }
    _allOthersMeta {
      count
    }
  }
`;
