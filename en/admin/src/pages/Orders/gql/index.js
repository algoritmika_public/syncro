import {
  CREATE_ORDER_MUTATION,
  ALL_ORDERS_QUERY,
  EDIT_ORDER_MUTATION,
  ORDER_QUERY,
  DELETE_ORDER_MUTATION,
  ORDER_QUERY_WITH_RELATIONS,
} from './../../../constants';
import createOrderMutation from './createOrderMutation';
import allOrdersQuery from './allOrdersQuery';
import editOrderMutation from './editOrderMutation';
import orderQuery from './orderQuery';
import deleteOrderMutation from './deleteOrderMutation';
import orderQueryWithRelations from './orderQueryWithRelations';

const gql = {
  [CREATE_ORDER_MUTATION]: createOrderMutation,
  [ALL_ORDERS_QUERY]: allOrdersQuery,
  [EDIT_ORDER_MUTATION]: editOrderMutation,
  [ORDER_QUERY]: orderQuery,
  [DELETE_ORDER_MUTATION]: deleteOrderMutation,
  [ORDER_QUERY_WITH_RELATIONS]: orderQueryWithRelations,
};

export default gql;
