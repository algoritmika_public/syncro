import React from 'react';

import gql from './gql';

import {
  ORDER_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import ordersFields from './fields';

const OrderViewWithRelations = (props) => {
  const gqlQueryWithRelationsName = ORDER_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Order';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={ordersFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
      />
    </div>
  );
};

export default OrderViewWithRelations;
