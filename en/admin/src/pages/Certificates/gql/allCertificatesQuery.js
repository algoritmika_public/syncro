import gql from 'graphql-tag';

export default gql`
  query AllCertificatesQuery($filter: CertificateFilter, $first: Int, $skip: Int, $orderBy: CertificateOrderBy) {
    allCertificates(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
    }
    _allCertificatesMeta {
      count
    }
  }
`;
