import gql from 'graphql-tag';

export default gql`
  mutation CreateCertificateMutation(
      $userId: ID,
    ){
    createCertificate(
      userId: $userId,
    ) {
      id
    }
  }
`;
