import gql from 'graphql-tag';

export default gql`
  query AllUsersQuery($filter: UserFilter, $first: Int, $skip: Int, $orderBy: UserOrderBy) {
    allUsers(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
      email,
      firstName,
      lastName,
      phone,
      role,
    }
    _allUsersMeta {
      count
    }
  }
`;
