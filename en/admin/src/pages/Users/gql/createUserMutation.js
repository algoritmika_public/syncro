import gql from 'graphql-tag';

export default gql`
  mutation signupUserWithRoleMutation(
      $email: String!,
      $firstName: String!,
      $lastName: String!,
      $password: String!,
      $phone: String!,
      $role: String!,
    ){
    signupUserWithRole(
      email: $email,
      firstName: $firstName,
      lastName: $lastName,
      password: $password,
      phone: $phone,
      role: $role,
    ) {
      id
    }
  }
`;
