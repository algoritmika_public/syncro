import React from 'react';
import PropTypes from 'prop-types';
import Divider from 'material-ui/Divider';
import AddIcon from 'material-ui-icons/Add';
import Button from 'material-ui/Button';
import { graphql, compose } from 'react-apollo';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';

import Actions from './../../components/Actions/';
import UserList from './UserList';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});

const actionItemsTitle = (userRole) => {
  switch (userRole) {
    case 'ADMIN': {
      return 'администратора';
    }
    case 'MODERATOR': {
      return 'модератора';
    }
    case 'CURATOR': {
      return 'Координатора';
    }
    default: {
      return 'клиента';
    }
  }
};

const actionItems = ({ url }, userRole) => [
  {
    title: `Создать ${actionItemsTitle(userRole)}`,
    to: `${url}/create`,
  },
];

const List = (props) => {
  const { classes, match, userRole } = props;

  const actions = actionItems(match, userRole);
  return (
    <div>
      <Typography type="title" gutterBottom>
        {props.title}
      </Typography>
      <Actions
        items={actions}
      />
      <UserList {...props} userRole={userRole} />
    </div>
  );
};

List.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(List);
