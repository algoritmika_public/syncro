import React, { Component } from 'react';
import { graphql, compose, withApollo } from 'react-apollo';

import { format } from './../../utils';

import ExtTable from './../../components/ExtTable';
import Filter from './../../components/Filter';

import { filter } from './fields';
import withProgress from './../../hoc/withProgress';
import gql from './gql';
import { ALL_USERS_QUERY, ITEMS_PER_PAGE } from './../../constants';

const Table = withProgress([ALL_USERS_QUERY])(ExtTable);

// createdAt
// email
// firstName
// id
// lastName
// phone
// role
// updatedAt

const columnData = [
  { id: 'firstName', numeric: false, disablePadding: false, label: 'Имя' },
  { id: 'lastName', numeric: false, disablePadding: false, label: 'Фамилия' },
  { id: 'email', numeric: false, disablePadding: false, label: 'E-mail' },
  { id: 'phone', numeric: true, disablePadding: false, label: 'Телефон' },
  { id: 'createdAt', numeric: true, disablePadding: false, label: 'Дата регистрации', format: format.date },
];

class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }
  handleSubmit(fields) {
    this.props[ALL_USERS_QUERY].refetch({ filter: fields });
    // this.setState({ links })
  }
  handleEdit(id) {
    const {
      match,
      history,
    } = this.props;
    history.push(`${match.url}/edit/${id}`);
  }
  render() {
    const usersToRender = this.props[ALL_USERS_QUERY].allUsers;
    filter.role.value = this.props.userRole;

    return (
      <div>
        <Filter
          fields={filter}
          onChange={(fields) => { }}
          onSubmit={this.handleSubmit}
        />
        <Table
          {...this.props}
          columnData={columnData}
          data={usersToRender}
          onEdit={this.handleEdit}
        />
      </div>
    )
  }
}

UserList.defaultProps = {
  [ALL_USERS_QUERY]: {
    allUsers: [],
  },
};

export default compose(
  graphql(gql[ALL_USERS_QUERY], {
    name: [ALL_USERS_QUERY],
    // options: {
    //   variables: { filter: { }, first: 1, skip: 0, orderBy: 'createdAt_DESC' },
    // },
    options(ownProps) {
      return (
        {
          variables: {
            filter: {
              role: ownProps.userRole,
            },
            orderBy: 'createdAt_DESC',
          },
        }
      );
    },
    // options: (ownProps) => {
    //   const page = parseInt(ownProps.match.params.page, 10);
    //   const isNewPage = ownProps.location.pathname.includes('new');
    //   const skip = isNewPage ? (page - 1) * ITEMS_PER_PAGE : 0;
    //   const first = isNewPage ? ITEMS_PER_PAGE : 1000;
    //   const orderBy = isNewPage ? 'createdAt_DESC' : null;
    //   const filter = ownProps.filter;
    //   return {
    //     variables: { filter, first, skip, orderBy },
    //   };
    // },
  }),
  // withProgress([ALL_USERS_QUERY]),
)(UserList);
