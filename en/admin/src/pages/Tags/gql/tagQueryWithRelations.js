import gql from 'graphql-tag';

export default gql`
  query tagQueryWithRelations($id: ID!) {
    Tag(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      alias {
        id,
        alias,
      }
      color,
      textColor,

      courses {
        id,
        createdAt,
        updatedAt,
        title,

        author {
          id,

          firstName,
          lastName,
        },
      }
      cycles {
        id,
        createdAt,
        updatedAt,
        price,
        title,

        author {
          id,

          firstName,
          lastName,
        },
        tags {
          id,
          title,
        }
        lectures {
          id,
          title,
          events {
            id,
            lecturers {
              id,

              firstName,
              lastName,
            }
            location {
              id,
              title,
            }
          }
        }
      }
      lectures {
        id,
        createdAt,
        updatedAt,
        title,

        author {
          id,

          firstName,
          lastName,
        },
      }
      # lecturers {
      #   id,
      #   createdAt,
      #   updatedAt,
      #
      #   firstName,
      #   lastName,
      # tags {
      #   id,
      #   title,
      # }
      # }
      lecturers {
        id,
        createdAt,
        updatedAt,

        firstName,
        lastName,
        tags {
          id,
          title,
        }
      }

      _coursesMeta {
        count
      }
      _cyclesMeta {
        count
      }
      # _lecturersMeta {
      #   count
      # }
      _lecturersMeta {
        count
      }
      _lecturesMeta {
        count
      }
    }
  }
`;
