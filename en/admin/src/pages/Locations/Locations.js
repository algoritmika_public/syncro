import React from 'react';

import gql from './gql';

import {
  ALL_LOCATIONS_QUERY,
  DELETE_LOCATION_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import locationsFields from './fields';
import locationsActions from './actions';
import locationsColumnData from './columnData';

const Locations = (props) => {

  const gqlAllQueryName = ALL_LOCATIONS_QUERY;
  const gqlAllQueryItemsName = 'allLocations';
  const gqlMutationDelete = DELETE_LOCATION_MUTATION;
  const gqlMutationDeleteItemName = 'deleteLocation';
  const resulMutatiomtMessageSuccses = 'Направление удалено';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={locationsActions}
        columnData={locationsColumnData}
        fields={locationsFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Locations;
