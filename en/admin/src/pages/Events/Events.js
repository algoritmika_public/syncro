import React from 'react';

import gql from './gql';

import {
  ALL_EVENTS_QUERY,
  DELETE_EVENT_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import eventsFields from './fields';
import eventsActions from './actions';
import eventsColumnData from './columnData';

import SelectAllQueryShort from './../../components/SelectAllQueryShort';

const Events = (props) => {

  const gqlAllQueryName = ALL_EVENTS_QUERY;
  const gqlAllQueryItemsName = 'allEvents';
  const gqlMutationDelete = DELETE_EVENT_MUTATION;
  const gqlMutationDeleteItemName = 'deleteEvent';
  const resulMutatiomtMessageSuccses = 'Мероприятие удалено';

  return (
    <div>
      <SelectAllQueryShort
        typeMaterial="events"
        typeView="select"
      />
      <SelectAllQueryShort
        typeMaterial="lecturers"
        typeView="select"
      />
      <SelectAllQueryShort
        typeMaterial="locations"
        typeView="select"
      />
      <SelectAllQueryShort
        typeMaterial="tags"
        typeView="select"
      />
      <ListMaterials
        {...props}
        actionsButtons={eventsActions}
        columnData={eventsColumnData}
        fields={eventsFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showTitle={props.showTitle}
        showActions={props.showActions === undefined ? true : props.showActions}
      />
    </div>
  );
};

export default Events;
