import gql from 'graphql-tag';

export default gql`
  query AllEventsQuery($filter: EventFilter, $first: Int, $skip: Int, $orderBy: EventOrderBy) {
    allEvents(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,

      date,
      quantityOfTickets,
      price,
      tickets {
        user {
          id,

          firstName,
          lastName,
        },
      },
      lecture {
        id,

        title,
      },
      lecturers {
        id,

        firstName,
        lastName,
      },
      location {
        id,

        title,
        address,
        metro,
      },
      # orders {
      #   payment {
      #     id,
      #
      #     createdAt,
      #     commission,
      #     certificate {
      #       id
      #     },
      #     subscription {
      #       id
      #     },
      #     type,
      #
      #   },
      #   user {
      #     id,
      #
      #     firstName,
      #     lastName,
      #   },
      # },
      participants {
        id,

        firstName,
        lastName,
      },
      reviews {
        user {
          id,

          firstName,
          lastName,
        },
      },
      _attendingsMeta {
        count
      },
      _lecturersMeta {
        count
      },
      _participantsMeta {
        count
      },
      _reviewsMeta {
        count
      },
    },
    _allEventsMeta {
      count
    },
  }
`;
