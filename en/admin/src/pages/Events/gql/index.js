import {
  CREATE_EVENT_MUTATION,
  EDIT_EVENT_MUTATION,
  ALL_EVENTS_QUERY,
  EVENT_QUERY,
  DELETE_EVENT_MUTATION,
} from './../../../constants';

import createEventMutation from './createEventMutation';
import updateEventMutation from './updateEventMutation';
import allEventsQuery from './allEventsQuery';
import eventQuery from './eventQuery';
import deleteEventMutation from './deleteEventMutation';

const gql = {
  [CREATE_EVENT_MUTATION]: createEventMutation,
  [EDIT_EVENT_MUTATION]: updateEventMutation,
  [ALL_EVENTS_QUERY]: allEventsQuery,
  [EVENT_QUERY]: eventQuery,
  [DELETE_EVENT_MUTATION]: deleteEventMutation,
};

export default gql;
