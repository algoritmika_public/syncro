import gql from 'graphql-tag';

export default gql`
  mutation UpdateGalleryMutation(
      $id: ID!,
      $title: String!,
      $type: String!,
    ){
    updateGallery(
      id: $id,
      title: $title,
      type: $type,
    ) {
      id,
      createdAt,
      updatedAt,

      title
      type
    }
  }
`;
