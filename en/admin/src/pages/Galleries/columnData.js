import { format } from './../../utils';

const columnData = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Название' },
];
export default columnData;
