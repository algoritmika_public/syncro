import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';

import Create from './Create';
import Update from './Update';
import View from './View';
import List from './List';
import ListRefetch from './ListRefetch';
import Relation from './Relation';

import Lecturers from './../../new_pages/Lecturers/Lecturers';
import LecturersMutation from './../../new_pages/Lecturers/LecturersMutation';
import LecturerViewWithRelations from './../../new_pages/Lecturers/LecturerViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const Test = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Lecturers
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/list`}
          component={rest => (
            <List
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/list-refetch`}
          component={rest => (
            <ListRefetch
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <LecturersMutation {...props} {...rest} mutationType="create" title={'Новый лектор'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <LecturersMutation {...props} {...rest} mutationType="update" title={'Редактирование лектора'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <LecturerViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          exact
          path={`${url}/relation`}
          component={rest => (
            <Relation
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(Test, { title: 'Тест' });
export default Test;
