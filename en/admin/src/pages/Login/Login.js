import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import SignInDialog from './SignInDialog';
import validateFields from './validateFields';
import gbl from './gql';
import { GC_USER_ID, GC_USER_ROLE, GC_AUTH_TOKEN, AUTHENTICATE_USER_MUTATION } from './../../constants';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';

const authMutationResult = `${AUTHENTICATE_USER_MUTATION}Result`;
const authMutationError = `${AUTHENTICATE_USER_MUTATION}Error`;
const authMutationLoading = `${AUTHENTICATE_USER_MUTATION}Loading`;
const authMutationReset = `${AUTHENTICATE_USER_MUTATION}Reset`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errors: [],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSignIn = this.handleSignIn.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[authMutationResult]) {
      this.saveUserData(nextProps[authMutationResult]);
    }
    if (nextProps[authMutationError]) {
      this.props.snackbar(nextProps[authMutationError]);
      this.props[`${AUTHENTICATE_USER_MUTATION}Reset`]();
      this.setState({
        email: '',
        password: '',
      })
    }
  }
  handleChange = name => (event) => {
    const state = Object.assign({}, this.state);
    state[name] = event.target.value;

    if (state.errors.length > 0) {
      state.errors = validateFields(state);
    }
    this.setState(state);
  };

  handleSignIn(event) {
    event.preventDefault();
    const state = Object.assign({}, this.state);
    state.errors = validateFields(state);
    if (state.errors.length > 0) {
      this.setState(state);
      return;
    }
    this.confirm();
  }

  confirm = async () => {
    const { email, password } = this.state;
    const result = await this.props.authenticateUserMutation({
      variables: {
        email,
        password,
      },
    });

    const { id, role, token } = result.data.authenticateUser;
    this.saveUserData(id, role, token);
    // this.props.history.push(this.props.match.url);
    window.location.reload();
  }

  saveUserData = (id, role, token) => {
    localStorage.setItem(GC_USER_ID, id);
    localStorage.setItem(GC_USER_ROLE, role);
    localStorage.setItem(GC_AUTH_TOKEN, token);
  }

  render() {
    return (
      <div>
        <SignInDialog
          open
          fields={this.state}
          onChange={this.handleChange}
          onSubmit={this.handleSignIn}
          disabled={this.props[authMutationLoading]}
          {...this.props}
        />
      </div>
    );
  }
}

export default compose(
  graphql(gbl[AUTHENTICATE_USER_MUTATION], { name: AUTHENTICATE_USER_MUTATION }),
  // withStateMutation({ name: AUTHENTICATE_USER_MUTATION }),
  withSnackbar())(Login);
