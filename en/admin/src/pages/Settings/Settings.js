import React from 'react';

import gql from './gql';

import {
  ALL_SETTINGS_QUERY,
} from './../../constants';

import MaterialQueryAndMutationRedirect from './../../components/MaterialQueryAndMutationRedirect';

const Settings = (props) => {

  return (
    <div>
      <MaterialQueryAndMutationRedirect
        {...props}
        gql={gql}
        gqlAllQueryName={ALL_SETTINGS_QUERY}
        gqlAllQueryItemsName={'allSettings'}
      />
    </div>
  );
};

export default Settings;
