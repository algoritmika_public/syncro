import validator from 'validator';
import { USER_ROLES } from './../enum';
const hasOwnProperty = (obj, property) => Object.prototype.hasOwnProperty.call(obj, property);

const locale = 'ru-RU';
const userRoles = USER_ROLES.map(role => role.value);

const validate = (input) => {
  const errors = [];
  const email = input.email;
  const password = input.password;
  const repassword = input.repassword;
  const phone = input.phone;
  const firstName = input.firstName;
  const lastName = input.lastName;
  const role = input.role;

  if (hasOwnProperty(input, 'email') && (validator.isEmpty(email) || !validator.isEmail(email))) {
    errors.push('email');
  }

  if (hasOwnProperty(input, 'password') && (validator.isEmpty(password) || !validator.isLength(password, { min: 8 }))) {
    errors.push('password');
  }

  if (hasOwnProperty(input, 'repassword') && (password !== repassword)) {
    errors.push('repassword');
  }

  if (hasOwnProperty(input, 'phone') && (validator.isEmpty(phone) || !validator.isMobilePhone(phone, locale))) {
    errors.push('phone');
  }

  if (hasOwnProperty(input, 'firstName') && (validator.isEmpty(firstName) || !validator.isAlpha(firstName, locale))) {
    errors.push('firstName');
  }

  if (hasOwnProperty(input, 'lastName') && (validator.isEmpty(lastName) || !validator.isAlpha(lastName, locale))) {
    errors.push('lastName');
  }

  if (hasOwnProperty(input, 'role') && (validator.isEmpty(role) || userRoles.indexOf(role) === -1)) {
    errors.push('role');
  }

  return errors;
};

const validateFields = (fields) => {
  const requredFields = {};
  Object.keys(fields).map((key) => {
    const field = fields[key];
    if (field.required) {
      requredFields[key] = field.value;
    }
  });
  return validate(requredFields);
};

export default validateFields;
