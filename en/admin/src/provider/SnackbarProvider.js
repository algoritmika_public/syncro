import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Snackbar from './../new_components/Snackbar';
// import Snackbar from './../components/Snackbar';

class SnackbarProvider extends Component {
  getChildContext() {
    const { brokerSnackbar } = this.props;
    return {
      snackbar: brokerSnackbar.showSnackbar,
    };
  }
  render() {
    return (
      <div>
        {React.Children.only(this.props.children)}
      </div>
    );
  }
}

SnackbarProvider.childContextTypes = {
  snackbar: PropTypes.func,
};

export default SnackbarProvider;
