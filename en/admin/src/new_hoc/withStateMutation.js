import React, { Component } from 'react';
import hoistNonReactStatic from 'hoist-non-react-statics';

const withStateMutation = ({ name = 'mutate' } = {}) => (WrappedComponent) => {
  class WithStateMutation extends Component {
    constructor(props) {
      super(props);
      this.state = {
        loading: false,
        error: null,
        result: null,
      };
      this.loadingProperty = `${name}Loading`;
      this.errorProperty = `${name}Error`;
      this.resultProperty = `${name}Result`;
      this.handleMutationStateReset = this.handleMutationStateReset.bind(this);
      this.handleMutation = this.handleMutation.bind(this);
    }

    handleMutation(options) {
      this.setState({
        loading: true,
        error: null,
        result: null,
      });
      return this.props[name](options)
        .then((result) => {
          this.setState({
            loading: false,
            error: null,
            result,
          });
        })
        .catch((err) => {
          this.setState({
            loading: false,
            error: err,
            result: null,
          });
        });
    }

    handleMutationStateReset() {
      this.setState({
        loading: false,
        error: null,
        result: null,
      });
    }

    render() {
      const props = {
        ...this.props,
        [name]: this.handleMutation,
        [`${name}Reset`]: this.handleMutationStateReset,
        [this.loadingProperty]: this.state.loading,
        [this.errorProperty]: this.state.error,
        [this.resultProperty]: this.state.result,
      };
      return (
        <WrappedComponent {...props} />
      );
    }
  }

  hoistNonReactStatic(withStateMutation, WrappedComponent);

  return WithStateMutation;
};

export default withStateMutation;
