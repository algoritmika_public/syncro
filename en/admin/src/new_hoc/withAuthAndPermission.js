import React, { Component } from 'react';
import { graphql, compose, withApollo } from 'react-apollo'

import config from './../config';

import {
  GC_AUTH_TOKEN,
  GC_USER_ID,
  LOGGED_IN_USER_WITH_ROLE,
  AUTHENTICATE_USER_MUTATION,
  USER_RESET_PASS_SEND_EMAIL_MUTATION,
} from './../constants';
import withStateMutation from './../new_hoc/withStateMutation';
import withSnackbar from './../new_hoc/withSnackbar';

import gql from './../new_pages/Users/gql';
import Login from './../new_components/Login';

const authMutationResult = `${AUTHENTICATE_USER_MUTATION}Result`;
const authMutationError = `${AUTHENTICATE_USER_MUTATION}Error`;
const authMutationLoading = `${AUTHENTICATE_USER_MUTATION}Loading`;
const authMutationReset = `${AUTHENTICATE_USER_MUTATION}Reset`;

const userResetPassSendEmailResult = `${USER_RESET_PASS_SEND_EMAIL_MUTATION}Result`;
const userResetPassSendEmailError = `${USER_RESET_PASS_SEND_EMAIL_MUTATION}Error`;
const userResetPassSendEmailLoading = `${USER_RESET_PASS_SEND_EMAIL_MUTATION}Loading`;
const userResetPassSendEmailReset = `${USER_RESET_PASS_SEND_EMAIL_MUTATION}Reset`;

const withAuthAndPermission = (BaseComponent) => {
  class WithAuthAndPermission extends Component {
    constructor(props) {
      super(props);
      this.state = {
        account: null,
        isRequested: true,
        networkError: false,
      };
      this.handleSignIn = this.handleSignIn.bind(this);
      this.handleResetPass = this.handleResetPass.bind(this);
      this.handleLogOut = this.handleLogOut.bind(this);
      this.handleResetPassSendEmail = this.handleResetPassSendEmail.bind(this);
    }

    componentWillReceiveProps(nextProps) {
      let networkError = false;
      const account = {
        id: null,
        role: null,
      };

      if (nextProps[LOGGED_IN_USER_WITH_ROLE].error) {
        networkError = true;
      }

      const {
        [LOGGED_IN_USER_WITH_ROLE]: data
      } = nextProps;

      if (data[LOGGED_IN_USER_WITH_ROLE]) {
        const { id, role } = data[LOGGED_IN_USER_WITH_ROLE];
        account.id = id;
        account.role = role;
      }

      if (nextProps[authMutationResult] && nextProps[authMutationResult].data) {
        const userData = nextProps[authMutationResult].data.authenticateUser;

        const { id, token } = userData;
        this.saveUserDataToLocal({ id, token });
        account.id = id;
      }

      if (nextProps[userResetPassSendEmailResult] && nextProps[userResetPassSendEmailResult].data) {
        const result = nextProps[userResetPassSendEmailResult].data;
      }

      if (nextProps[authMutationError]) {
        this.props.snackbar(nextProps[authMutationError]);
        this.props[`${AUTHENTICATE_USER_MUTATION}Reset`]();
      }

      const isRequested =
        nextProps[LOGGED_IN_USER_WITH_ROLE].loading
        || nextProps[authMutationLoading]
        || nextProps[userResetPassSendEmailLoading]
        || false;

      this.setState({
        account,
        isRequested,
        networkError,
      });
    }

    handleSignIn(formData) {
      this.setState({
        isRequested: true,
      }, () => {
        this.props[AUTHENTICATE_USER_MUTATION]({
          variables: formData,
        });
      });
    }

    handleResetPassSendEmail(formData) {
      const { origin } = window.location;
      this.setState({
        isRequested: true,
      }, () => {
        this.props[USER_RESET_PASS_SEND_EMAIL_MUTATION]({
          variables: Object.assign({}, formData, { host: `${origin}${config.PATH}` }),
        });
      });
    }

    handleResetPass() {

    }


    handleLogOut() {

    }

    saveUserDataToLocal({ id, token }) {
      localStorage.setItem(GC_USER_ID, id);
      localStorage.setItem(GC_AUTH_TOKEN, token);
    }

    render() {
      const {
        account,
        isRequested,
        networkError,
      } = this.state;
      const props = {
        ...this.props,
        account,
      };
      const authToken = localStorage.getItem(GC_AUTH_TOKEN);

      if (networkError) {
        return (
          <div>Ошибка сети</div>
        );
      }

      if (!authToken || !account || !account.id) {
        return (
          <Login
            {...this.props}
            onSubmitSignIn={this.handleSignIn}
            onSubmitResetPass={this.handleResetPass}
            onSubmitResetPassSendEmail={this.handleResetPassSendEmail}
            isRequested={isRequested}
            authToken={authToken}
            account={account}
            open
          />
        );
      }

      return (
        <BaseComponent {...props} />
      );
    }
  }

  // return withApollo(WithAuthAndPermission);
  return compose(
    graphql(gql[LOGGED_IN_USER_WITH_ROLE], {
      name: LOGGED_IN_USER_WITH_ROLE,
      options: { fetchPolicy: 'network-only' },
    }),
    graphql(gql[AUTHENTICATE_USER_MUTATION], {
      name: AUTHENTICATE_USER_MUTATION,
      options: { fetchPolicy: 'network-only' },
    }),
    graphql(gql[USER_RESET_PASS_SEND_EMAIL_MUTATION], {
      name: USER_RESET_PASS_SEND_EMAIL_MUTATION,
      options: { fetchPolicy: 'network-only' },
    }),
    withStateMutation({ name: AUTHENTICATE_USER_MUTATION }),
    withStateMutation({ name: USER_RESET_PASS_SEND_EMAIL_MUTATION }),
    withSnackbar(),
  )(WithAuthAndPermission);
};

export default withAuthAndPermission;
