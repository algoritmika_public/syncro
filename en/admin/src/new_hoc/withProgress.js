import React, { PureComponent } from 'react';
import wrapDisplayName from 'recompose/wrapDisplayName';
import hoistNonReactStatic from 'hoist-non-react-statics';

import Progress from './../new_components/Progress';

const withProgress = (queries = []) => (BaseComponent) => {
  const WithProgress = props => (
    <div>
      <BaseComponent {...props} />
      <Progress queries={queries} {...props} />
    </div>
  );
  WithProgress.displayName = wrapDisplayName(BaseComponent, 'withProgress');
  hoistNonReactStatic(WithProgress, BaseComponent);

  return WithProgress;
};

export default withProgress;
