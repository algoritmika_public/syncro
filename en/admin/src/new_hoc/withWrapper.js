import React, { PureComponent } from 'react';
import wrapDisplayName from 'recompose/wrapDisplayName';
import hoistNonReactStatic from 'hoist-non-react-statics';
import Typography from 'material-ui/Typography';
import {
  Route,
  Switch,
} from 'react-router-dom';

import AppFrame from './../new_containers/AppFrame';
import AppContent from './../new_containers/AppContent';
import AppContentInner from './../new_containers/AppContentInner';

import pages from './../routes/pages';

const withWrapper = (BaseComponent, options) => {
  const WithWrapper = props => (
    <AppFrame {...props}>
      <AppContent {...props}>
        <AppContentInner {...props}>
          <Switch>
            {
              pages.map(({ exact, path, title }, index) => (
                <Route
                  key={index}
                  exact={exact || false}
                  path={path}
                  component={rest => (
                    <Typography type="display1" gutterBottom>
                      {title || ''}
                    </Typography>
                  )}
                />
              ))
            }
          </Switch>
          <BaseComponent {...props} />
        </AppContentInner>
      </AppContent>
    </AppFrame>
  );

  return WithWrapper;
};

export default withWrapper;
