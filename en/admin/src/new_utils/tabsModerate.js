const tabsModerate = [
  {
    label: 'Активные',
    id: 'active',
  },
  {
    label: 'Не модерированные',
    id: 'not_moderated',
  },
];

export default tabsModerate;
