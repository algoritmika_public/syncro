import formatInputDataForTable from './formatInputDataForTable';

const formatInputDataArrayForTable = data => data.map(n => formatInputDataForTable(n));

export default formatInputDataArrayForTable;
