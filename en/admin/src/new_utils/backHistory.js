export default (history, path) => {
  if (history.length > 0) {
    history.goBack();
  } else {
    history.push(path);
  }
};
