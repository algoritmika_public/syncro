import {
  ALL_EVENTS_QUERY,
} from './../../../constants';

import allEventsQuery from './allEventsQuery';

export default {
  [ALL_EVENTS_QUERY]: allEventsQuery,
};
