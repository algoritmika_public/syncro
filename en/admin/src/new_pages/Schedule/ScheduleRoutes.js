import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Schedule from './Schedule';
import ScheduleViewWithRelations from './ScheduleViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../new_hoc/withWrapper';

const ScheduleRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Schedule
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <ScheduleViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(PaymentsRoutes, { title: 'Скидки' });
export default ScheduleRoutes;
