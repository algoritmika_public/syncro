const modifyQueryFilter = (input = {}, type) => {
  const filter = {};
  if (!input) return {};
  const {
    title,
    address,
    metro,
  } = input;

  if (title) filter.title_contains = title;
  if (address) filter.address_contains = address;
  if (metro) filter.metro_contains = metro;

  return filter;
};

export default modifyQueryFilter;
