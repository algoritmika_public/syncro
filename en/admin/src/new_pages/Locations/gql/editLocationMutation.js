import gql from 'graphql-tag';

export default gql`
  mutation UpdateLocationMutation(
      $id: ID!,
      $metro: String!,
      $title: String!,
      $address: String!,
      $image: String,
    ){
    updateLocation(
      id: $id,
      image: $image,
      metro: $metro,
      title: $title,
      address: $address,
    ) {
      id,
      createdAt,
      updatedAt,
      image,
      metro,
      title,
      address,
    }
  }
`;
