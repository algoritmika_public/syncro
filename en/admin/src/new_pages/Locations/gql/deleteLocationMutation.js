import gql from 'graphql-tag';

export default gql`
  mutation deleteLocationMutation(
      $id: ID!
    ){
    deleteLocation(
      id: $id,
    ) {
      id
    }
  }
`;
