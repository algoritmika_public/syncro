import {
  ALL_LOCATIONS_QUERY,
  ALL_LOCATIONS_QUERY_SHORT,
  CREATE_LOCATION_MUTATION,
  EDIT_LOCATION_MUTATION,
  LOCATION_QUERY,
  DELETE_LOCATION_MUTATION,
  LOCATION_QUERY_WITH_RELATIONS,
} from './../../../constants';
import createLocationMutation from './createLocationMutation';
import allLocationsQuery from './allLocationsQuery';
import allLocationsQueryShort from './allLocationsQueryShort';
import editLocationMutation from './editLocationMutation';
import locationQuery from './locationQuery';
import deleteLocationMutation from './deleteLocationMutation';
import locationQueryWithRelations from './locationQueryWithRelations';

const gql = {
  [ALL_LOCATIONS_QUERY]: allLocationsQuery,
  [ALL_LOCATIONS_QUERY_SHORT]: allLocationsQueryShort,
  [CREATE_LOCATION_MUTATION]: createLocationMutation,
  [EDIT_LOCATION_MUTATION]: editLocationMutation,
  [LOCATION_QUERY]: locationQuery,
  [DELETE_LOCATION_MUTATION]: deleteLocationMutation,
  [LOCATION_QUERY_WITH_RELATIONS]: locationQueryWithRelations,
};

export default gql;
