import gql from 'graphql-tag';

export default gql`
  query locationQuery($id: ID!) {
    Location(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
      image,
      metro,
      title,
      address,
    }
  }
`;
