import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_LOCATION_MUTATION,
  EDIT_LOCATION_MUTATION,
  LOCATION_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import locationsFields from './fields';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const LocationMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_LOCATION_MUTATION;
    fields = locationsFields.create;
    mutationResultObjectName = 'createLocation';
    resultMutationMessageSuccses = 'Локация создана';
  } else {
    gqlMutationName = EDIT_LOCATION_MUTATION;
    fields = locationsFields.update;
    mutationResultObjectName = 'updateLocation';
    resultMutationMessageSuccses = 'Локация обновлена';
    gqlQueryName = LOCATION_QUERY;
    queryResultObjectName = 'Location';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default LocationMutation;
