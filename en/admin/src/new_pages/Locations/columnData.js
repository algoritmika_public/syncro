const columnData = [
  {
    id: 'title',
    label: 'Название',
  }, {
    id: 'metro',
    label: 'Метро',
  }, {
    id: 'address',
    label: 'Адресс',
  }
];
export default columnData;
