import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  ALL_SETTINGS_QUERY,
  SETTING_QUERY,
  CREATE_SETTINGS_MUTATION,
  EDIT_SETTINGS_MUTATION,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import settingsFields from './fields';
import handleSuccessMutation from './handleSuccessMutation';

const SettingsMutation = (props) => {
  const {
    mutationType,
    typeMaterial,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;
  queryResultObjectName = 'Setting';

  if (mutationType === 'create') {
    gqlMutationName = CREATE_SETTINGS_MUTATION;
    fields = settingsFields.createMailchimp;
    resultMutationMessageSuccses = 'Создано';
    mutationResultObjectName = 'createSetting';
  } else {
    gqlQueryName = SETTING_QUERY;
    gqlMutationName = EDIT_SETTINGS_MUTATION;
    mutationResultObjectName = 'updateSetting';
    resultMutationMessageSuccses = 'Обновлено';
    mutationResultObjectName = 'updateSetting';
  }

  const outputModifier = outputUtils.outputMutationModifier;
  switch (typeMaterial) {
    case 'mailchimp': {
      if (mutationType === 'create') {
        fields = settingsFields.createMailchimp;
      } else {
        fields = settingsFields.updateMailchimp;
      }
      break;
    }
    case 'mandrill': {
      if (mutationType === 'create') {
        fields = settingsFields.createMaindrill;
      } else {
        fields = settingsFields.updateMandrill;
      }
      break;
    }
    case 'smtp': {
      if (mutationType === 'create') {
        fields = settingsFields.createSmtp;
      } else {
        fields = settingsFields.updateSmtp;
      }
      break;
    }
    case 'contacts': {
      if (mutationType === 'create') {
        fields = settingsFields.createContacts;
      } else {
        fields = settingsFields.updateContacts;
      }
      break;
    }
    case 'acquiring': {
      if (mutationType === 'create') {
        fields = settingsFields.createAcquiring;
      } else {
        fields = settingsFields.updateAcquiring;
      }
      break;
    }
    default: {
      if (mutationType === 'create') {
        fields = settingsFields.createMailchimp;
      } else {
        fields = settingsFields.updateMailchimp;
      }
    }
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default SettingsMutation;
