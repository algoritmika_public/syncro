import gql from 'graphql-tag';

export default gql`
  query SettingQuery($id: ID!) {
    Setting(
      id: $id
    ) {
      id
      createdAt
      updatedAt

      acquiring {
        id
        title
      }
      contactOfficeAddress
      contactOfficePhone
      contactOfficeTitle
      mailChimpAPIKey
      mailChimpHost
      mailChimpMainList
      mailChimpNewSchedule
      mailChimpReactivationList
      senderEmail
      senderName
      mandrillAPIKey
      mandrillHost
      mandrillTemplateSlugCertificate
      mandrillTemplateSlugCertificateAsGift
      mandrillTemplateSlugConfirmEmail
      mandrillTemplateSlugMaterialsOfLecture
      mandrillTemplateSlugOptRegistration
      mandrillTemplateSlugOrder
      mandrillTemplateSlugOrderWithoutPayment
      mandrillTemplateSlugPayment
      mandrillTemplateSlugRegistration
      mandrillTemplateSlugReminderOfLecture
      mandrillTemplateSlugResetPass
      mandrillTemplateSlugSubscription
      mandrillTemplateSlugTicket
      mandrillTemplateSlugUserBlock
      mandrillTemplateSlugUserUnblock
      smtpHost
      smtpLogin
      smtpPassword
    }
  }
`;
