import React from 'react';

import gql from './gql';

import {
  EDIT_CERTIFICATE_MUTATION,
  CERTIFICATE_QUERY,
} from './../../constants';

import { UpdateCertificateOrder } from './../../new_components/CertificateOrder';
import QueryWithQueryOptions from './../../new_containers/QueryWithQueryOptions';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const modifyQueryOptions = (ownProps) => {
  const { id } = ownProps.match.params;
  return (
    {
      variables: {
        id,
      },
    }
  );
};

const CertificateMutationUpdate = (props) => {
  const {
    match,
    history,
  } = props;
  const gqlMutationName = EDIT_CERTIFICATE_MUTATION;
  const mutationResultObjectName = 'updateCertificate';
  const resultMutationMessageSuccses = 'Сертификат обновлен';
  const gqlQueryName = CERTIFICATE_QUERY;
  const queryResultObjectName = 'Certificate';

  return (
    <div>
      <QueryWithQueryOptions
        {...props}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryOptions={modifyQueryOptions}
        match={match}
        history={history}
      >
        <UpdateCertificateOrder
          {...props}
        />
      </QueryWithQueryOptions>
    </div>
  );
};

export default CertificateMutationUpdate;
