import React from 'react';

import gql from './gql';

import {
  CREATE_CERTIFICATE_MUTATION,
  EDIT_CERTIFICATE_MUTATION,
  CERTIFICATE_QUERY,
} from './../../constants';

import certificatesFields from './fields';

import CertificateOrder from './../../new_components/CertificateOrder';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const CertificatesMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_CERTIFICATE_MUTATION;
    fields = certificatesFields.create;
    mutationResultObjectName = 'createCertificate';
    resultMutationMessageSuccses = 'Сертификат создан';
  } else {
    gqlMutationName = EDIT_CERTIFICATE_MUTATION;
    fields = certificatesFields.update;
    mutationResultObjectName = 'updateCertificate';
    resultMutationMessageSuccses = 'Сертификат обновлен';
    gqlQueryName = CERTIFICATE_QUERY;
    queryResultObjectName = 'Certificate';
  }

  return (
    <div>
      <CertificateOrder
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default CertificatesMutation;
