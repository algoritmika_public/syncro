import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_CERTIFICATES_QUERY,
  DELETE_CERTIFICATE_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import { tabsExpNoPayment } from './../../new_utils';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Certificates = (props) => {

  const gqlQueryName = ALL_CERTIFICATES_QUERY;
  const queryResultObjectName = 'allCertificates';
  const gqlMutationName = DELETE_CERTIFICATE_MUTATION;
  const mutationResultObjectName = 'deleteCertificate';
  const resulMutationMessageSuccses = 'Сертификат удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter.modifyQueryCertificates}
        tabs={tabsExpNoPayment}
      />
    </div>
  );
};

export default Certificates;
