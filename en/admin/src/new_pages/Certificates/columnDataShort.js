const columnDataShort = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'unusedLectures',
    label: 'Остаток лекций',
  },
  {
    id: 'end',
    label: 'Окончание срока действия',
  },
];
export default columnDataShort;
