const create = [];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
);

const view = update.slice();

const filter = [
  {
    id: 'title',
    label: 'Название сертификата',
    type: 'multiline',
  },
  {
    id: 'lecturesCount',
    label: 'Количество лекций',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'firstName',
    label: 'Имя',
    type: 'text',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
    type: 'text',
  },
  {
    id: 'email',
    label: 'E-mail',
    type: 'text',
  },
  {
    id: 'start',
    label: 'Дата покупки',
    type: 'date',
  },
  {
    id: 'end',
    label: 'Дата окончания',
    type: 'date',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
