import gql from 'graphql-tag';

export default gql`
  query LoggedInUserWithRole {
    loggedInUserWithRole {
      id
      role
    }
  }
`;
