import gql from 'graphql-tag';

export default gql`
  query userClientQueryWithRelations($id: ID, $email: String) {
    User(
      id: $id,
      email: $email,
    ) {
      id
      createdAt
      updatedAt
      blocked
      email
      firstName
      lastName
      phone
      role
      blocked
      draft
      coursesCreated {
        id
        title
      }
      lecturesCreated {
        id
        title
      }
      cyclesCreated {
        id
        title
      }
      curator {
        id
        date
        lecture {
          id
          title
        }
      }
    }
  }
`;
