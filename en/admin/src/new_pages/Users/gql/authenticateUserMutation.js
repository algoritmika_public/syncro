import gql from 'graphql-tag';

export default gql`
  mutation AuthenticateUserMutation($email: String!, $password: String!) {
    authenticateUser(
      email: $email,
      password: $password
    ) {
      token
      id
    }
  }
`;
