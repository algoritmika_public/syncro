import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Users from './Users';
import UserMutation from './UserMutation';
import UserViewWithRelations from './UserViewWithRelations';
import NotFound from './../NotFound';
// import withWrapper from './../../new_hoc/withWrapper';

const UsersRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Users
              {...props}
              {...rest}
              userRole="CLIENT"
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="create"
              userRole="CLIENT"
              title={'Новый пользователь'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="update"
              userRole="CLIENT"
              title={'Редактирование пользователя'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <UserViewWithRelations
              {...props}
              {...rest}
              userRole="CLIENT"
            />
          )}
        />
        <Route
          exact
          path={`${url}/administrators`}
          component={rest => (
            <Users
              {...props}
              {...rest}
              userRole="ADMIN"
              linkForUpdateMaterial={`${url}/administrators/update`}
              linkForViewMaterial={`${url}/administrators/view`}
              title="Администраторы"
            />
          )}
        />
        <Route
          exact
          path={`${url}/administrators/create`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="create"
              userRole="ADMIN"
              title={'Новый администратор'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/administrators/update/:id`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="update"
              userRole="ADMIN"
              title={'Редактирование администратора'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/administrators/view/:id`}
          component={rest => (
            <UserViewWithRelations
              {...props}
              {...rest}
              userRole="ADMIN"
            />
          )}
        />
        <Route
          exact
          path={`${url}/moderators`}
          component={rest => (
            <Users
              {...props}
              {...rest}
              userRole="MODERATOR"
              linkForUpdateMaterial={`${url}/moderators/update`}
              linkForViewMaterial={`${url}/moderators/view`}
              title="Модераторы"
            />
          )}
        />
        <Route
          exact
          path={`${url}/moderators/create`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="create"
              userRole="MODERATOR"
              title={'Новый модератор'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/moderators/update/:id`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="create"
              userRole="MODERATOR"
              title={'Редактирование модератора'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/moderators/view/:id`}
          component={rest => (
            <UserViewWithRelations
              {...props}
              {...rest}
              userRole="MODERATOR"
            />
          )}
        />
        <Route
          exact
          path={`${url}/curators`}
          component={rest => (
            <Users
              {...props}
              {...rest}
              userRole="CURATOR"
              linkForUpdateMaterial={`${url}/curators/update`}
              linkForViewMaterial={`${url}/curators/view`}
              title="Координаторы"
            />
          )}
        />
        <Route
          exact
          path={`${url}/curators/create`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="create"
              userRole="CURATOR"
              title={'Новый Координатор'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/curators/update/:id`}
          component={rest => (
            <UserMutation
              {...props}
              {...rest}
              mutationType="update"
              userRole="CURATOR"
              title={'Редакторавать Координатор'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/curators/view/:id`}
          component={rest => (
            <UserViewWithRelations
              {...props}
              {...rest}
              userRole="CURATOR"
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(UsersRoutes, { title: "Пользователи" });
export default UsersRoutes;
