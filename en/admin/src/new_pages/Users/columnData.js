const columnData = [
  {
    id: 'firstName',
    label: 'Имя',
  }, {
    id: 'lastName',
    label: 'Фамилия',
  }, {
    id: 'email',
    label: 'E-mail',
  }, {
    id: 'phone',
    label: 'Телефон',
  }, {
    id: 'createdAt',
    label: 'Дата регистрации',
  },
];

export default columnData;
