import React from 'react';

import gql from './gql';

import {
  REVIEW_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import reviewsFields from './fields';

const ReviewViewWithRelations = (props) => {
  const gqlQueryWithRelationsName = REVIEW_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Review';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={reviewsFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
      />
    </div>
  );
};

export default ReviewViewWithRelations;
