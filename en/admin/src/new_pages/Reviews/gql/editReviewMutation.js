import gql from 'graphql-tag';

export default gql`
  mutation UpdateReviewMutation(
      $id: ID!
      $anons: String!
      $public: Boolean
      $description: String!
      $courseId: ID
      $cycleId: ID
      $eventId: ID
      $lectureId: ID
      $userId: ID
    ){
    updateReview(
      id: $id
      anons: $anons
      public: $public
      description: $description
      courseId: $courseId
      cycleId: $cycleId
      eventId: $eventId
      lectureId: $lectureId
      userId: $userId
    ) {
      id,
    }
  }
`;
