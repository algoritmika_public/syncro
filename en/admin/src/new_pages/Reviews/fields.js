const create = [
  {
    id: 'user',
    label: 'Автор отзыва',
    defaultValue: [],
    required: true,
    disabled: true,
    relation: 'user',
    filterType: 'CLIENT',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'id',
  },
  {
    id: 'lecture',
    label: 'Лекция',
    defaultValue: [],
    required: true,
    disabled: true,
    relation: 'lecture',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'id',
  },
  {
    id: 'anons',
    type: 'multiline',
    label: 'Анонс',
  },
  {
    id: 'description',
    type: 'multiline',
    label: 'Основной текст',
  },
  {
    id: 'public',
    label: 'Опубликовано',
    defaultValue: false,
    type: 'checkbox',
    outputType: 'boolean',
  },
];

const update = create.slice();
update.push({
  id: 'id',
  label: 'id',
  required: true,
  disabled: true,
  type: 'multiline',
});

const view = update.slice();
// view.push(
//   {
//     id: 'lecturers',
//     label: 'Лекторы связанные с направлением',
//     disabled: true,
//     relation: 'lecturers',
//     type: 'relation',
//   },
// );

const filter = [
  {
    id: 'firstName',
    value: '',
    label: 'Имя',
    type: 'text',
  },
  {
    id: 'lastName',
    value: '',
    label: 'Фамилия',
    type: 'text',
  },
  {
    id: 'email',
    value: '',
    label: 'E-mail',
    type: 'text',
  },
  {
    id: 'phone',
    value: '',
    label: 'Телефон',
    type: 'text',
  },
  {
    id: 'description',
    value: '',
    label: 'Текст',
    type: 'text',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
