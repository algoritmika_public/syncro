import gql from 'graphql-tag';

export default gql`
  query SimpleContentQuery {
    SimpleContent(
      type: "agreement"
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      description,
    }
  }
`;
