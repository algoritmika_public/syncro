const create = [
  {
    id: 'title',
    label: 'Заголовок',
    type: 'multiline',
    required: true,
  },
  {
    id: 'subTitle',
    label: 'Подзаголовок',
    type: 'multiline',
  },
  {
    id: 'img1240x430',
    label: 'Основная картинка 1240x430',
    defaultValue: 'no-photo',
    type: 'image',
    size: '1240x430',
  },
  {
    id: 'description',
    label: 'Основной текст',
    type: 'multiline',
  },

  {
    id: 'title1',
    label: 'Блок 1 - Подзаголовок 1',
    type: 'multiline',
  },
  {
    id: 'description1',
    label: 'Блок 1 - Описание 1',
    type: 'multiline',
  },
  {
    id: 'title2',
    label: 'Блок 1 - Подзаголовок 2',
    type: 'multiline',
  },
  {
    id: 'description2',
    label: 'Блок 1 - Описание 2',
    type: 'multiline',
  },
  {
    id: 'title3',
    label: 'Блок 1 - Подзаголовок 3',
    type: 'multiline',
  },
  {
    id: 'description3',
    label: 'Блок 1 - Описание 3',
    type: 'multiline',
  },

  {
    id: 'titleFigures',
    label: 'Блок с цифрами - Заголовок',
    type: 'multiline',
  },
  {
    id: 'fig1',
    label: 'Блок с цифрами - Цифра 1',
    type: 'multiline',
  },
  {
    id: 'caption1',
    label: 'Блок с цифрами - Описание 1',
    type: 'multiline',
  },
  {
    id: 'fig2',
    label: 'Блок с цифрами - Цифра 2',
    type: 'multiline',
  },
  {
    id: 'caption2',
    label: 'Блок с цифрами - Описание 2',
    type: 'multiline',
  },
  {
    id: 'fig3',
    label: 'Блок с цифрами - Цифра 3',
    type: 'multiline',
  },
  {
    id: 'caption3',
    label: 'Блок с цифрами - Описание 3',
    type: 'multiline',
  },
  {
    id: 'fig4',
    label: 'Блок с цифрами - Цифра 4',
    type: 'multiline',
  },
  {
    id: 'caption4',
    label: 'Блок с цифрами - Описание 4',
    type: 'multiline',
  },

  {
    id: 'titleImportant',
    label: 'Блок самое главное - Заголовок',
    type: 'multiline',
  },
  {
    id: 'subTitleImportant1',
    label: 'Блок самое главное - Подзаголовок 1',
    type: 'multiline',
  },
  {
    id: 'descriptionImportant1',
    label: 'Блок самое главное - Описание 1',
    type: 'multiline',
  },
  {
    id: 'subTitleImportant2',
    label: 'Блок самое главное - Подзаголовок 2',
    type: 'multiline',
  },
  {
    id: 'descriptionImportant2',
    label: 'Блок самое главное - Описание 2',
    type: 'multiline',
  },
  {
    id: 'subTitleImportant3',
    label: 'Блок самое главное - Подзаголовок 3',
    type: 'multiline',
  },
  {
    id: 'descriptionImportant3',
    label: 'Блок самое главное - Описание 3',
    type: 'multiline',
  },

  {
    id: 'titleTeam',
    label: 'Блок наша комманда - Заголовок',
    type: 'multiline',
  },

  {
    id: 'titleThanks',
    label: 'Блок люди - Заголовок',
    type: 'multiline',
  },
  {
    id: 'peopleThanks',
    label: 'Блок люди - Люди',
    type: 'multiline',
    helperText: 'Каждая персона с новой строки',
    outputType: 'array',
    outputSubType: '\n',
  },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
);

export default {
  create,
  update,
};
