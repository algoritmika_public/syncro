import gql from 'graphql-tag';

export default gql`
  query AllImagesQuery($filter: ImageFilter, $first: Int, $skip: Int, $orderBy: ImageOrderBy) {
    allImages(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      createdAt
      updatedAt

      description
      secret
    }
    _allImagesMeta {
      count
    }
  }
`;
