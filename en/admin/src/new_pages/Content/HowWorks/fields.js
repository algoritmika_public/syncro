const createHWMainPage = [
  {
    id: 'page',
    defaultValue: 'mainpage',
    required: true,
    disabled: true,
  },
  {
    id: 'titleFirstBlock',
    label: 'Заголовок - 1 Блок',
    type: 'multiline',
    // required: true,
  },
  {
    id: 'textFirstBlock',
    label: 'Текст - 1 Блок',
    type: 'multiline',
    // required: true,
  },
  {
    id: 'titleSecondBlock',
    label: 'Заголовок - 2 Блок',
    type: 'multiline',
    // required: true,
  },
  {
    id: 'textSecondBlock',
    label: 'Текст - 2 Блок',
    type: 'multiline',
    // required: true,
  },
  {
    id: 'titleThreeBlock',
    label: 'Заголовок - 3 Блок',
    type: 'multiline',
    // required: true,
  },
  {
    id: 'textThreeBlock',
    label: 'Текст - 3 Блок',
    type: 'multiline',
    // required: true,
  },
  {
    id: 'titleFourBlock',
    label: 'Заголовок - 4 Блок',
    type: 'multiline',
    // required: true,
  },
  {
    id: 'textFourBlock',
    label: 'Текст - 4 Блок',
    type: 'multiline',
    // required: true,
  },
];

const createHWCourse = createHWMainPage.slice();
createHWCourse.splice(0, 1);
createHWCourse.unshift({
  id: 'page',
  defaultValue: 'course',
  required: true,
  disabled: true,
});

const createHWLecture = createHWMainPage.slice();
createHWLecture.splice(0, 1);
createHWLecture.unshift({
  id: 'page',
  defaultValue: 'lecture',
  required: true,
  disabled: true,
});

const updateHWMainPage = createHWMainPage.slice();
updateHWMainPage.unshift({
  id: 'id',
  required: true,
  disabled: true,
  type: 'multiline',
});

const updateHWCourse = createHWCourse.slice();
updateHWCourse.unshift({
  id: 'id',
  required: true,
  disabled: true,
  type: 'multiline',
});

const updateHWLecture = createHWLecture.slice();
updateHWLecture.unshift({
  id: 'id',
  required: true,
  disabled: true,
  type: 'multiline',
});

export default {
  createHWMainPage,
  createHWCourse,
  createHWLecture,
  updateHWMainPage,
  updateHWCourse,
  updateHWLecture,
};
