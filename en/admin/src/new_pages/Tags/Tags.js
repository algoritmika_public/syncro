import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_TAGS_QUERY,
  DELETE_TAG_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Tags = (props) => {

  const gqlQueryName = ALL_TAGS_QUERY;
  const queryResultObjectName = 'allTags';
  const gqlMutationName = DELETE_TAG_MUTATION;
  const mutationResultObjectName = 'deleteTag';
  const resulMutationMessageSuccses = 'Направление удалено';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
      />
    </div>
  );
};

export default Tags;
