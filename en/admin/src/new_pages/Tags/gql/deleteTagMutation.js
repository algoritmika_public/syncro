import gql from 'graphql-tag';

export default gql`
  mutation deleteTagMutation(
      $id: ID!,
      $aliasId: ID!
    ){
    deleteTag(
      id: $id,
    ) {
      id
    }
    deleteAlias(
      id: $aliasId
    ) {
      id
    }
  }
`;
