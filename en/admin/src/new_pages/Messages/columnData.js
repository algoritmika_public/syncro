const columnData = [
  {
    id: 'title',
    label: 'Заголок'
  }, {
    id: 'text',
    label: 'Текст'
  }
];

export default columnData;
