import React from 'react';

import gql from './gql';

import {
  ALL_MESSAGES_QUERY,
  DELETE_MESSAGE_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';


const Messages = (props) => {

  const gqlQueryName = ALL_MESSAGES_QUERY;
  const queryResultObjectName = 'allMessages';
  const gqlMutationName = DELETE_MESSAGE_MUTATION;
  const mutationResultObjectName = 'deleteLecturer';
  const resulMutationMessageSuccses = 'Партнер удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={[]}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
        showMenu={false}
      />
    </div>
  );
};

export default Messages;
