const modifyQueryFilter = (input = {}, type) => {
  const filter = {};
  if (!input) return {};
  const {
    title,
    link,
  } = input;

  if (title) filter.title_contains = title;
  if (link) filter.link_contains = link;

  return filter;
};

export default modifyQueryFilter;
