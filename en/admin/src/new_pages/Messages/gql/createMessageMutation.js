import gql from 'graphql-tag';

export default gql`
  mutation CreateMessageMutation(
      $title: String!
      $text: String!
    ){
    createMessage(
      text: $text,
      title: $title,
    ) {
      id
    }
  }
`;
