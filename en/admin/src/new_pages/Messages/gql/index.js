import {
  CREATE_MESSAGE_MUTATION,
  ALL_MESSAGES_QUERY,
  EDIT_MESSAGE_MUTATION,
  MESSAGE_QUERY,
  DELETE_MESSAGE_MUTATION,
  MESSAGE_QUERY_WITH_RELATIONS,
} from './../../../constants';

import createMessageMutation from './createMessageMutation';
import allMessagesQuery from './allMessagesQuery';
import editMessageMutation from './editMessageMutation';
import messageQuery from './messageQuery';
import deleteMessageMutation from './deleteMessageMutation';
import messageRelationQuery from './messageRelationQuery';

const gql = {
  [CREATE_MESSAGE_MUTATION]: createMessageMutation,
  [DELETE_MESSAGE_MUTATION]: deleteMessageMutation,
  [EDIT_MESSAGE_MUTATION]: editMessageMutation,
  [ALL_MESSAGES_QUERY]: allMessagesQuery,
  [MESSAGE_QUERY]: messageQuery,
  [MESSAGE_QUERY_WITH_RELATIONS]: messageRelationQuery,
};

export default gql;
