import gql from 'graphql-tag';

export default gql`
  query OrderQuery($id: ID!) {
    Order(
      id: $id,
    ) {
      id
      totalPrice
      events {
        id
        date
        price
        lecture {
          id
          title
          tags {
            id
            title
          }
          course {
            id
            title
            buyFull
            price
            tags {
              id
              title
            }
            _lecturesMeta {
              count
            }
          }
          cycle {
            id
            title
            buyFull
            price
            tags {
              id
              title
            }
            _lecturesMeta {
              count
            }
          }
        }
      }
      participants {
        id
        firstName
        lastName
        email
      }
      payment {
        id
        type
        status
        additionalDiscounts {
          id
          title
          rate
          validityPeriodFrom,
          validityPeriodTo,
          useCount
          unused
          buyingLecturesCount
          participantsCount
          allProducts
          tags {
            id
          }
          lectures {
            id
          }
          courses {
            id
          }
          cycles {
            id
          }
        }
        discounts {
          id
          title
          rate
          validityPeriodFrom,
          validityPeriodTo,
          useCount
          unused
          buyingLecturesCount
          participantsCount
          allProducts
          tags {
            id
          }
          lectures {
            id
          }
          courses {
            id
          }
          cycles {
            id
          }
        }
        certificatesInPayments {
          id
          lecturesCount
          unusedLectures
          personsCount
          start
          end
          price
          title
          user {
            id
            firstName
            lastName
            email
          }
        }
        subscriptionsInPayments {
          id
          lecturesCount
          unusedLectures
          personsCount
          start
          end
          price
          title
          user {
            id
            firstName
            lastName
            email
          }
        }
        acquiring {
          id
          title
        }
      }
      user {
        id
        firstName
        lastName
        email
      }
    }
  }
`;
