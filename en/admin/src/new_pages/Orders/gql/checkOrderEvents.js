import gql from 'graphql-tag';

export default gql`
  query CheckOrderEvents($order: Json!) {
    checkOrderEvents(
      order: $order,
    ) {
      resultOfChecking
      resultOfCalcOrder
    }
    allSettings (
      first: 1
    ) {
      acquiring {
        id
        commission
      }
    }
  }
`;
