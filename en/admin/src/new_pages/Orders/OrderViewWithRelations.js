import React from 'react';

import gql from './gql';

import {
  ORDER_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const OrderViewWithRelations = (props) => {
  const gqlQueryName = ORDER_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Order';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default OrderViewWithRelations;
