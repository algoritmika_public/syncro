 import { appendProp } from './../../new_utils';

const modifyQueryFilter = (input = {}, type) => {
  let filter = {};
  filter = appendProp(filter, 'payment');

  if (!input) return {};
  const {
    firstName,
    lastName,
    email,
    totalPriceMin,
    totalPriceMax,
    participantsCount,
    tab,
    lectureTitle,
    courseTitle,
    cycleTitle,
    orderDate,
    paymentDate,
  } = input;

  if (firstName) {
    filter = appendProp(filter, 'user');
    filter.user.firstName_contains = firstName;
  }
  if (lastName) {
    filter = appendProp(filter, 'user');
    filter.user.lastName_contains = lastName;
  }
  if (email) {
    filter = appendProp(filter, 'user');
    filter.user.email_contains = email;
  }
  if (totalPriceMin) filter.totalPrice_gte = totalPriceMin;
  if (totalPriceMax) filter.totalPrice_lte = totalPriceMax;

  if (lectureTitle) {
    filter = appendProp(filter, 'events_some', 'lecture');
    filter.events_some.lecture.title_contains = lectureTitle;
  }
  if (courseTitle) {
    filter = appendProp(filter, 'events_some', 'lecture');
    filter.events_some.lecture.course_some = {};
    filter.events_some.lecture.course_some.title_contains = courseTitle;
  }
  if (cycleTitle) {
    filter = appendProp(filter, 'events_some', 'lecture');
    filter.events_some.lecture.cycle_some = {};
    filter.events_some.lecture.cycle_some.title_contains = cycleTitle;
  }
  if (orderDate) {
    filter.createdAt = orderDate;
  }
  if (paymentDate) {
    filter.payment.updatedAt = paymentDate;
    filter.payment.status = 'PAID';
  }

  if (tab && tab.id === 'unpaid') {
    filter.payment.status = 'PENDING';
  } else {
    filter.payment.status = 'PAID';
  }

  return filter;
};

export default modifyQueryFilter;
