import { EDIT_ALIAS_MUTATION } from './../../../constants';
import updateAliasMutation from './updateAliasMutation';

export default {
  [EDIT_ALIAS_MUTATION]: updateAliasMutation,
};
