import gql from 'graphql-tag';

export default gql`
  fragment DiscountFullPayload on Discount {
    id
    createdAt
    updatedAt

    title
    promocode
    rate
    allProducts
    buyingLecturesCount
    participantsCount
    useCount
    validityPeriodFrom
    validityPeriodTo
    certificateProducts {
      id
      title
    }
    subscriptionProducts {
      id
      title
    }
    courses {
      id
      title
    }
    cycles {
      id
      title
    }
    lectures {
      id
      title
    }
    orders {
      id
    }
    tags {
      id
      title
    }
    _coursesMeta {
      count
    }
    _cyclesMeta {
      count
    }
    _lecturesMeta {
      count
    }
    _ordersMeta {
      count
    }
    _tagsMeta {
      count
    }
  }
`;
