const columnData = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'promocode',
    label: 'Промокод',
  },
  {
    id: 'rate',
    label: 'Размер скидки',
  },
  {
    id: 'validityPeriodFrom',
    label: 'Начало действия',
  },
  {
    id: 'validityPeriodTo',
    label: 'Окончание действия',
  },
  {
    id: 'useCount',
    label: 'Количество использований',
  },
  {
    id: 'buyingLecturesCount',
    label: 'Количество лекций для действия скидки',
  },
  {
    id: 'participantsCount',
    label: 'Количество участников для действия скидки',
  },
];
export default columnData;
