import {
  CREATE_DISCOUNT_MUTATION,
  ALL_DISCOUNTS_QUERY,
  EDIT_DISCOUNT_MUTATION,
  DISCOUNT_QUERY,
  DELETE_DISCOUNT_MUTATION,
  DISCOUNT_QUERY_WITH_RELATIONS,
  ALL_DISCOUNTS_QUERY_SHORT,
  ALL_DISCOUNTS_RELAIONS_QUERY,
} from './../../../constants';
import createDiscountMutation from './createDiscountMutation';
import allDiscountsQuery from './allDiscountsQuery';
import editDiscountMutation from './editDiscountMutation';
import discountQuery from './discountQuery';
import deleteDiscountMutation from './deleteDiscountMutation';
import discountQueryWithRelations from './discountQueryWithRelations';
import allDiscountsQueryShort from './allDiscountsQueryShort';
import allDiscountsRelationsQuery from './allDiscountsRelationsQuery';

const gql = {
  [CREATE_DISCOUNT_MUTATION]: createDiscountMutation,
  [ALL_DISCOUNTS_QUERY]: allDiscountsQuery,
  [ALL_DISCOUNTS_QUERY_SHORT]: allDiscountsQueryShort,
  [EDIT_DISCOUNT_MUTATION]: editDiscountMutation,
  [DISCOUNT_QUERY]: discountQuery,
  [DELETE_DISCOUNT_MUTATION]: deleteDiscountMutation,
  [DISCOUNT_QUERY_WITH_RELATIONS]: discountQueryWithRelations,
  [ALL_DISCOUNTS_RELAIONS_QUERY]: allDiscountsRelationsQuery,
};

export default gql;
