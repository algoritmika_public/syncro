import gql from 'graphql-tag';

export default gql`
  mutation deletePartnerMutation(
      $id: ID!
    ){
    deletePartner(
      id: $id,
    ) {
      id
    }
  }
`;
