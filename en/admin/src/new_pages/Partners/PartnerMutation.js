import React from 'react';

import gql from './gql';

import {
  CREATE_PARTNER_MUTATION,
  EDIT_PARTNER_MUTATION,
  PARTNER_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import partnersFields from './fields';

const PartnersMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_PARTNER_MUTATION;
    fields = partnersFields.create;
    mutationResultObjectName = 'createPartner';
    resultMutationMessageSuccses = 'Партнер создан';
  } else {
    gqlMutationName = EDIT_PARTNER_MUTATION;
    fields = partnersFields.update;
    mutationResultObjectName = 'updatePartner';
    resultMutationMessageSuccses = 'Партнер обновлен';
    gqlQueryName = PARTNER_QUERY;
    queryResultObjectName = 'Partner';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
      />
    </div>
  );
};

export default PartnersMutation;
