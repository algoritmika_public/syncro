import React from 'react';

import gql from './gql';

import {
  ALL_PARTNERS_QUERY,
  DELETE_PARTNER_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const Partners = (props) => {

  const gqlQueryName = ALL_PARTNERS_QUERY;
  const queryResultObjectName = 'allPartners';
  const gqlMutationName = DELETE_PARTNER_MUTATION;
  const mutationResultObjectName = 'deletePartner';
  const resulMutationMessageSuccses = 'Партнер удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default Partners;
