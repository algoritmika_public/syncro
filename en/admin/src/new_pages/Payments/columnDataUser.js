const columnData = [
  {
    id: 'updatedAt',
    label: 'Дата',
  },
  {
    id: 'productOfPayment',
    label: 'Продукт',
  },
  {
    id: 'type',
    label: 'Тип платежа',
  },
  {
    id: 'totalPrice',
    label: 'Сумма платежа',
  },
  {
    id: 'orderPrice',
    label: 'Сумма заказа',
  },
  {
    id: 'discountPrice',
    label: 'Сумма скидки',
  },
  {
    id: 'commission',
    label: 'Коммиссия',
  },
];
export default columnData;
