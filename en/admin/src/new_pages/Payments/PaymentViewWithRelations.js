import React from 'react';

import gql from './gql';

import {
  PAYMENT_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const PaymentViewWithRelations = (props) => {
  const gqlQueryName = PAYMENT_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Payment';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default PaymentViewWithRelations;
