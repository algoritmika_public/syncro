import { appendProp } from './../../new_utils';

const modifyQueryFilter = (input = {}, type) => {
 let filter = {};


 if (!input) return {};
 const {
   firstName,
   lastName,
   email,
   totalPriceMin,
   totalPriceMax,
   participantsCount,
   tab,
   lectureTitle,
   courseTitle,
   cycleTitle,
   orderDate,
   paymentDate,
 } = input;

 if (firstName) filter.firstName_contains = firstName;
 if (lastName) filter.lastName_contains = lastName;
 if (email) filter.email_contains = email;
 if (totalPriceMin) filter.totalPrice_gte = totalPriceMin;
 if (totalPriceMax) filter.totalPrice_lte = totalPriceMax;

 if (lectureTitle) {
   filter = appendProp(filter, 'events_some', 'lecture');
   filter.events_some.lecture.title_contains = lectureTitle;
 }
 if (courseTitle) {
   filter = appendProp(filter, 'events_some', 'lecture');
   filter.events_some.lecture.course_some = {};
   filter.events_some.lecture.course_some.title_contains = courseTitle;
 }
 if (cycleTitle) {
   filter = appendProp(filter, 'events_some', 'lecture');
   filter.events_some.lecture.cycle_some = {};
   filter.events_some.lecture.cycle_some.title_contains = cycleTitle;
 }

 if (paymentDate) {
   filter.updatedAt = paymentDate;
   filter.status = 'PAID';
 }

 if (tab && tab.id === 'unpaid') {
   filter.status = 'PENDING';
 } else {
   filter.status = 'PAID';
 }

 return filter;
};

export default modifyQueryFilter;
