import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllPaymentsQuery($filter: PaymentFilter, $first: Int, $skip: Int, $orderBy: PaymentOrderBy) {
    allPayments(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...PaymentPayload
    }
    _allPaymentsMeta {
      count
    }
  }
  ${fragments.paymentPayload}
`;
