import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllPaymentsRelationsQuery($filter: PaymentFilter, $first: Int, $skip: Int, $orderBy: PaymentOrderBy) {
    allPayments(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...PaymentRelationsPayload
    }
  }
  ${fragments.paymentReationsPayload}
`;
