import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Acquirings from './Acquirings';
import AcquiringsMutation from './AcquiringsMutation';
import AcquiringViewWithRelations from './AcquiringViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const AcquiringsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Acquirings
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <AcquiringsMutation {...props} {...rest} mutationType="create" title={'Новый эквайринг'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <AcquiringsMutation {...props} {...rest} mutationType="update" title={'Редактирование эквайринга'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <AcquiringViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(AcquiringsRoutes, { title: 'Эквайринг' });
export default AcquiringsRoutes;
