import gql from 'graphql-tag';

export default gql`
  query EventQUery(
    $id: ID!,
  ) {
    Event(
      id: $id,
    ) {
      id
      date
      price
      lecture {
        id
        title
        course {
          id
          title
        }
        cycle {
          id
          title
        }
      }
      lecturers {
        id
        firstName
        lastName
      }
      location {
        id
        title
        address
        metro
      }
      curator {
        id
        firstName
        lastName
        email
      }
      quantityOfTickets
      quantityOfTicketsAvailable
      _ticketsMeta {
        count
      }
      _ordersMeta {
        count
      }
    }
  }
`;
