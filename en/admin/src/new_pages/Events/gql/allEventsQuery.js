import gql from 'graphql-tag';

export default gql`
  query AllEventsQueryShort(
    $filter: EventFilter,
    $first: Int,
    $skip: Int,
    $orderBy: EventOrderBy
  ) {
    allEvents(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      createdAt
      updatedAt

      date
      price
      lecture {
        id
        title
        tags {
          title
        }
        course {
          id
          title
          tags {
            title
          }
        }
        cycle {
          id
          title
          tags {
            title
          }
        }
      }
      lecturers {
        id
        firstName
        lastName
      }
      location {
        id
        title
        address
        metro
      }
      curator {
        id
        firstName
        lastName
        email
      }
      quantityOfTickets
      quantityOfTicketsAvailable
      _ticketsMeta {
        count
      }
    }
  }
`;
