import gql from 'graphql-tag';

export default gql`
  query AllEventsQueryShort(
    $filter: EventFilter,
    $first: Int,
    $skip: Int,
    $orderBy: EventOrderBy
  ) {
    allEvents(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      date
      price
      lecture {
        id
        title
      }
      lecturers {
        id
        firstName
        lastName
      }
      location {
        id
        title
        address
        metro
      }
      quantityOfTickets
      _ticketsMeta {
        count
      }
    }
  }
`;
