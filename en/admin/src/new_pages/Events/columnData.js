// | Дата/Время | Название | Тип | Лекторы | Локации | Направление | Количество билетов | Количество проданных билетов |
const columnData = [
  {
    id: 'dateShort',
    label: 'Дата',
  },
  {
    id: 'time',
    label: 'Время',
  },
  {
    id: 'price',
    label: 'Цена',
  },
  {
    id: 'curator',
    label: 'Координатор',
  },
  {
    id: 'lecture',
    label: 'Лекция',
  },
  {
    id: 'course',
    label: 'Курс',
  },
  {
    id: 'cycle',
    label: 'Цикл',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
  },
  {
    id: 'location',
    label: 'Локация',
  },
  {
    id: 'tags',
    label: 'Направление',
  },
  {
    id: 'quantityOfTickets',
    label: 'Количество билетов',
  },
  {
    id: 'quantityOfTicketsAvailable',
    label: 'Количество доступных билетов',
  },
  {
    id: '_ticketsMeta',
    label: 'Количество проданных билетов',
  },
];

export default columnData;
