const columnDataShort = [
  {
    id: 'date',
    label: 'Дата',
  },
  {
    id: 'lectureTitle',
    label: 'Лекция',
  },
  {
    id: 'location',
    label: 'Локация',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
  },
  {
    id: 'price',
    label: 'Цена',
  },
  {
    id: 'quantityOfTickets',
    label: 'Количество билетов',
  },
];

export default columnDataShort;
