const registeredParticipants = (classes, { src: { payment } }) => {
  const {
    status,
    certificatesInPayments,
    subscriptionsInPayments,
    discounts = [],
    additionalDiscounts = [],
  } = payment;

  const {
    rowRed,
    rowYellow,
    rowBlue,
    rowGreen,
    rowLightBlue,
  } = classes;

  if (status === 'PENDING') {
    return rowRed;
  }
  const d = Math.max.apply(null, discounts.concat(additionalDiscounts).map(({ rate }) => rate));
  if (d >= 100) return rowYellow;
  if (certificatesInPayments.length > 0) return rowBlue;
  if (subscriptionsInPayments.length > 0) return rowLightBlue;
  return rowGreen;
};

export default {
  registeredParticipants,
};
