import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import gql from './../gql';

import {
  PARTICIPANTS_OF_EVENT_QUERY,
} from './../../../constants';

import ListEntitiesQuery from './../../../new_components/ListEntitiesQuery';

import fields from './fields';
import columnData from './columnData';
import modifyQueryOptions from './modifyQueryOptions';
import modifyQueryFilter from './modifyQueryFilter';
import modifyQueryResponse from './modifyQueryResponse';
import coloredRow from './coloredRow';

const RegisteredParticipants = (props) => {
  const gqlQueryName = PARTICIPANTS_OF_EVENT_QUERY;
  const queryResultObjectName = 'Event';

  return (
    <div>
      <div>
        {/* <Typography type="subheading" gutterBottom>
          Легенда
        </Typography> */}
        <Typography type="body1" gutterBottom>
          <strong>Легенда: </strong>
          Пользователи оплатившие заказ - зеленым.
          Не оплатившие — красным.
          Использующие сертификат — синим.
          Использующие абонемент — голубым.
          Подарок (в заказе есть скидка 100%) — желтый.
        </Typography>
      </div>
      <ListEntitiesQuery
        {...props}
        columnData={columnData.registeredParticipants}
        fields={fields.registeredParticipants}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryFilter={modifyQueryFilter.registeredParticipants}
        modifyQueryOptions={modifyQueryOptions.registeredParticipants}
        coloredRow={coloredRow.registeredParticipants}
        modifyQueryResponse={modifyQueryResponse.registeredParticipants}
      />
    </div>
  );
};

export default RegisteredParticipants;
