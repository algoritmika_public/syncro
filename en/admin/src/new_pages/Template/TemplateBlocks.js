import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  ALL_TEMPLATES_QUERY,
} from './../../constants';

import MaterialQueryAndMutationRedirect from './../../components/MaterialQueryAndMutationRedirect';

const TemplateBlocks = (props) => {

  return (
    <div>
      <MaterialQueryAndMutationRedirect
        {...props}
        gql={gql}
        gqlAllQueryName={ALL_TEMPLATES_QUERY}
        gqlAllQueryItemsName={'allTemplates'}
      />
    </div>
  );
};

export default TemplateBlocks;
