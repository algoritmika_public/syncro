import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Subscriptions from './Subscriptions';
import SubscriptionsMutation from './SubscriptionsMutation';
import SubscriptionsMutationUpdate from './SubscriptionsMutationUpdate';
import SubscriptionViewWithRelations from './SubscriptionViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../new_hoc/withWrapper';

const SubscriptionsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Subscriptions
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <SubscriptionsMutation {...props} {...rest} mutationType="create" title={'Новый заказ'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <SubscriptionsMutationUpdate {...props} {...rest} mutationType="update" title={'Редактирование абонемента'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <SubscriptionViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(SubscriptionsRoutes, { title: 'Абонементы' });
export default SubscriptionsRoutes;
