import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_SUBSCRIPTIONS_QUERY,
  DELETE_SUBSCRIPTION_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import { tabsExpNoPayment } from './../../new_utils';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Subscriptions = (props) => {

  const gqlQueryName = ALL_SUBSCRIPTIONS_QUERY;
  const queryResultObjectName = 'allSubscriptions';
  const gqlMutationName = DELETE_SUBSCRIPTION_MUTATION;
  const mutationResultObjectName = 'deleteSubscription';
  const resulMutationMessageSuccses = 'Сертификат удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter.modifyQuerySubscriptions}
        tabs={tabsExpNoPayment}
      />
    </div>
  );
};

export default Subscriptions;
