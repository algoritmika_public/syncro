export default () => {
  return (
    {
      refetchQueries: [
        'AllSubscriptionsQuery',
        'AllSubscriptionsOfUserQuery',
      ],
    }
  );
};
