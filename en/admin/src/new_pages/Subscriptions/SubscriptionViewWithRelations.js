import React from 'react';

import gql from './gql';

import {
  SUBSCRIPTION_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const SubscriptionViewWithRelations = (props) => {
  const gqlQueryName = SUBSCRIPTION_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Subscription';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default SubscriptionViewWithRelations;
