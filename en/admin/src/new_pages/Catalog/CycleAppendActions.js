import React from 'react';
import Icon from 'material-ui/Icon';
import NoteAdd from 'material-ui-icons/NoteAdd';
import Button from 'material-ui/Button';
import { Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

const CycleAppendActions = (props) => {
  const { classes } = props;
  return (
    <div>
      <Button
        className={classes.button}
        raised
        dense
        color="primary"
        component={Link}
        to="/catalog/lectures/create-lecture-for-cycle"
      >
        <NoteAdd className={classes.leftIcon} />
        {'Создать лекцию для цикла'}
      </Button>
    </div>
  );
};

export default withStyles(styles)(CycleAppendActions);
