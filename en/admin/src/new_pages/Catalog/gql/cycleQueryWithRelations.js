import gql from 'graphql-tag';

export default gql`
  query cycleQuery($id: ID!) {
    Cycle(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      anons,
      description,
      buyFull,
      img1240x349,
      img340x192,
      metaDescription,
      metaKeywords,
      price,
      public,
      when,
      whenStrong,
      #
      author {
        id,

        firstName,
        lastName,
        email,
        phone,
        role,
      }
      #
      lectures {
        id,

        title,
        author {
          id,

          firstName,
          lastName,
        }
        events {
          id,

          date,
          price,
          location {
            id,
            title,
          }
          lecturers {
            id,
            firstName,
            lastName,
          }
        }
      }
      #
      alias {
        id,
        alias,
      }
      #
      metaTags {
        id,
        metaKeywords,
        metaDescription,
        title,
      }
      #
      reviews {
        id,
        user {
          id,
          firstName,
          lastName,
        },
      }
      #
      tags {
        id,
        title,
        color,
        textColor,
      }
      #
      recommendedCourses {
        id,
        title,
        lectures {
          id,
          title,
          events {
            id,
            date,
            lecturers {
              id,

              firstName,
              lastName,
            }
            location {
              id,
              title,
            }
          }
        }
      }
      #
      recommendedCycles {
        id,
        title,
        lectures {
          id,
          title,
          events {
            id,
            date,
            lecturers {
              id,

              firstName,
              lastName,
            }
            location {
              id,
              title,
            }
          }
        }
      }
      #
      recommendedLectures {
        id,
        title,
        events {
          id,
          date,
          lecturers {
            id,

            firstName,
            lastName,
          }
          location {
            id,
            title,
          }
        }
      }
      #
      inRecommendationsOfCourses {
        id,
        title,
      }
      #
      inRecommendationsOfCycles {
        id,
        title,
      }
      #
      inRecommendationsOfLectures {
        id,
        title,
      }
      #
    }
  }
`;
