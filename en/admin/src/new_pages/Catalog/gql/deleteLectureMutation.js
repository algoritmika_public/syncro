import gql from 'graphql-tag';

export default gql`
  mutation deleteLectureMutation(
      $id: ID!,
      $aliasId: ID!
    ){
    deleteLecture(
      id: $id,
    ) {
      id
    }
    deleteAlias(
      id: $aliasId
    ) {
      id
    }
  }
`;
