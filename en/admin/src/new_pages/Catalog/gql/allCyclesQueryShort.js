import gql from 'graphql-tag';

export default gql`
  query AllCyclesQueryShort($filter: CycleFilter, $first: Int, $skip: Int, $orderBy: CycleOrderBy) {
    allCycles(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      title
      buyFull
      price
      public
      tags {
        id
        title
      }
      lectures {
        title
        events {
          location {
            title
          }
          lecturers {
            firstName,
            lastName,
          }
        }
      }
      _lecturesMeta {
        count
      }
    }
  }
`;
