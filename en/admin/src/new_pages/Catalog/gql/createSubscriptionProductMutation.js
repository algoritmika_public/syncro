import gql from 'graphql-tag';

export default gql`
  mutation createSubscriptionProductMutation(
    $lecturesCount: Int!,
    $months: Int!,
    $price: Float!,
    $title: String!,
    $personsCount: Int!
    $image: String!
    $type: SubProductTypes,
    $prefix: String,
    $public: Boolean,
    $discountsIds: [ID!],
  ){
    createSubscriptionProduct(
      lecturesCount: $lecturesCount,
      months: $months,
      price: $price,
      title: $title,
      personsCount: $personsCount,
      image: $image,
      type: $type,
      prefix: $prefix,
      public: $public,
      discountsIds: $discountsIds,
    ) {
      id
    }
  }
`;
