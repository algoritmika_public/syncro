import gql from 'graphql-tag';

export default gql`
  query AllCoursesQueryShort($filter: CourseFilter, $first: Int, $skip: Int, $orderBy: CourseOrderBy) {
    allCourses(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      title
      public
      buyFull
      price
      tags {
        id
        title
      }
      lectures {
        id
        title
        events {
          location {
            title
          }
          lecturers {
            firstName
            lastName
          }
        }
      }
      _lecturesMeta {
        count
      }
    }
  }
`;
