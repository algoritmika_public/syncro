import gql from 'graphql-tag';

export default gql`
  query courseQuery($id: ID!) {
    Course(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      anons,
      description,
      buyFull,
      img1240x349,
      img340x192,
      metaDescription,
      metaKeywords,
      price,
      public,
      #
      author {
        id,

        firstName,
        lastName,
        email,
        phone,
        role,
      }
      #
      lectures {
        id,
        title,
        events {
          id,
          date,
          price,
          lecturers {
            id,
            firstName,
            lastName,
          },
          location {
            id,
            title,
            address,
            metro,
          },
        },
      },
      #
      alias {
        id
        alias
        manuallyChanged
      }
      #
      metaTags {
        id,
        metaKeywords,
        metaDescription,
        title,
      },
      #
      reviews {
        id,
        user {
          id,
          firstName,
          lastName,
        },
      }
      #
      tags {
        id,
        title,
        color,
        textColor,
      }
      #
      discounts {
        id,
        title,
      }
      #
      recommendedCourses {
        id,
        title,
      },
      #
      recommendedCycles {
        id,
        title,
      },
      #
      recommendedLectures {
        id,
        title,
      }
      inRecommendationsOfCourses {
        id,
        title,
      },
      #
      inRecommendationsOfCycles {
        id,
        title,
      },
      #
      inRecommendationsOfLectures {
        id,
        title,
      },
      #
    }
  }
`;
