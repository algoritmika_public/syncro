import gql from 'graphql-tag';

export default gql`
  mutation deleteCourseMutation(
      $id: ID!,
      $aliasId: ID!
    ){
    deleteCourse(
      id: $id,
    ) {
      id
    }
    deleteAlias(
      id: $aliasId
    ) {
      id
    }
  }
`;
