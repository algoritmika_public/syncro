import {
  createLecture,
  createLectureForCourse,
  createLectureForCycle,
} from './createLecture';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateLecture = createLecture.filter(field => field.id !== 'events');
updateLecture.unshift(idField);

const updateLectureForCourse = createLectureForCourse.filter(field => field.id !== 'events');
updateLectureForCourse.unshift(idField);

const updateLectureForCycle = createLectureForCycle.filter(field => field.id !== 'events');
updateLectureForCycle.unshift(idField);

export default {};
export { updateLecture };
export { updateLectureForCourse };
export { updateLectureForCycle };
