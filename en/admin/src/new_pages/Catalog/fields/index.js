import {
  createLecture,
  createLectureForCourse,
  createLectureForCycle,
} from './createLecture';

import {
  updateLecture,
  updateLectureForCourse,
  updateLectureForCycle,
} from './updateLecture';

import {
  viewLecture,
  viewLectureForCourse,
  viewLectureForCycle,
} from './viewLecture';

import createCourse from './createCourse';
import updateCourse from './updateCourse';
import viewCourse from './viewCourse';
import createCycle from './createCycle';
import updateCycle from './updateCycle';
import viewCycle from './viewCycle';

import createSubscription from './createSubscription';
import updateSubscription from './updateSubscription';
import createCertificate from './createCertificate';
import updateCertificate from './updateCertificate';
import viewSubscription from './viewSubscription';
import viewCertificate from './viewCertificate';

import filterLectures from './filterLectures';
import filterCourses from './filterCourses';
import filterCycles from './filterCycles';
import filterSubscriptions from './filterSubscriptions';
import filterCertificates from './filterCertificates';
import filterLecturesShort from './filterLecturesShort';
import filterCoursesShort from './filterCoursesShort';
import filterCyclesShort from './filterCyclesShort';
import filterCertificatesShort from './filterCertificatesShort';
import filterSubscriptionsShort from './filterSubscriptionsShort';

const lecture = {
  create: createLecture,
  update: updateLecture,
  view: viewLecture,
  filter: filterLectures,
  filterShort: filterLecturesShort,
};

const lectureForCourse = {
  create: createLectureForCourse,
  update: updateLectureForCourse,
  view: viewLectureForCourse,
  filter: filterLectures,
};

const lectureForCycle = {
  create: createLectureForCycle,
  update: updateLectureForCycle,
  view: viewLectureForCycle,
  filter: filterLectures,
};

const course = {
  create: createCourse,
  update: updateCourse,
  view: viewCourse,
  filter: filterCourses,
  filterShort: filterCoursesShort,
};

const cycle = {
  create: createCycle,
  update: updateCycle,
  view: viewCycle,
  filter: filterCycles,
  filterShort: filterCyclesShort,
};

const subscription = {
  create: createSubscription,
  update: updateSubscription,
  view: viewSubscription,
  filter: filterSubscriptions,
  filterShort: filterSubscriptionsShort,
};

const certificate = {
  create: createCertificate,
  update: updateCertificate,
  view: viewCertificate,
  filter: filterCertificates,
  filterShort: filterCertificatesShort,
};

export default {};
export { lecture };
export { lectureForCourse };
export { lectureForCycle };
export { course };
export { cycle };
export { subscription };
export { certificate };
