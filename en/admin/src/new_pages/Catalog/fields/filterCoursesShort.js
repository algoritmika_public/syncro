// дате, направлению, лектору, локации.
import filterLecturesShort from './filterLecturesShort';
const filterCoursesShort = filterLecturesShort.slice().filter(({ id }) => id !== 'lectureType');

export default filterCoursesShort;
