import {
  SUB_PRODUCT_TYPES,
} from './../../../enum';
import { GC_USER_ID } from './../../../constants';

const userId = localStorage.getItem(GC_USER_ID);

const createSubscription = [
  // {
  //   id: 'author',
  //   label: 'Автор',
  //   type: 'author',
  //   disabled: true,
  //   hidden: true,
  //   defaultValue: userId,
  //   outputType: 'id',
  // },
  {
    id: 'type',
    label: 'Тип',
    type: 'enum',
    enum: SUB_PRODUCT_TYPES,
    defaultValue: 'SUBSCRIPTION',
    required: true,
    disabled: true,
  },
  {
    id: 'public',
    label: 'Опубликовать',
    type: 'checkbox',
    defaultValue: false,
    outputType: 'boolean',
  },
  {
    id: 'title',
    label: 'Название',
    required: true,
    type: 'multiline',
  },
  // {
  //   id: 'itemType',
  //   required: true,
  //   label: 'Тип абонемента',
  //   type: 'multiline',
  // },
  {
    id: 'lecturesCount',
    required: true,
    label: 'Количество лекций',
    type: 'number',
    helperText: '-1 - безлимит',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'personsCount',
    label: 'Количество персон',
    type: 'number',
    defaultValue: '1',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'months',
    required: true,
    label: 'Срок действия(месяцев)',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'price',
    required: true,
    label: 'Цена',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'prefix',
    required: true,
    label: 'Префикс',
    type: 'multiline',
  },
  {
    id: 'image',
    label: 'Картинка 610x338',
    defaultValue: 'no-photo',
    type: 'image',
    // size: '610x!/0x0:610x338',
    size: '610x338',
  },
  {
    id: 'discounts',
    label: 'Скидки',
    relation: 'discounts',
    type: 'relation',
    disabled: true,
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
];

export default createSubscription;
