import createCertificate from './createCertificate';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateCertificate = createCertificate.concat(idField);
export default updateCertificate;
