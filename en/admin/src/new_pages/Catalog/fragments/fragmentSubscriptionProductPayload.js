import gql from 'graphql-tag';

export default gql`
  fragment SubscriptionProductPayload on SubscriptionProduct {
    id
    createdAt
    updatedAt

    title
    lecturesCount
    months
    price
    prefix
    personsCount
    image
    type
  }
`;
