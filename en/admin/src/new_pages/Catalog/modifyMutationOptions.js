const modifyMutationOptionsCertificate = () => {
  return (
    {
      refetchQueries: [
        'AllCertificateProductQuery',
        'AllCertificateProductQueryShort',
      ],
    }
  );
};

const modifyMutationOptionsSubscription = () => {
  return (
    {
      refetchQueries: [
        'AllSubscriptionProductQuery',
        'AllSubscriptionProductQuery',
      ],
    }
  );
};

const modifyMutationOptionsLecture = () => {
  return (
    {
      refetchQueries: [
        'AllLecturesQuery',
        'AllLecturesQueryShort',
      ],
    }
  );
};

const modifyMutationOptionsCourse = () => {
  return (
    {
      refetchQueries: [
        'AllCoursesQuery',
        'AllCoursesQueryShort',
      ],
    }
  );
};

const modifyMutationOptionsCycle = () => {
  return (
    {
      refetchQueries: [
        'AllCyclesQuery',
        'AllCyclesQueryShort',
      ],
    }
  );
};

export default {
  certificate: modifyMutationOptionsCertificate,
  subscription: modifyMutationOptionsSubscription,
  course: modifyMutationOptionsCourse,
  cycle: modifyMutationOptionsCycle,
  lecture: modifyMutationOptionsLecture,
};
