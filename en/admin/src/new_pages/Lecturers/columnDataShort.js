const columnDataShort = [
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'tags',
    label: 'Направления',
  },
];

export default columnDataShort;
