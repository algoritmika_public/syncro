export default () => {
  return (
    {
      refetchQueries: [
        'AllLecturersQuery',
        'AllLecturersQueryShort',
      ],
    }
  );
};
