const columnData = [
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'tags',
    label: 'Направления',
  },
];

export default columnData;
