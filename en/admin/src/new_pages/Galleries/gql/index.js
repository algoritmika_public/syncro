import {
  CREATE_GALLERY_MUTATION,
  ALL_GALLERIES_QUERY,
  EDIT_GALLERY_MUTATION,
  GALLERY_QUERY,
  DELETE_GALLERY_MUTATION,
  GALLERY_QUERY_WITH_RELATIONS,
} from './../../../constants';
import createGalleryMutation from './createGalleryMutation';
import allGalleriesQuery from './allGalleriesQuery';
import editGalleryMutation from './editGalleryMutation';
import galleryQuery from './galleryQuery';
import deleteGalleryMutation from './deleteGalleryMutation';
import galleryQueryWithRelations from './galleryQueryWithRelations';

const gql = {
  [CREATE_GALLERY_MUTATION]: createGalleryMutation,
  [ALL_GALLERIES_QUERY]: allGalleriesQuery,
  [EDIT_GALLERY_MUTATION]: editGalleryMutation,
  [GALLERY_QUERY]: galleryQuery,

  [DELETE_GALLERY_MUTATION]: deleteGalleryMutation,
  [GALLERY_QUERY_WITH_RELATIONS]: galleryQueryWithRelations,
};

export default gql;
