import { backHistory } from './../../new_utils';

export default (data, { history }) => {
  backHistory(history, '/galleries');
};
