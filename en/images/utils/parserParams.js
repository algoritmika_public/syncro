const resizePattern = /^(\d*)x(\d*)(!?)$/;
const cropPattern = /^(\d*)x(\d*):(\d*)x(\d*)$/;
// const namePattern = /^(\w+)(.(png|jpg|jpeg|svg|gif|bmp|webp))?$/;
const namePattern = /(.(png|jpg|jpeg|svg|gif|bmp|webp))?$/;

// valid paths:
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711/600x200
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711/600x200/20x20
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711/600x200/20x20/Graphcool.jpg
const extractResize = (str) => {
  const [, widthStr, heightStr, forceStr] = str.match(resizePattern);

  if (parseInt(widthStr, 10) <= 0 || parseInt(heightStr, 10) <= 0) {
    throw new Error('Width or height must be positive');
  }

  const width = parseInt(widthStr, 10) || undefined;
  const height = parseInt(heightStr, 10) || undefined;

  if (width === undefined && height === undefined) {
    throw new Error('At least width or height must be provided');
  }

  if ((width && width > 10000) || (height && height > 10000)) {
    throw new Error(`Limit exceeded. Width (${width}) or height (${height}) must not be bigger than 10000`);
  }

  const force = forceStr === '!';

  return { width, height, force };
};

const extractCrop = (str) => {
  const [, xStr, yStr, widthStr, heightStr] = str.match(cropPattern);

  const x = parseInt(xStr, 10) || 0;
  const y = parseInt(yStr, 10) || 0;
  const width = parseInt(widthStr, 10) || 0;
  const height = parseInt(heightStr, 10) || 0;

  return {
    width,
    height,
    x,
    y,
  };
};

// valid paths:
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711/600x200
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711/600x200/20x20
// /v1/ciwkuhq2s0dbf0131rcb3isiq/cj37jinmt008o0108zwhax711/600x200/20x20/Graphcool.jpg

const parseParams = (crop, resize, filename) => {
  const output = {
    resize: undefined,
    crop: undefined,
    filename: undefined,
  };
  let error = null;

  if (resize) {
    if (resize.match(resizePattern)) {
      output.resize = resize;
    } else {
      error = new Error('Invalid resize');
    }
  }

  if (crop && resize) {
    if (crop && resize && `${crop}:${resize}`.match(cropPattern)) {
      output.crop = `${crop}:${resize}`;
    } else {
      error = new Error('Invalid resize and crop');
    }
  }

  if (filename) {
    if (filename.match(namePattern)) {
      output.filename = filename;
    } else {
      error = new Error('Invalid filename');
    }
  }



  return [error, output]
};

const getConfig = (props) => {
  return {
    resize: props.resize ? extractResize(props.resize) : undefined,
    crop: props.crop ? extractCrop(props.crop) : undefined,
  };
};

module.exports = {
  parseParams,
  getConfig,
};
