const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/gallery');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || !req.body.data.Gallery) return next(new Error('Wrond Gallery data'));
  const { data: { Gallery: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
