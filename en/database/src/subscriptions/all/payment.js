const { fromEvent } = require('graphcool-lib');

const fetcPostJson = require('./../../lib/fetcPostJson');

const createTicket = require('./../../lib/order/createTicket');
const updateEvents = require('./../../lib/order/updateEvents');
const updateCertificates = require('./../../lib/order/updateCertificates');
const updateSubscriptions = require('./../../lib/order/updateSubscriptions');
const updateDiscounts = require('./../../lib/order/updateDiscounts');

module.exports = (event) => {
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  const { data } = event;
  const { Payment: { node, mutation, previousValues } } = data;

  if (
    (mutation === 'CREATED' || mutation === 'UPDATED')
    && (node.status === 'PAID')
  ) {
    return Promise.all([
      updateDiscounts(node, api),
      updateSubscriptions(node, api),
      updateCertificates(node, api),
      createTicket(node, api),
    ])
      .then(() => Promise.all([
        fetcPostJson({ data }, `${process.env.CASH_HOST}/payment`),
        fetcPostJson({ data }, `${process.env.MAIL_HOST}/payment`),
      ]))
      .then(result => ({ result }));
  }

  return Promise.all([
    fetcPostJson({ data }, `${process.env.CASH_HOST}/payment`),
    fetcPostJson({ data }, `${process.env.MAIL_HOST}/payment`),
  ])
    .then(result => ({ result }));
};
