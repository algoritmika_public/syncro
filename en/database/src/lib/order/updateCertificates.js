const usedLectures = (id, lecturesOfPromocode) => {
  if (Array.isArray(lecturesOfPromocode)) {
    return lecturesOfPromocode.find(({ id: prId }) => prId === id) || 0;
  }
  return lecturesOfPromocode;
};

module.exports = ({ certificatesInPayments, data }, api) => {
  const { order: lecturesOfPromocode } = data;

  const mutations = certificatesInPayments.map(({ id, unusedLectures }) => `
    mutation {
      updateCertificate(id: "${id}", unusedLectures: ${unusedLectures - usedLectures(id, lecturesOfPromocode)}) {
        id
      }
    }
  `);

  return Promise.all(mutations.map(mutation => api.request(mutation)));
};
