require('isomorphic-fetch');

module.exports = (data, url, { handleError, handleResponse } = {}) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  return fetch(
    url,
    options,
  )
    .then((res) => {
      if (handleError) {
        if (res.status !== 200) {
          return Promise.reject(new Error(res.statusText));
        }
      }

      if (handleResponse) {
        if (res.headers.get('content-type') === 'application/json') return res.json();
        return res.text();
      }

      return { url, status: res.status };
    });
};
