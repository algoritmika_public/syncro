const { fromEvent } = require('graphcool-lib');

const createGraphcoolUser = require('./lib/createGraphcoolUser');
const getGraphcoolUser = require('./lib/getGraphcoolUser');

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!')
    return { error: 'Email Signup not configured correctly.' };
  }
  const { data } = event;
  const {
    email,
    firstName,
    lastName,
    socToken,
    socUserId,
    socialNetwork,
  } = data;

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  return getGraphcoolUser(api, email)
    .then((graphcoolUser) => {
      if (graphcoolUser === null) {
        return createGraphcoolUser(api, {
          email,
          firstName,
          lastName,
          socToken,
          socUserId,
          socialNetwork,
        });
      }
      return graphcoolUser;
    })
    .then((graphcoolUser) => {
      return graphcool.generateAuthToken(graphcoolUser.id, 'User')
        .then((token) => {
          return { data: Object.assign({}, graphcoolUser, { token }) };
        });
    })
    .catch((error) => {
      console.log(error)
      // don't expose error message to client!
      return { error: 'An unexpected error occured.' }
    });
};
