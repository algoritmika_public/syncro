const random = require('./../../../lib/random');

module.exports = (api, { id }, host) => (
  api.request(`
    mutation {
      createToken(
        userId: "${id}",
        token: "${random.string(29)}",
        expired: "${new Date(new Date().getTime() + 86400000).toISOString()}",
        host: "${host}"
      ) {
        id
        user {
          id
          email
          firstName
          lastName
        }
      }
    }`)
    .then((tokenMutationResult) => {
      return tokenMutationResult.createToken;
    })
);
