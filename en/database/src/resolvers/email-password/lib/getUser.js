module.exports = (api, userId) => (
  api.request(`
    query {
      User(id: "${userId}"){
        id,
        role
      }
    }`)
    .then((userQueryResult) => {
      return userQueryResult.User;
    })
    .catch((error) => {
      // Log error but don't expose to caller
      console.log(error);
      return { error: 'An unexpected error occured' };
    })
);
