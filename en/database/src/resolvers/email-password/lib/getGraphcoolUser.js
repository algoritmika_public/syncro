module.exports = (api, email) => (
  api.request(`
    query {
      User(email: "${email}"){
        id
        password
        blocked
        role
      }
    }`)
    .then((userQueryResult) => {
      if (userQueryResult.error) {
        return Promise.reject(userQueryResult.error);
      }
      return userQueryResult.User;
    })
);
