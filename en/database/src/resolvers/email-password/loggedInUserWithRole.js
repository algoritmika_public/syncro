const { fromEvent } = require('graphcool-lib');

const getUser = require('./lib/getUser');

module.exports = (event) => {

  if (!event.context.auth || !event.context.auth.nodeId) {
    return { data: { id: null, role: null } };
  }

  const userId = event.context.auth.nodeId;
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  return getUser(api, userId)
    .then((user) => {
      if (!user) {
        return { error: `No user with id: ${userId}` };
      }
      return { data: user };
    })
    .catch((error) => {
      // Log error but don't expose to caller
      console.log(error);
      return { error: 'An unexpected error occured' };
    });
};
