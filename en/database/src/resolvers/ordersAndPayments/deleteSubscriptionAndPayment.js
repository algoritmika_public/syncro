const { fromEvent } = require('graphcool-lib');

const config = require('./../../config');

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!')
    return { error: 'Email Signup not configured correctly.' };
  }
  const { data } = event;
  const {
    id,
    userId,
  } = data;

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');


  return api.request(`
    query {
      Subscription(id: "${id}"){
        id
        payment {
          id
        }
        user {
          id
        }
      }
    }`)
    .then((result) => {
      if (!result !== !result.Subscription) return null;
      const { Subscription } = result;
      if (Subscription.user.id !== userId) return null;

      return api.request(`
        mutation {
          deleteSubscription(id: "${id}") { id }
          deletePayment(id: "${Subscription.payment.id}") { id }
        }
      `);
    })
    .then((result) => {
      if (!result) return event;
      const { deleteSubscription, deletePayment } = result;
      return { data: { id: deleteSubscription.id } };
    })
    .catch((error) => {
      console.error(error);
      // don't expose error message to client!
      return { error: 'An unexpected error occured.' };
    });
};
