const { fromEvent } = require('graphcool-lib');

const checkOrderCertAndSubOfSite = require('./checkOrderCertAndSubOfSite');
const checkOrderCertAndSubOfAdmin = require('./checkOrderCertAndSubOfAdmin');

const checkSource = (order, api) => {
  const { source } = order;
  if (source && source === 'ADMIN') return checkOrderCertAndSubOfAdmin(order, api);
  return checkOrderCertAndSubOfSite(order, api);
};

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!');
    return { error: 'Email Authentication not configured correctly.' };
  }

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  const { data: { order: orderJson } } = event;
  const order = typeof orderJson === 'string' ? JSON.parse(orderJson) : orderJson;
  // //
  const {
    certificate,
    subscription,
    source,
  } = order;

  //
  if (!certificate && !subscription) return { error: 'Wrong order params' };

  return checkSource(order, api)
    .then(({ resultOfChecking, resultOfCalcOrder }) => {
      return {
        data: {
          resultOfChecking: JSON.stringify(resultOfChecking),
          resultOfCalcOrder: JSON.stringify(resultOfCalcOrder),
        },
      };
    })
    .catch((err) => {
      console.error(err);
      return { error: 'An unexpected error occured' };
    });
};
