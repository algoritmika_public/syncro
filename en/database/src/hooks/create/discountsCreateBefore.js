module.exports = (event) => {
  const { data } = event;
  const { useCount } = data;

  if (useCount === -1) {
    data.unused = 1;
  } else {
    data.unused = useCount;
  }

  return event
};
