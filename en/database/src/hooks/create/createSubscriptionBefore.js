const moment = require('moment');
const { fromEvent } = require('graphcool-lib');

const generateCode = require('./../../lib/order/generateCode');

module.exports = (event) => {
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  return api.request(`
      query {
        SubscriptionProduct(
          id: "${event.data.productId}"
        ) {
          title
          price
          lecturesCount
          months
          prefix
          personsCount
        }
      }
  `)
    .then((result) => {
      if (!result) return Promise.reject();
      const product = result.SubscriptionProduct;
      if (!product) return Promise.reject();
      event.data.title = product.title;
      event.data.price = product.price;
      event.data.lecturesCount = product.lecturesCount;
      event.data.unusedLectures = product.lecturesCount;
      event.data.start = new Date();
      event.data.end = moment().add(product.months, 'months');
      event.data.prefix = product.prefix;
      event.data.personsCount = product.personsCount;
      event.data.code = generateCode(event);
      return event;
    })
    .catch((err) => {
      console.error(err);
      return { error: 'An unexpected error occured' };
    })
};
