const { fromEvent } = require('graphcool-lib');

module.exports = (event) => {
  const { data } = event;
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');
  const { alias, id } = data;

  return api.request(`
    query {
      allAliases(
        filter: {
          id_not: "${id}",
          alias: "${alias}",
        }
        first: 1
      ) {
        id
      }
    }
  `)
    .then((result) => {
      if (result.allAliases.length > 0) return { error: 'Алиас уже сещуствует' };
      return event;
    })
    .catch((error) => {
      console.error(error);
      return { error: 'An unexpected error occured' };
    });
};
