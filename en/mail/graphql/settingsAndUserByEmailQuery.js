const { graphQlFetch } = require('./graphql');

module.exports = (email) => {
  return graphQlFetch({ query: 'allSettingsAndUserByEmailQuery', variables: { email } })
    .then(({ data, errors, error }) => {

      if (errors || error) return Promise.reject(errors || error);

      if (!data || data.allSettings.length === 0) return Promise.reject(new Error('No settings found'));
      const { allSettings: [settings], User: user } = data;
      if (!settings.mailChimpHost
        || !settings.mailChimpAPIKey
        || !settings.mailChimpMainList
        || !settings.mailChimpReactivationList
        || !settings.mailChimpNewSchedule
      ) return Promise.reject(new Error('Settings param not found'));

      return [settings, user];
    });
};
