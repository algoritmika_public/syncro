const express = require('express');
const router = express.Router();
const moment = require('moment');

const mandrill = require('./../mandrill');

router.post('/', (req, res, next) => {
  const { data: { Ticket: { node } } } = req.body;
  const {
    number,
    event,
    user,
  } = node;

  const { lecture, location, date } = event

  let templateName = null;
  let tags = null;
  let subject = null;
  const content = [
    {
      name: 'LECTURE',
      content: lecture.title,
    },
    {
      name: 'DATE',
      content: date,
    },
    {
      name: 'LOCATION',
      content: `Адрес: ${location.address} (м. ${location.metro})`,
    },
    {
      name: 'NUMBER',
      content: number,
    },
  ];
  subject = 'Ваш билет на лекцию';
  tags = ['ticket'];
  templateName = 'mandrillTemplateSlugTicket';


  mandrill(user, {
    name: templateName,
    tags,
    subject,
    content,
  })
    .then((result) => {
      res.json(result);
      // res.json(req.body);
    })
    .catch((err) => {
      console.error(err);
      // res.json(req.body);
      next(err);
    });
});

router.post('/create', (req, res, next) => {
  const { data: { Ticket: { node } } } = req.body;

  const {
    number,
    event,
    user,
  } = node;

  const { lecture, location, date } = event

  let templateName = null;
  let tags = null;
  let subject = null;
  const content = [
    {
      name: 'LECTURE',
      content: lecture.title,
    },
    {
      name: 'DATE',
      content: date,
    },
    {
      name: 'LOCATION',
      content: `Адрес: ${location.address} (м. ${location.metro})`,
    },
    {
      name: 'NUMBER',
      content: number,
    },
  ];
  subject = 'Ваш билет на лекцию';
  tags = ['ticket'];
  templateName = 'mandrillTemplateSlugTicket';


  mandrill(user, {
    name: templateName,
    tags,
    subject,
    content,
  })
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      console.error(err);
      next(err);
    });
});

router.post('/update', (req, res, next) => {
  const { data: { Ticket: { node } } } = req.body;

  const {
    number,
    event,
    user,
  } = node;

  const { lecture, location } = event

  let templateName = null;
  let tags = null;
  let subject = null;
  const content = [
    {
      name: 'LECTURE',
      content: lecture.title,
    },
    {
      name: 'DATE',
      content: date,
    },
    {
      name: 'LOCATION',
      content: `Адрес: ${location.address} (м. ${metro})`,
    },
    {
      name: 'TICKET_NUMBER',
      content: number,
    },
  ];
  subject = 'Ваш билет на лекцию';
  tags = ['ticket'];
  templateName = 'mandrillTemplateSlugTicket';


  mandrill(user, {
    name: templateName,
    tags,
    subject,
    content,
  })
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      console.error(err);
      next(err);
    });
});

module.exports = router;
