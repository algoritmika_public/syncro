const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/cron');

router.post('/upcoming', (req, res, next) => {
  const { body: { data } } = req;

  if (!data || data.allEvents.length === 0) return res.json({});

  controllerAsync.upcoming(req)
    .then((result) => {
      res.json(result);
    })
    .catch(next);
});

router.post('/non-payments', (req, res, next) => {
  const { body: { data } } = req;

  if (!data || data.allEvents.length === 0) return res.json({});

  controllerAsync.nonPayment(req)
    .then((result) => {
      res.json(result);
    })
    .catch(next);
});

router.post('/after', (req, res, next) => {
  const { body: { data } } = req;
  if (!data || !data.Event) return res.json({});

  controllerAsync.after(req)
    .then((result) => {
      res.json(result);
    })
    .catch(next);
});


module.exports = router;
