const express = require('express');
const router = express.Router();
const moment = require('moment');

const mandrill = require('./../mandrill');

router.post('/', (req, res, next) => {
  const { data: { Certificate: { node } } } = req.body;

  const {
    gift,
    end,
    title,
    code,
    user,
  } = node;

  let templateName = null;
  let tags = null;
  let subject = null;

  const content = [
    {
      name: 'TITLE',
      content: title,
    },
    {
      name: 'GIFT',
      content: gift,
    },
    {
      name: 'END',
      content: moment(end).format('YYYY-MM-DD HH:mm'),
    },
    {
      name: 'CODE',
      content: code,
    },
    {
      name: 'USER',
      content: `${user.firstName} ${user.lastName}`,
    },
  ];

  if (gift) {
    templateName = 'mandrillTemplateSlugCertificateAsGift';
    tags = ['certificate-gift'];
    subject = 'Вам подарили сертификат';
  } else {
    templateName = 'mandrillTemplateSlugCertificate';
    tags = ['certificate'];
    subject = 'Сертификат';
  }

  mandrill(user, {
    name: templateName,
    tags,
    subject,
    content,
  })
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      console.error(err);
      next(err);
    });
});

module.exports = router;
