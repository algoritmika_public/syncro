const { $, utils } = window;

const onScroll = (doc) => {

};

const tagsList = () => {
  const container = $('.filter-container');
  const items = $('.sortable-items');
  const dataSelector = 'filter-alias';
  const doc = $(document);
  const fnScroll = onScroll.bind(null, doc);
  doc.on('scroll', fnScroll);

  items.each((i, e) => {
    const itemsActual = $(e);
    const tagsEl = itemsActual.find('.tag');
    tagsEl.on('click', (eT) => {
      const el = $(eT.target);
      let alias = '';
      if (el.hasClass('tag')) {
        alias = el.data('alias');
      } else {
        alias = el.parents('.tag').data('alias');
      }

      const pos = el.offset().top - doc.scrollTop();
      container.eq(i).find(`[data-filter-alias=${alias}]`).trigger('click');
      doc.scrollTop(el.offset().top - pos);
    });
  });

  container.each((i, e) => {
    const containerActual = $(e);
    const itemsActual = items.eq(i).find('.sortable-item');
    const sortingType = containerActual.data('sorting-type');
    let alias = [];
    containerActual.on('click', (event) => {
      const el = $(event.target);
      if (el.hasClass('filter-item')) {
        event.preventDefault();
        const elAlias = el.data(dataSelector);

        if (elAlias === 'all') {
          alias = [];
          utils.setActive(containerActual, el);
        } else {
          if (sortingType === 'single') {
            alias = [elAlias];
            utils.setActive(containerActual, el, alias, '.filter-item');
          } else {
            const ind = alias.indexOf(elAlias);
            if (ind === -1) {
              alias.push(elAlias);
            } else {
              alias.splice(ind, 1);
            }
            utils.setActiveMany(containerActual, el, alias, '.filter-item');
          }
        }
        if (sortingType === 'single') {
          utils.sortItems(itemsActual, alias, dataSelector);
        } else {
          utils.sortItemsMany(itemsActual, alias, dataSelector);
        }
      }
    });
  });
};

export default tagsList;
