import Calendar from './calendar';

const $ = window.$;

const sortableDate = () => {
  const filterContainer = $('.filter-schedule');
  const calendarContainer = $('.simple-calendar-ml');
  const calendar = new Calendar(calendarContainer, scheduleEventsDates, template_calendar);

  calendar.on('nextMonth', (currentMonth, nextMonth) => {

  });

  calendar.on('prevMonth', (currentMonth, prevMonth) => {

  });

  calendar.on('selectDate', (activeDate) => {

  });

  filterContainer.on('click', (e) => {
    const el = $(e.target);
    if (el.closest(calendarContainer).length === 0) {
      e.preventDefault();
      if (el.hasClass('filter-nav')) {
        filterContainer.find('.active').removeClass('active');
        el.addClass('active');
        if (el.hasClass('calendar')) {
          calendarContainer.toggle();
          e.stopPropagation();
        }
      }
    }
  });

  $('body').on('click', (e) => {
    const el = $(e.target);
    if (el.closest(calendarContainer).length === 0 && calendarContainer.is(':visible')) {
      calendarContainer.hide();
    }
  });
};

export default sortableDate;
