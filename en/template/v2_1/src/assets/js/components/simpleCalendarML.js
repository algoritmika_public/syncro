const { $, moment, EventEmitter } = window.moment;

// moment.locale('ru');

class SimpleCalendarML extends EventEmitter {
  constructor(element, eventsDates, options = {}) {
    super();
    this.defaults = {
      defaultDate: new Date(),
    };
    this.container = $(element);
    this.monthWrap = this.container.find('.month');
    this.dateWrap = this.container.find('ul.date-wrap');
    this.options = Object.assign({}, this.defaults, options);

    this.activeDate = this.options.defaultDate;
    this.monthFirstDate = null;
    this.d = null;

    this.handleChangeMonth = this.handleChangeMonth.bind(this);
    this.handleSelectDate = this.handleSelectDate.bind(this);

    this.setCurrentMonth();
    this.renderClendar();
    this.initActions();
  }

  setCurrentMonth(newDate) {
    const date = newDate || this.options.defaultDate;
    const d = new Date(Date.UTC(date.getFullYear(), date.getMonth() + 1, 0));
    this.monthFirstDate = new Date(Date.UTC(date.getFullYear(), d.getMonth(), 1));
    this.d = d;
  }

  initActions() {
    const container = this.container;
    const navMonth = container.find('.nav-calendar');
    const dates = container.find('.date-wrap');
    navMonth.on('click', this.handleChangeMonth);
    dates.on('click', this.handleSelectDate);
  }

  changeMonth(n) {
    const oldMonth = this.monthFirstDate;
    const newMonth = new Date(Date.UTC(
      this.monthFirstDate.getFullYear(),
      this.monthFirstDate.getMonth() + n, 1));

    this.setCurrentMonth(newMonth);

    this.renderClendar(this.activeDate);

    this.emit('setMonth', oldMonth, newMonth, n);
  }

  handleChangeMonth(e) {
    const el = $(e.target);
    if (el.hasClass('next')) {
      this.changeMonth(1);
    } else if (el.hasClass('prev')) {
      this.changeMonth(-1);
    }
  }

  handleSelectDate(e) {
    const el = $(e.target);

  }

  renderClendar() {
    const container = this.container;
    const self = this;
    let i = 0;

    this.dateElements = [];
    this.dateWrap.empty();

    const currentDate = moment(this.monthFirstDate);
    const activeDate = moment(this.activeDate);
    const d = this.d;
    const date = this.monthFirstDate;

    this.monthWrap.text(moment(date).format('MMMM'));

    let day = null;
    let old = true;

    const firstDayWeek = this.monthFirstDate.getDay();

    for (i = 1; i < firstDayWeek; i += 1) {
      day = $('<li/>', { class: 'empty' });
      this.dateWrap.append(day);
    }

    for (i = 1; i <= d.getDate(); i += 1) {
      day = $('<li/>', {
        class: 'date',
        'data-date': i,
        'data-src-date': currentDate.toISOString(),
      }).append($('<a/>', {
        href: `#${i}`,
        'data-date': i,
        'data-src-date': currentDate.toISOString(),
        class: 'date-anc',
        text: i,
      }));

      const diff = activeDate.diff(currentDate, 'h')
      if (diff < 24 && diff > 0) {
        day.addClass('active');
        old = false;
      } else {
        // if (old) {
        //     day.addClass('old');
        // }
      }

      if (scheduleEventsDates.indexOf(currentDate.toISOString()) !== -1) {
        day.addClass('event');
      } else {
        day.addClass('empty');
      }

      const eD = currentDate.format('e');

      if (eD === '5' || eD === '6') {
        day.addClass('holiday');
      }

      this.dateWrap.append(day);
      this.dateElements.push(day);
      day = null;

      currentDate.add(1, 'd');
    }
  }

  static get defaults() {
    return { today: new Date(), activeDate: new Date() };
  }




}

export default SimpleCalendarML;
