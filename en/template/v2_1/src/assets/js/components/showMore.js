const showMore = () => {
  $(document).ready(() => {
    const elements = $('.button-show-more');
    elements.each((i, e) => {
      const el = $(e);
      const container = el.parents('.show-more-container');
      const hiddenEl = container.children('.hidden');
      el.on('click', () => {
        hiddenEl.removeClass('hidden');
        el.remove();
      });
    });
  });
};

export default showMore;
