const $ = window.$;
const utils = window.utils;

const lecturesList = () => {
  const container = $('.lectures-container');
  container.each((i, e) => {
    const el = $(e);
    const lectures = el.find('.lectures-item');
    const moreButton = el.find('.more-lectures');
    moreButton.on('click', (event) => {
      utils.show(lectures);
      utils.hide(moreButton);
      lectures.removeClass('last');
    });
  });
};

export default lecturesList;
