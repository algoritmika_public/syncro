import Queue from './queue';

const {
  $,
  Foundation,
  Spin,
  IMAGE_HOST,
} = window;

const bodyEl = $('body');

class Search {
  constructor(container, template) {
    if (!template) {
      throw new Error('No template');
    }

    this.queue = new Queue();

    this.id = null;
    this.timer = null;
    this.searchResult = null;
    this.template = template;
    this.spin = new Spin();
    this.container = container;
    this.searchEl = this.container.find('.search');
    this.resultSeacrhContainer = this.searchEl.find('.result');
    this.searchForm = this.searchEl.find('.search-form');
    this.btnToggle = $('.search-toggle-btn');
    this.btnClose = this.container.find('.close');
    this.input = this.searchForm.find('input');


    this.search = this.search.bind(this);
    this.success = this.success.bind(this);
    this.error = this.error.bind(this);
    this.worker = this.worker.bind(this);

    this.handlerToggleClick = this.handlerToggleClick.bind(this);
    this.handlerBodyClick = this.handlerBodyClick.bind(this);
    this.handlerCloseClick = this.handlerCloseClick.bind(this);
    this.handlerSearchFormSubmit = this.handlerSearchFormSubmit.bind(this);

    this.queue.worker = this.worker;
    this.queue.success = this.success;
    this.queue.error = this.error;

    this.showSpin = this.showSpin.bind(this);
    this.hideSpin = this.hideSpin.bind(this);

    this.btnToggle.on('click', this.handlerToggleClick);
    this.btnClose.on('click', this.handlerCloseClick);
    this.searchForm.submit(this.handlerSearchFormSubmit);
    this.input.on('input', (e) => {
      this.search();
    });

    bodyEl.on('click', this.handlerBodyClick);
  }
  handlerBodyClick(e) {
    const el = $(e.target);
    if (el.closest(this.searchEl).length === 0 && this.searchEl.is(':visible')) {
      this.btnClose.trigger('click');
    }
  }
  handlerToggleClick(e) {
    let el = '';
    const elTmp = $(e.target);
    if (elTmp.hasClass('search-toggle-btn')) {
      el = elTmp;
    } else {
      el = elTmp.parent();
    }
    this.container.addClass('border-right');
    Foundation.Motion.animateIn(this.searchEl, 'slide-in-right ease-in', (...args) => {
      this.container.removeClass('overflow-hidden');
      this.container.removeClass('border-right');
      this.input.focus();
    });
  }
  handlerCloseClick(e) {
    this.resultSeacrhContainer.empty();
    this.input.val('');
    this.container.addClass('border-right');
    this.container.addClass('overflow-hidden');
    Foundation.Motion.animateOut(this.searchEl, 'slide-out-right ease-out', (...args) => {
      this.container.removeClass('border-right');
    });
  }
  handlerSearchFormSubmit(e) {
    e.preventDefault();
    this.search();
  }
  hideSpin() {
    const { id, spin } = this;
    if (id) {
      spin.hide(id);
      this.id = null;
    }
  }
  showSpin() {
    const { id, spin } = this;
    this.id = spin.show(this.resultSeacrhContainer);
  }
  search() {
    const {
      id,
      input,
      queue,
    } = this;

    this.resultSeacrhContainer.find('.no-found').remove();

    const val = input.val().toLowerCase();

    if (val.length > 2) {
      queue.push(val);
    }
  }
  worker(value, done) {
    this.showSpin();
    $.post('/search/events', { input: value })
      .done((result) => {
        this.hideSpin();
        done(null, result);
      })
      .fail((err) => {
        this.hideSpin();
        done(err);
      });
  }
  success(result) {
    this.searchResult = result.allEvents;
    this.render();
  }
  error(err) {

  }
  render() {
    const html = this.template({ events: this.searchResult, IMAGE_HOST });
    this.resultSeacrhContainer.empty();
    this.resultSeacrhContainer.append(html);
  }
}


export default Search;
