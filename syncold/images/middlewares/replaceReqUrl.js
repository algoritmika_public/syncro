const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;

const {
  PROJECT_ID,
} = require('./../config');

module.exports = (req, res, next) => {
  const { params: { secret } } = req;

  const { filenameForSave } = res.locals;

  if (secret) {
    if (filenameForSave) {
      req.url = `${PROJECT_ID}/${secret}/${filenameForSave}`;
    } else {
      req.url = `${PROJECT_ID}/${secret}`;
    }
  }
  next();
};
