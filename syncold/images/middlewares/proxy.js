const proxy = require('http-proxy-middleware');

const {
  FILE_API_ENDPOINT,
} = require('./../config');

const proxyOptions = {
  target: FILE_API_ENDPOINT,
  changeOrigin: true,
  pathRewrite: (pathOrg, req) => {
    const { params: { secret, param1 } } = req;

    let url = pathOrg;
    if (secret) {
      if (param1) {
        url = `${secret}/${param1}`;
      } else {
        url = `${secret}`;
      }
    }
    return url;
  },
};

module.exports = proxy(proxyOptions);
