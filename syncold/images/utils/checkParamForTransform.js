module.exports = param => /\d{1,}x\d{1,}!?|\d{1,}x!?|x\d{1,}!?/g.test(param);
