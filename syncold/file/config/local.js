const PROJECT_ID = 'cjd5n24x100080161u9vrln2c';
const DOMAIN = 'localhost:60000';

module.exports = {
  PROJECT_ID,
  SIMPLE_API_ENDPOINT: `http://${DOMAIN}/simple/v1`,
  DIR: 'public/files',
  WHITE_LIST: ['http://localhost:3000', 'http://localhost:3001'],
  CONTENT_LENGTH: 300 * 1024 * 1024,
};
