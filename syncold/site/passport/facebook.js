const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;

const signupUserSocialNetworks = require('./../graphql/signupUserSocialNetworks');

const { URL } = require('./../config');

const FACEBOOK_APP_ID = '262384627627343';
const facebookCallback = 'facebook/callback';
const FACEBOOK_APP_SECRET = '815db799cd4d167c2baf06f88c7714d1';

module.exports = () => {
  passport.use(new FacebookStrategy(
    {
      clientID: FACEBOOK_APP_ID,
      clientSecret: FACEBOOK_APP_SECRET,
      callbackURL: `${URL}/users/${facebookCallback}`,
      scope: ['email', 'public_profile'],
      profileFields: ['id', 'first_name', 'last_name', 'email'],
    },
    (accessToken, refreshToken, profile, done) => {
      const user = {};
      user.socToken = accessToken;
      user.socUserId = profile.id;
      user.firstName = profile.name.givenName;
      user.lastName = profile.name.familyName;
      user.email = profile.emails[0].value;
      user.socialNetwork = 'FACEBOOK';

      signupUserSocialNetworks(null, user)
        .then(({ data, errors }) => {
          if (data) {
            const result = data.signupUserSocialNetworks;
            return done(null, result);
          }
          return done(null, false);
        })
        .catch(done);
    },
  ));
};
