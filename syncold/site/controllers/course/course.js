const redis = require('./../../db/redis');
const { graphQlFetch } = require('./../../graphql');
const { ROOT_TOKEN } = require('./../../config');

const {
  sortForUniq,
  additionalDataOfEventsForLectures,
  productParams,
  checkCourseAvailableForBuy,
  additionalDataOfEvents,
} = require('./../course/functions');

const courseByIdQuery = (req, course) => {
  const { id, lectures, tags } = course;

  return graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'courseByIdQuery',
    variables: {
      id,
      lecturesIds: lectures.map(lecture => lecture.id),
      tagsIds: tags ? tags.map(tag => tag.id) : [],
      date: new Date(),
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      const { Course, allEvents, allLectures } = data;
      const { lectures: lecturesOfCourse, recommendedLectures } = Course;
      const additionalDataForCourse = additionalDataOfEvents(allEvents);
      const additionalDataForLectures = additionalDataOfEventsForLectures(allEvents);
      const lecturesWithAdditionalData = lecturesOfCourse
        .map(lecture => Object.assign({}, lecture, additionalDataForLectures[lecture.id]));

      const CourseWithAdditionalData = Object.assign(
        {},
        Course,
        {
          discounts: data.allDiscounts.slice(),
          lectures: lecturesWithAdditionalData,
        },
        additionalDataForCourse,
      );

      const dataWithAdditionalData = Object.assign(
        {},
        data,
        {
          Course: Object.assign(
            CourseWithAdditionalData,
            {
              isAvailableForBuy: checkCourseAvailableForBuy(CourseWithAdditionalData),
              product: productParams(CourseWithAdditionalData),
            },
          ),
          recomendation: recommendedLectures.concat(allLectures).slice(0, 3),
        },
      );
      const currentPageData = Object.assign(
        {},
        dataWithAdditionalData,
      );
      return currentPageData;
      // return null;
    });
};

module.exports = (req, course) => {
  return Promise.all([
    redis.batch([
      ['get', 'template'],
      ['get', 'howWork:course'],
    ]).execAsync(),
    courseByIdQuery(req, course),
  ])
    .then((result) => {
      const [[template, howWork], data] = result;

      return Object.assign(
        {},
        data,
        {
          HowWorkPage: JSON.parse(howWork),
          templateData: JSON.parse(template),
        },
        { ___typename: 'Course' },
      );
    });
};
