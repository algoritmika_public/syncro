const { graphQlFetch } = require('./../../graphql');
const acquiringSettingQuery = require('./../../graphql/acquiringSettingQuery');
const checkAvailabilityOfUserMutation = require('./../../graphql/checkAvailabilityOfUserMutation');
const validator = require('./../../lib/validator');
const redis = require('./../../db/redis');

const { randomBytesAsync } = require('./../../lib/crypto');

const clearPhone = require('./../../lib/clearPhone');

const { ROOT_TOKEN } = require('./../../config');

const allCertificateProductsQuery = (req) => {
  return graphQlFetch({ req: { user: { token: ROOT_TOKEN } }, query: 'allCertificateProductsQuery' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};

module.exports = (req) => {
  return Promise.all([
    redis.getAsync('template'),
    allCertificateProductsQuery(req),
  ])
    .then((result) => {

      const [template, data] = result;
      return Object.assign(
        {},
        data,
        {
          templateData: JSON.parse(template),
        },
      );
    });
};

module.exports.setDraft = (req) => {
  let userAnOrder = null;
  let userAnGift = null;
  let certificate = null;
  let order = null;

  const {
    firstName,
    lastName,
    email,
    phone,
    firstNameGift,
    lastNameGift,
    emailGift,
    dateOfDeliveryGift,
  } = req.body;

  userAnOrder = {
    firstName,
    lastName,
    email,
    phone: clearPhone(phone),
  };

  userAnGift = {
    firstName: firstNameGift,
    lastName: lastNameGift,
    email: emailGift,
  };

  const id = Object.keys(req.body)
    .filter(key => /^id-/.test(key))
    .map(key => key.replace(/^id-/, ''))[0];

  if (!id) return Promise.reject(new Error('Wrong params'));
  const errUserAnOrder = validator(userAnOrder);
  const errUserAnGift = validator(userAnGift);
  if (errUserAnOrder.length > 0 || errUserAnGift.length > 0) return Promise.reject(new Error('Wrong user params'));

  let hash = null;

  return randomBytesAsync(12)
    .then((buff) => {
      hash = buff.toString('hex');
      return graphQlFetch({
        req: { user: { token: ROOT_TOKEN } },
        query: 'certificateProductByIdQuery',
        variables: {
          id,
        },
      });
    })
    .then(({ data, errors, error }) => {
      if (
        errors
        || error
        || !data
        || !data.CertificateProduct
        || !data.CertificateProduct.public
      ) return Promise.reject(errors || error);
      certificate = data.CertificateProduct;
      order = {
        totalPrice: certificate.price,
        orderPrice: certificate.price,
        discountsPrice: 0,
        discountsRate: 0,
        dateOfDeliveryGift
      };
      return redis.setAsync(
        `certificate-draft:${hash}`,
        JSON.stringify({
          userAnOrder,
          userAnGift,
          certificate,
          order,
        }),
      );
    })
    .then(() => ({ hash }));
};

module.exports.draft = (req) => {
  const { id } = req.params;

  return redis.batch([
    ['get', `certificate-draft:${id}`],
    ['get', 'acquiring']
  ]).execAsync()
    .then((results) => {

      if (!results || !results[0] || !results[1]) return Promise.reject(new Error('Not found'));
      data = JSON.parse(results[0]);
      acquiring = JSON.parse(results[1]);

      return Object.assign(
        {},
        data,
        {
          id,
          acquiring: acquiring || {},
        },
      );
    });
};

module.exports.order = (req) => {
  const { id } = req.body;

  let userAnOrder = null;
  let userAnGift = null;
  let order = null;
  let product = null;

  let certificate = null;
  let draft = null;

  return redis.getAsync(`certificate-draft:${id}`)
    .then((reply) => {
      if (!reply) return Promise.reject();
      const obj = JSON.parse(reply);
      draft = JSON.parse(reply);
      userAnOrder = obj.userAnOrder;
      userAnGift = obj.userAnGift;
      order = obj.order;
      product = obj.certificate;
      return Promise.all([
        acquiringSettingQuery(req),
        checkAvailabilityOfUserMutation(req, [userAnOrder, userAnGift]),
      ]);
    })
    .then((result) => {
      const [setting, userIds] = result;
      const [userAnOrderIds, userAnGiftIds] = userIds.ids;

      const payment = {
        acquiringId: setting.acquiring.id,
        commission: setting.acquiring.commission,
        orderPrice: order.totalPrice,
        discountPrice: 0,
        totalPrice: order.totalPrice,
        type: 'INTERNET_ACQUIRING',
        userId: userAnOrderIds,
        status: 'PENDING',
        referer: 'SITE',
        data: draft,
      };
      return graphQlFetch({
        req: { user: { token: ROOT_TOKEN } },
        query: 'createCertificateMutation',
        variables: {
          userId: userAnGiftIds,
          gift: true,
          productId: product.id,
          payment,
          dateOfDeliveryGift: new Date(order.dateOfDeliveryGift),
        }
      })
    })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.createCertificate;
    })
    .then((result) => {
      certificate = result;
      draft.paymentId = certificate.payment.id;

      const commands = [
        ['set', `payment-data:${certificate.payment.id}`, JSON.stringify(draft)],
        ['del', `certificate-draft:${id}`],
      ];
      return redis.batch(commands).execAsync();
    })
    .then(() => {
      certificate.data = draft;
      return certificate;
    });
};

module.exports.payment = (req) => {
  const { id } = req.params;

  return Promise.all([
    redis.getAsync(`payment-data:${id}`),
    acquiringSettingQuery(req),
  ])
    .then((result) => {
      const [reply, setting] = result;
      if (!reply) return Promise.reject();
      const data = JSON.parse(reply);
      if (!data) return Promise.reject();

      return Object.assign(
        {},
        data,
        {
          acquiring: setting.acquiring || {},
        },
      );
    });
};

module.exports.cancel = (req) => {
  const { id } = req.params;
  const { id: userId } = req.user;

  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'deleteCertificateWithPaymentMutation',
    variables: {
      id,
      userId,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};
