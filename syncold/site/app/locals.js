const { slugify } = require('transliteration');
const moment = require('moment');
moment.locale('ru');
slugify.config({ lowercase: false, separator: '_' });

module.exports = (app) => {
  app.locals.moment = moment;
  app.locals.slugify = slugify;
};
