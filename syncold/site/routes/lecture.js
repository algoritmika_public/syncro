const express = require('express');
const router = express.Router();

const graphQlFetch = require('./../graphql').graphQlFetch;
const templateQuery = require('./../graphql/templateQuery');

const lectureFormattedPrice = (lecture) => {
  const events = lecture.events;
  if (events.length === 0) {
    return '';
  }
  const prices = events.map(event => event.price).sort((a, b) => a - b);
  const min = prices[0];
  const max = prices[prices.length - 1];
  if (!max) return min;
  return `${min} - ${max}`;
};

const lectureUniqLecturersOfEvents = (lecture) => {
  const events = lecture.events;
  const lecturers = [];
  const lecturersIds = [];
  events.forEach((event) => {
    event.lecturers.forEach((lecturer) => {
      const { id } = lecturer;
      if (lecturersIds.indexOf(id) !== -1) return null;
      lecturersIds.push(id);
      lecturers.push(lecturer);
    });
  });
  return lecturers;
};

const lectureByIdQuery = (req, id) => {
  return graphQlFetch({ req, query: 'lectureByIdQuery', variables: { id, date_gte: new Date() } })
    .then(({ data, errors, extensions }) => {
      return {
        data,
        errors,
        extensions,
      };
    });
};

router.get('/:id', (req, res, next) => {
  const { id } = req.params;
  Promise.all([
    templateQuery(req),
    lectureByIdQuery(req, id),
  ])
    .then((values) => {
      let currentPageData = { user: req.user };
      let errors = null;
      const error = null;
      values.forEach(({ data, errors: err, extensions }) => {
        if (err) errors = err;
        currentPageData = Object.assign(currentPageData, data);
      });

      if (errors) {
        next(errors);
        return null;
      }

      currentPageData = Object.assign(
        currentPageData,
        {
          Lecture: Object.assign(
            {},
            currentPageData.Lecture,
            {
              formattedPrice: lectureFormattedPrice(currentPageData.Lecture),
              lecturers: lectureUniqLecturersOfEvents(currentPageData.Lecture),
            },
          ),
        },
      );

      res.render('pages/lecture', { currentPageData });
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
