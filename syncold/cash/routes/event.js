const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/event');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || req.body.data.Event) return next(new Error('Wrond Event data'));
  const { data: { Event: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
