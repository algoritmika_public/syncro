const { graphQlFetch } = require('./../graphql');
const redis = require('./../db/redis');

const fetchCallback = (result) => {
  const { data, errors, error } = result;
  if (errors || error) return Promise.reject(errors || error);
  return data;
};

// aliases
const redisBatchSetAliases = (data) => {
  const commands = []
    .concat(
      data.map((item) => {
        const { alias } = item;
        return ['set', `alias:${alias}`, JSON.stringify(item)];
      }),
      data.map((item) => {
        const { alias, id } = item;
        return ['set', `alias:${id}`, alias];
      }),
    );
  return redis.batch(commands).execAsync();
};

const allAliasesQueryAndSet = () => graphQlFetch({ query: 'allAliasesQuery' })
  .then(fetchCallback)
  .then(data => redisBatchSetAliases(data.allAliases));

// events
const groupEventsOfLecture = (data) => {
  const lectures = {};
  data.forEach((item) => {
    const { id, lecture: { id: lectureId } } = item;
    if (!lectures[lectureId]) lectures[lectureId] = [];
    lectures[lectureId].push(id);
  });
  return lectures;
};

const redisBatchEventsSet = (data) => {
  const lectures = groupEventsOfLecture(data);
  const commands = []
    .concat(
      Object.keys(lectures).map((key) => {
        const eventsOfLectures = lectures[key];
        return ['set', `events-of-lecture:${key}`, JSON.stringify(eventsOfLectures)];
      }),
      data.map((item) => {
        const { id } = item;
        return ['set', `events:${id}`, JSON.stringify(item)];
      }),
    );
  return redis.batch(commands).execAsync();
};

const allEventsQueryAndSet = () => graphQlFetch({ query: 'allEventsQuery' })
  .then(fetchCallback)
  .then(data => redisBatchEventsSet(data.allEvents));

// courses
const redisBatchCoursesSet = (data) => {
  const commands = data.map((item) => {
    const { id } = item;
    return ['set', `course:${id}`, JSON.stringify(item)];
  });
  return redis.batch(commands).execAsync();
};

const allCoursesQueryAndSet = () => graphQlFetch({ query: 'allCoursesQuery' })
  .then(fetchCallback)
  .then(data => redisBatchCoursesSet(data.allCourses));

// cycles
const redisBatchCyclesSet = (data) => {
  const commands = data.map((item) => {
    const { id } = item;
    return ['set', `cycle:${id}`, JSON.stringify(item)];
  });
  return redis.batch(commands).execAsync();
};

const allCyclesQueryAndSet = () => graphQlFetch({ query: 'allCyclesQuery' })
  .then(fetchCallback)
  .then(data => redisBatchCyclesSet(data.allCycles));

// lectures
const redisBatchLecturesSet = (data) => {
  const commands = []
    .concat(
      data.map((item) => {
        const { id } = item;
        return ['set', `lecture:${id}`, JSON.stringify(item)];
      }),
      // data
      //   .map((item) => {
      //     const { id, tags } = item;
      //     return tags.map(({ id: tagId }) => ['set', `tag-lecture:${tagId}-${id}`, id]);
      //   })
      //   .reduce((prev, curr) => prev.concat(curr)),
    );

  return redis.batch(commands).execAsync();
};

const allLecturesQueryAndSet = () => graphQlFetch({ query: 'allLecturesQuery' })
  .then(fetchCallback)
  .then(data => redisBatchLecturesSet(data.allLectures));

// lecturer
const redisBatchLecturersSet = (data) => {
  const commands = data.map((item) => {
    const { id } = item;
    return ['set', `lecturer:${id}`, JSON.stringify(item)];
  });
  return redis.batch(commands).execAsync();
};

const allLecturersQueryAndSet = () => graphQlFetch({ query: 'allLecturersQuery' })
  .then(fetchCallback)
  .then(data => redisBatchLecturersSet(data.allLecturers));

// how work
const redisBatchHowWorkSet = (data) => {
  const commands = data.map((item) => {
    const { page } = item;
    return ['set', `howWork:${page}`, JSON.stringify(item)];
  });
  return redis.batch(commands).execAsync();
};

const allHowWorkPagesQueryAndSet = () => graphQlFetch({ query: 'allHowWorkPagesQuery' })
  .then(fetchCallback)
  .then(data => redisBatchHowWorkSet(data.allHowWorkPages));

// template
const allTemplatesQueryAndSet = () => graphQlFetch({ query: 'allTemplatesQuery' })
  .then(fetchCallback)
  .then((data) => {
    if (data.allTemplates.length === 0) return null;
    const template = data.allTemplates[0];
    return redis.setAsync('template', JSON.stringify(template));
  });

// setting
const allSettingsQueryAndSet = () => graphQlFetch({ query: 'allSettingsQuery' })
  .then(fetchCallback)
  .then((data) => {
    if (data.allSettings.length === 0) return null;
    const setting = data.allSettings[0];

    return redis
      .batch([
        ['set', 'setting', JSON.stringify(setting)],
        ['set', 'acquiring', JSON.stringify(setting.acquiring)],
      ])
      .execAsync();
    // return redis.setAsync('setting', JSON.stringify(setting));
  });

// images
const redisBatchImagesSet = (data) => {
  const commands = data.map((item) => {
    const { type } = item;
    return ['set', `gallery:${type}`, JSON.stringify(item)];
  });
  return redis.batch(commands).execAsync();
};

const allImagesOfGalleryQueryAndSet = () => graphQlFetch({ query: 'allImagesOfGalleries' })
  .then(fetchCallback)
  .then(data => redisBatchImagesSet(data.allGalleries));

// payments
const redisBatchPaymentsSet = (data) => {
  const commands = data.map((item) => {
    const { id, data } = item;
    if (data) data.paymentId = id;
    return ['set', `payment-data:${id}`, JSON.stringify(data)];
  });
  return redis.batch(commands).execAsync();
};

const allPaymentsPendingQueryAndSet = () => graphQlFetch({ query: 'allPaymentsPendingQuery' })
  .then(fetchCallback)
  .then(data => redisBatchPaymentsSet(data.allPayments));

module.exports = (app) => {
  // redis.flushall((...args) => {
  //   console.log(args);
  // });
  Promise.all([
    allAliasesQueryAndSet(),
    // allEventsQueryAndSet(),
    allCoursesQueryAndSet(),
    allCyclesQueryAndSet(),
    allLecturesQueryAndSet(),
    allLecturersQueryAndSet(),
    allHowWorkPagesQueryAndSet(),
    allTemplatesQueryAndSet(),
    allSettingsQueryAndSet(),
    allImagesOfGalleryQueryAndSet(),
    allPaymentsPendingQueryAndSet(),
  ])
    .then((results) => {
      console.log('Initial done', results);
      return Promise.resolve(results);
    })
    .catch((err) => {
      console.error('Initial fail', err);
      throw new Error(err);
    });
};
