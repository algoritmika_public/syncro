const redis = require('./../../db/redis');

const redisBatchUpdate = (data) => {
  const { id, alias: { alias } } = data;

  return redis.getAsync(`alias:${alias}`)
    .then((reply) => {
      const oldValue = JSON.parse(reply);
      const newValue = Object.assign(
        {},
        oldValue,
        {
          lecture: {
            id,
            public: data.public,
            tags: data.tags,
          },
        },
      );

      const commands = [];
      commands.push(
        ['set', `alias:${alias}`, JSON.stringify(newValue)],
        ['set', `lecture:${id}`, JSON.stringify(data)],
      );

      return redis.batch(commands).execAsync();
    });
};

const redisBatchSet = (data) => {
  const { id } = data;
  return redis.batch([
    ['set', `lecture:${id}`, JSON.stringify(data)],
  ]).execAsync();
};

const redisBatchDelete = (previousValues) => {
  const { id } = previousValues;
  return redis.batch([
    ['del', `lecture:${id}`],
  ]).execAsync();
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchSet(node);
    }
    case 'UPDATED': {
      return redisBatchUpdate(node);
    }
    case 'DELETED': {
      return redisBatchDelete(previousValues);
    }
    default: {
      return redisBatchSet(node);
    }
  }
};
