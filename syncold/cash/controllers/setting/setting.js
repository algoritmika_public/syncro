const redis = require('./../../db/redis');

const redisBatchUpdate = (data) => {
  const commands = [];
  commands.push(
    ['set', 'setting', JSON.stringify(data)],
  );

  return redis.batch(commands).execAsync();
};

const redisBatchSet = (data) => {
  return redis.batch([
    ['set', 'setting', JSON.stringify(data)],
  ]).execAsync();
};

const redisBatchDelete = (previousValues) => {
  return redis.batch([
    ['del', 'setting'],
  ]).execAsync();
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchSet(node);
    }
    case 'UPDATED': {
      return redisBatchUpdate(node);
    }
    case 'DELETED': {
      return redisBatchDelete(previousValues);
    }
    default: {
      return redisBatchSet(node);
    }
  }
};
