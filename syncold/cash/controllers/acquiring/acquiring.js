const redis = require('./../../db/redis');

const redisBatchUpdate = (data) => {
  return redis.batch([
    ['get', 'setting'],
    ['get', 'acquiring'],
  ]).execAsync()
    .then((reply) => {
      if (!reply) return Promise.reject(new Error('Not find in db'));
      const setting = JSON.parse(reply[0]);
      const acquiring = JSON.parse(reply[1]);
      const commands = [];
      if (acquiring.id === data.id) {
        commands.push(['set', 'acquiring', JSON.stringify(data)]);
        commands.push(['set', 'setting', JSON.stringify(Object.assign(setting, { acquiring: data }))]);
      }

      return redis.batch(commands).execAsync();
    });
};

const redisBatchSet = (data) => {
  return Promise.resolve();
};

const redisBatchDelete = (previousValues) => {
  return redis.getAsync('acquiring')
    .then((reply) => {
      if (!reply) return Promise.resolve();
      const acquiring = JSON.parse(reply);
      if (acquiring.id !== previousValues.id) return Promise.resolve();
      return redis.delAsync('acquiring');
    });
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchSet(node);
    }
    case 'UPDATED': {
      return redisBatchUpdate(node);
    }
    case 'DELETED': {
      return redisBatchDelete(previousValues);
    }
    default: {
      return redisBatchSet(node);
    }
  }
};
