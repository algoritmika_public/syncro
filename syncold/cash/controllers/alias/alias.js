const redis = require('./../../db/redis');

const redisBatchAliasUpdate = (prevAlias, nextAlias) => {
  const { alias: prevAliasAlias, id } = prevAlias;
  const { alias: nextAliasAlias } = nextAlias;
  const commands = [];
  if (prevAliasAlias !== nextAliasAlias) {
    commands.push(
      ['del', `alias:${prevAlias.alias}`],
      ['set', `alias:${id}`, nextAliasAlias],
    );
  }
  commands.push(['set', `alias:${nextAliasAlias}`, JSON.stringify(nextAlias)]);
  return redis.batch(commands).execAsync();
};

const redisBatchAliasSet = (data) => {
  const { alias, id } = data;
  return redis.batch([
    ['set', `alias:${alias}`, JSON.stringify(data)],
    ['set', `alias:${id}`, alias],
  ]).execAsync();
};

const redisBatchAliasDelete = (previousValues) => {
  const { alias, id } = previousValues;
  return redis.batch([
    ['del', `alias:${alias}`],
    ['del', `alias:${id}`],
  ]).execAsync();
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchAliasSet(node);
    }
    case 'UPDATED': {
      return redisBatchAliasUpdate(previousValues, node);
    }
    case 'DELETED': {
      return redisBatchAliasDelete(previousValues);
    }
    default: {
      return redisBatchAliasSet(node);
    }
  }
};
