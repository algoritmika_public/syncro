const express = require('express');
const router = express.Router();
const moment = require('moment');

const mandrill = require('./../mandrill');

router.post('/create', (req, res, next) => {
  const { data: { Order: { node } } } = req.body;

  const {
    user,
    participants,
    events,
  } = node;

  let templateName = null;
  let tags = null;
  let subject = null;

  const content = [
    {
      name: 'EVENTS',
      content: events.map(({ lecture: { title }, date }) => `${title} - ${date}`).join(', '),
    },
    {
      name: 'PARTICIPANTS',
      content: participants.map(({ firstName, lastName }) => `${firstName} ${lastName}`).join(', '),
    },
  ];

  templateName = 'mandrillTemplateSlugOrder';
  tags = ['order'];
  subject = 'Новый заказ';

  mandrill(user, {
    name: templateName,
    tags,
    subject,
    content,
  })
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      console.error(err);
      next(err);
    });
});

module.exports = router;
