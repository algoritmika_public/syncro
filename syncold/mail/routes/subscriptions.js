const express = require('express');
const router = express.Router();
const moment = require('moment');

const mandrill = require('./../mandrill');

router.post('/create', (req, res, next) => {
  const { data: { Subscription: { node } } } = req.body;

  const {
    gift,
    payment,
    end,
    title,
    code,
    user,
  } = node;

  const templateName = 'mandrillTemplateSlugSubscription';
  const tags = ['subscription'];
  const subject = 'Абонемент';

  const content = [
    {
      name: 'TITLE',
      content: title,
    },
    {
      name: 'END',
      content: moment(end).format('YYYY-MM-DD HH:mm'),
    },
    {
      name: 'CODE',
      content: code,
    },
    {
      name: 'USER',
      content: `${payment.user.firstName} ${payment.user.lastName}`,
    },
  ];

  mandrill(user, {
    name: templateName,
    tags,
    subject,
    content,
  })
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      console.error(err);
      next(err);
    });
});

module.exports = router;
