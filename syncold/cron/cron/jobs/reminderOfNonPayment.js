const { graphQlFetch } = require('./../../graphql');

exports.onTick = () => graphQlFetch({
  query: 'allEventsUpcomingQuery',
  variables: {
    date: new Date(),
  },
})
  .then(({ data, errors, error }) => {
    if (errors || error) return Promise.reject(errors || error);
    return data.allEvents;
  });

exports.onComplete = () => {

};
