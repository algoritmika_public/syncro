const controllerAsync = require('./../controllers/errors');

module.exports = (err, req, res, next) => {
  if (!err.checkOrderEventsQuery && !err.checkOrderCertAndSubQuery) return next(err);
  // error handler
  const { checkOrderEventsQuery, checkOrderCertAndSubQuery } = err;

  return controllerAsync(req)
    .then(result => res.render('pages/order-error', { currentPageData: Object.assign({}, result, { user: req.user, checkResults: checkOrderEventsQuery || checkOrderCertAndSubQuery }) }))
    .catch(next);
};
