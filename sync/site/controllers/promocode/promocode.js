const { graphQlFetch } = require('./../../graphql');
const { ROOT_TOKEN } = require('./../../config');

module.exports = (req) => {
  const { data } = req.body;
  if (!data) return Promise.reject(new Error('Wrong input data'));
  const { promocode, entityIds, tagsIds } = JSON.parse(data);
  if (!promocode || !entityIds || !tagsIds) return Promise.reject(new Error('Wrong input data'));

  const date = new Date();

  return graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'promocodeQuery',
    variables: {
      promocode,
      entityIds,
      tagsIds,
      date,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};
