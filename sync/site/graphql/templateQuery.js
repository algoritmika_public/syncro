const { graphQlFetch } = require('./graphql');

const templateQuery = (req) => {
  return graphQlFetch({ req, query: 'allTemplatesQuery' })
    .then(({ data, errors, extensions }) => {
      return {
        data: {
          templateData: data ? data.allTemplates[0] : [],
        },
        errors,
        extensions,
      };
    });
};

module.exports = templateQuery;
