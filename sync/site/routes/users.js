const express = require('express');
const router = express.Router();
const passport = require('passport');

const crypto = require('./../lib/crypto');
const redis = require('./../db/redis');
const fetcPostJson = require('./../lib/fetcPostJson');
// const templateQuery = require('./../graphql/templateQuery');

const { profile: controllerUserProfileAsync } = require('./../controllers/users');

const {
  COOKIE_MAX_AGE,
  ROOT_TOKEN,
  RESET_PASS_TOKEN_EXPIRES,
  URL,
  SECRET,
  MAIL_HOST,
} = require('./../config');
const signupUserMutation = require('./../graphql/signupUserMutation');
const { graphQlFetch } = require('./../graphql/');

const md = (req, res, next) => {
  next();
};

const logout = (req, res, next) => {
  if (req.isAuthenticated()) {
    req.logout();
  }
  next();
};

const addRefererToSession = (req, res, next) => {
  req.session.referer = req.get('Referer');
  next();
};

const rememberCookieAge = (req) => {
  const { remember } = req.body;
  if (remember) {
    req.session.cookie.maxAge = COOKIE_MAX_AGE; // Cookie expires after 30 days
  } else {
    req.session.cookie.expires = false; // Cookie expires at end of session
  }
};

const refererRedirect = (req, res) => {
  const { referer } = req.session;
  if (referer) {
    return res.redirect(referer);
  }
  return res.redirect('/');
};

const signInCallback = (req, res, next) => (error, user, info, status) => {
  if (error) return next(error);
  if (!user) {
    // res.status(401);
    return res.json({ data: null, error: info });
  }

  rememberCookieAge(req);

  req.login(user, (err) => {
    if (err) { return next(err); }
    const { email, firstName, lastName } = user;
    return res.json({ data: { user: { email, firstName, lastName } }, error: null });
  });
};

const signInCallbackSocial = (req, res, next) => (error, user, info, status) => {
  if (error) return next(error);

  if (!user) {
    // res.status(401);
    return refererRedirect(req, res);
  }

  rememberCookieAge(req);

  req.login(user, (err) => {
    if (err) { return next(err); }
    return refererRedirect(req, res);
  });
};

router.post('/sign-up', logout, (req, res, next) => {
  let link = `${URL}/users/reset-pass`;

  let user = null;
  let isError = false;
  let token = '';
  let message = 'Сервис не доступен. Попробуйте позже.';

  signupUserMutation(req)
    .then(({ data, errors, error }) => {
      if (!data || !data.signupUser) {
        isError = true;
        message = errors.map(({ message }) => {
          switch (true) {
            case message.indexOf('Email already in use') !== -1: {
              return 'E-mail уже используется';
            }
            case message.indexOf('Not a valid input data email') !== -1: {
              return 'Неправильный E-mail';
            }
            default: {
              return 'Ошибка. Проверьте указанные данные';
            }
          }
        }).join(' ');
        return null;
      }
      user = data.signupUser;
      return user;
    })
    .then(() => {
      if (isError) {
        return res.json({ data: null, error: { message } });
      }
      return signInCallback(req, res, next)(null, user);
    })
    .catch(next);
});

router.post('/sign-in', logout, (req, res, next) => {
  passport.authenticate(
    'local',
    signInCallback(req, res, next),
  )(req, res, next);
});

router.get('/facebook', logout, addRefererToSession, passport.authenticate('facebook'));

router.get('/facebook/callback', (req, res, next) => {
  passport.authenticate(
    'facebook',
    signInCallbackSocial(req, res, next),
  )(req, res, next);
});

router.get('/vk', logout, addRefererToSession, passport.authenticate('vkontakte'));

router.get('/vk/callback', (req, res, next) => {
  passport.authenticate(
    'vkontakte',
    signInCallbackSocial(req, res, next),
  )(req, res, next);
});

router.get('/logout', logout, (req, res, next) => {
  res.redirect(req.get('Referer') || '/');
});

router.post('/reset-pass', (req, res, next) => {
  const { email } = req.body;
  let link = `${URL}/users/reset-pass`;
  let token = '';
  let user = null;

  let isError = false;
  let message = 'Сервис не доступен. Попробуйте позже.';

  graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'userByEmailQuery',
    variables: { email },
  })
    .then(({ data, errors }) => {
      if (!data || !data.User) {
        isError = true;
        message = 'Пользователь не найден.';
        return null;
      }
      user = data.User;

      return crypto.randomBytesAsync(48);
    })
    .then((buffer) => {
      if (!buffer) {
        isError = true;
        return null;
      }
      token = buffer.toString('hex');

      link = `${link}/${token}`;
      return redis.setAsync(`reset-pass:${token}`, user.id, 'EX', RESET_PASS_TOKEN_EXPIRES);
    })
    .then((reply) => {
      if (!reply) {
        isError = true;
        return null;
      }

      return fetcPostJson(`${MAIL_HOST}/user/reset-pass`, Object.assign(
        {},
        {
          link,
          expires: RESET_PASS_TOKEN_EXPIRES / 60,
        },
        user,
      ));
    })
    .then((mailRes) => {
      if (!mailRes || mailRes.status !== 200) {
        isError = true;
      }
      return null;
    })
    .then(() => {
      if (isError) return res.json({ data: null, error: { message } });
      return res.json({ data: { success: true } });
    })
    .catch(next);
});

router.get('/reset-pass/:token', (req, res, next) => {
  if (req.isAuthenticated()) {
    req.logout();
  }

  const { token } = req.params;
  let id = '';

  Promise.all([
    redis.getAsync(`reset-pass:${token}`)
      .then((reply) => {
        if (!reply) return Promise.reject();
        id = reply;
        return crypto.randomBytesAsync(48);
      })
      .then((buffer) => {
        checkToken = buffer.toString('hex');
        return redis.setAsync(`reset-pass-check:${checkToken}`, JSON.stringify({ id, token }), 'EX', RESET_PASS_TOKEN_EXPIRES);
      })
      .then(() => ({ data: { checkToken } })),
    redis.getAsync('template'),
  ])
    .then((result) => {
      const [data, template] = result;
      const currentPageData = Object.assign(
        {
          user: req.user,
          templateData: JSON.parse(template),
        },
        data,
      );

      return res.render('pages/reset-pass', { currentPageData });
    })
    .catch(next);
});

router.post('/update-pass', (req, res, next) => {
  if (req.isAuthenticated()) {
    req.logout();
  }

  const { password, repassword, checkToken } = req.body;
  if (password !== repassword || !checkToken || password.length < 8) {
    return res.json({ data: null, error: { message: 'Неверные данные' } });
  }
  let obj = null;
  redis.getAsync(`reset-pass-check:${checkToken}`)
    .then((reply) => {
      obj = JSON.parse(reply);
      return graphQlFetch({
        req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
        query: 'updateUserPasswordMutation',
        variables: {
          id: obj.id,
          password,
        },
      });
    })
    .then(({ data, errors, error }) => {
      if (data && data.updateUser) {
        return redis.delAsync(`reset-pass-check:${checkToken}`, `reset-pass:${obj.token}`);
      }
      return null;
    })
    .then((reply) => {
      if (reply) {
        return res.json({ data: { success: true }, error: null });
      }
      return res.json({ data: null, error: { message: 'Ошибка. Попробуйте позже' } });
    })
    .catch(next);
});

router.post('/confirm-email', (req, res, next) => {
  const { data: { User: { node: { email } } }, secret } = req.body;

  if (secret !== SECRET) return next(new Error('Wrong param'));

  let link = `${URL}/users/confirm-email`;
  let token = '';
  let user = null;

  let isError = false;
  let message = 'Сервис не доступен. Попробуйте позже.';

  return graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'userByEmailQuery',
    variables: { email },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);

      if (!data || !data.User) {
        isError = true;
        message = 'Пользователь не найден.';
        return null;
      }

      if (data.User.emailConfirmed) {
        isError = true;
        message = 'E-mail уже подтвержден.';
        return null;
      }

      user = data.User;

      return crypto.randomBytesAsync(48);
    })
    .then((buffer) => {
      if (!buffer) {
        isError = true;
        return null;
      }
      token = buffer.toString('hex');

      link = `${link}/${token}`;
      return redis.setAsync(`confirm-email:${token}`, user.id);
    })
    .then((reply) => {
      if (!reply) {
        isError = true;
        return null;
      }

      return fetcPostJson(`${MAIL_HOST}/user/confirm-email`, Object.assign({}, { link }, user));
    })
    .then((mailRes) => {
      if (!mailRes || mailRes.status !== 200) {
        isError = true;
      }
      return null;
    })
    .then(() => {
      if (isError) return res.json({ data: null, error: { message } });
      return res.json({ data: { success: true } });
    })
    .catch(next);
});

router.get('/confirm-email/:token', (req, res, next) => {
  const { token } = req.params;
  let isError = false;

  redis.getAsync(`confirm-email:${token}`)
    .then((reply) => {
      if (!reply) return {};
      return graphQlFetch({
        req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
        query: 'updateUserEmailConfirmMutation',
        variables: {
          id: reply,
        },
      });
    })
    .then(({ data, errors, error }) => {
      if (!data || !data.updateUser) {
        isError = true;
        return null;
      }
      return redis.delAsync(`confirm-email:${token}`);
    })
    .then(() => {
      return redis.getAsync('template');
    })
    .then((reply) => {
      return res.render('pages/confirm-email', {
        currentPageData: {
          error: isError,
          templateData: JSON.parse(template),
        },
      });
    })
    .catch(next);
});

router.get('/profile', (req, res, next) => {
  if (!req.isAuthenticated()) return res.json({ data: null, error: false });
  controllerUserProfileAsync(req)
    .then((user) => {
      return res.json({ data: { user }, error: false });
    })
    .catch(next);
});

module.exports = router;
