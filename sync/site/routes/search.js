const express = require('express');
const router = express.Router();
const moment = require('moment');

const graphQlFetch = require('./../graphql').graphQlFetch;

/* GET home page. */
router.post('/events', (req, res, next) => {
  const input = req.body.input.substring(0, 150);
  graphQlFetch({ req, query: 'searchEventsOfLectureTitle', variables: { input, date_gte: new Date() } })
    .then(({ data, errors, extensions }) => {
      if (errors) {
        res.json({ error: true });
        return null;
      }
      res.json(data);
    })
    .catch((error) => {
      res.json({ error: true });
    });
});

router.post('/lecture', (req, res, next) => {
  const input = req.body.input.substring(0, 150);
  graphQlFetch({ req, query: 'searchLectureOfTitle', variables: { input, date_gte: new Date() } })
    .then(({ data, errors, extensions }) => {
      if (errors) {
        res.json({ error: true });
        return null;
      }
      res.json(data);
    })
    .catch((error) => {
      res.json({ error: true });
    });
});

module.exports = router;
