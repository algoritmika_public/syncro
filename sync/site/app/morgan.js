const morgan = require('morgan');

module.exports = (app) => {
  let format = 'combined';
  if (process.env.NODE_ENV === 'development') {
    format = 'dev';
  }

  app.use(morgan(format, {
    skip: (req, res) => res.statusCode < 400,
    stream: process.stdout,
  }));

  app.use(morgan(format, {
    skip: (req, res) => res.statusCode >= 400,
    stream: process.stderr,
  }));
};
