const validator = require('validator');
const locale = 'ru-RU';

module.exports = (obj) => {
  const keys = Object.keys(obj);
  if (keys.length === 0) return false;

  const errors = keys.filter((key) => {
    const value = obj[key];
    if (validator.isEmpty(value)) return validator.isEmpty(value);

    switch (key) {
      case 'firstName': {
        return !validator.isAlpha(value, locale);
      }
      case 'secondName': {
        return !validator.isAlpha(value, locale);
      }
      case 'lastName': {
        return !validator.isAlpha(value, locale);
      }
      case 'email': {
        return !validator.isEmail(value);
      }
      case 'phone': {
        return !validator.isMobilePhone(value, locale);
      }
      default: {
        return false;
      }
    }
  });
  return errors;
};
